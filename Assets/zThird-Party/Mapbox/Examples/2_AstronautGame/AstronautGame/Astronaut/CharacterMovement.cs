using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Mapbox.Unity.Location;

namespace Mapbox.Examples {
    public class CharacterMovement : MonoBehaviour {
        public PositionWithLocationProvider positionWithLocationProvider;
        public Vector3 posTarget;
        public Animator CharacterAnimator;
        public float Speed;
        AstronautMouseController _controller;
        void Start() {
            _controller = GetComponent<AstronautMouseController>();
        }

        void Update() {
            var distance = Vector3.Distance(transform.position, positionWithLocationProvider._targetPosition);
            if (distance > 0.1f) {
                transform.LookAt(positionWithLocationProvider._targetPosition);
                CharacterAnimator.SetBool("IsWalking", true);
            } else {
                CharacterAnimator.SetBool("IsWalking", false);
            }
        }
    }
}