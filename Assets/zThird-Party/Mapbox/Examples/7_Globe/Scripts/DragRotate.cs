﻿namespace Mapbox.Examples
{
    using UnityEngine;

    namespace Scripts.Utilities
    {
        public class DragRotate : MonoBehaviour
        {
            [SerializeField]
            Transform _objectToRotate;
            [SerializeField] Transform astronaut;


            [SerializeField]
            float _multiplier;

            Vector3 _startTouchPosition;

            private float timer;
            [SerializeField] private float timeToActivateForceRotation;
            [SerializeField] private bool PlayerRotating;
            [SerializeField] private bool ForceRotation;

            void Update() {
                if (Input.GetMouseButtonDown(0)) {
                    PlayerRotating = true;
                    ForceRotation = false;
                    _startTouchPosition = Input.mousePosition;
                }

                if (Input.GetMouseButton(0) && !(Input.touchCount >= 2)) {
                    var dragDelta = Input.mousePosition - _startTouchPosition;
                    var axis = new Vector3(0f, -dragDelta.x * _multiplier, 0f);
                    _objectToRotate.RotateAround(_objectToRotate.position, axis, _multiplier);
                }

                if (Input.GetMouseButtonUp(0)) {
                    PlayerRotating = false;
                    timer = 0;
                }

                if (!PlayerRotating && !GeoManager.instance.OnClosestLocationWithoutGPS) {
                    timer += Time.deltaTime;
                    if (timer >= timeToActivateForceRotation) {
                        ForceRotation = true;
                        timer = 0;
                    }
                }
                //if (ForceRotation) {
                //    _objectToRotate.rotation = Quaternion.Lerp(transform.rotation, astronaut.rotation, 0.04f);
                //}
            }
        }
    }
}
