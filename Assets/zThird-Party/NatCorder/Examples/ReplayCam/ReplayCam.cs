/* 
*   NatCorder
*   Copyright (c) 2020 Yusuf Olokoba
*/

namespace NatCorder.Examples
{

    using UnityEngine;
    using System.Collections;
    using Clocks;
    using Inputs;
    using UnityEditor;
    using System.IO;

    public class ReplayCam : MonoBehaviour
    {

        [Header("Recording")]
        private int videoWidth = 1280;
        private int videoHeight = 720;
        public bool recordMicrophone;
        public Camera uiCamera;

        private IMediaRecorder recorder;
        private CameraInput cameraInput;
        private AudioInput audioInput;
        private AudioSource microphoneSource;

        string path;
        byte[] arrayBytes;

        private IEnumerator Start()
        {
            var screenDif = (float)Screen.height / (float)Screen.width;
            //print("ScreenAspect 1/" + screenDif);
            //videoHeight = (int)(screenDif * 500);
            //videoWidth = (int)(500); 
            videoHeight = Screen.height;
            videoWidth = Screen.width;

            //print(videoWidth + " + " + videoHeight);

            // Start microphone
            microphoneSource = gameObject.AddComponent<AudioSource>();
            microphoneSource.mute =
            microphoneSource.loop = true;
            microphoneSource.bypassEffects =
            microphoneSource.bypassListenerEffects = false;
            microphoneSource.clip = Microphone.Start(null, true, 10, AudioSettings.outputSampleRate);
            yield return new WaitUntil(() => Microphone.GetPosition(null) > 0);
            microphoneSource.Play();
        }

        private void OnDestroy()
        {
            // Stop microphone
            microphoneSource.Stop();
            Microphone.End(null);
        }

        public void StartRecording()
        {
            SharingManager.instance.SetUIActive(false);
            //SharingManager.instance.videoButtonStop.SetActive(true);
            SharingManager.instance.photoImageUXart.SetActive(false);

            // Start recording
            var frameRate = 30;
            var sampleRate = recordMicrophone ? AudioSettings.outputSampleRate : 0;
            var channelCount = recordMicrophone ? (int)AudioSettings.speakerMode : 0;
            var clock = new RealtimeClock();
            recorder = new MP4Recorder(videoWidth, videoHeight, frameRate, sampleRate, channelCount);
            // Create recording inputs
            cameraInput = new CameraInput(recorder, clock, Camera.main, uiCamera);
            audioInput = recordMicrophone ? new AudioInput(recorder, clock, microphoneSource, true) : null;
            // Unmute microphone
            microphoneSource.mute = audioInput == null;
        }

        public async void StopRecording()
        {
            //SharingManager.instance.SetUIActive(true);
            //SharingManager.instance.Recording = false;

            // Mute microphone
            microphoneSource.mute = true;
            // Stop recording
            audioInput.Dispose();
            cameraInput.Dispose();
            path = await recorder.FinishWriting();
            //// Playback recording

            arrayBytes = File.ReadAllBytes(path);

            //Debug.Log("Saved recording to: {path}");

            //finish upload after confirmation

            SharingManager.instance.OpenConfirmCG();
        }

        public void PlayRecording()
        {
            StartCoroutine(PlayRecordingCR(path));
        }

        private IEnumerator PlayRecordingCR(string path)
        {
            yield return new WaitForSecondsRealtime(1);

    #if UNITY_EDITOR
                EditorUtility.OpenWithDefaultApp(path);
    #elif UNITY_IOS
                Handheld.PlayFullScreenMovie("file://" + path);
    #elif UNITY_ANDROID
                Handheld.PlayFullScreenMovie(path);
    #endif
        }

        public void FinishRecording() {
            StartCoroutine(UploadWaiter(path));
            //SharingManager.instance.UploadFeed(arrayBytes, "video");
            SharingManager.instance.SetUIActive(true);
            SharingManager.instance.Recording = false;
        }

        public string GetPreviewPath()
        {
            return path;
        }

        private IEnumerator UploadWaiter(string path)
        {
            yield return new WaitForSecondsRealtime(1);
#if UNITY_EDITOR
            //EditorUtility.OpenWithDefaultApp(path);
            new NativeShare().AddFile(path).SetSubject("UXART").SetText("#UXart").Share();
#elif UNITY_IOS
            //Handheld.PlayFullScreenMovie("file://" + path);
            new NativeShare().AddFile("file://" + path).SetSubject("UXART").SetText("#UXart").Share();
#elif UNITY_ANDROID
            //Handheld.PlayFullScreenMovie(path);
            new NativeShare().AddFile(path).SetSubject("UXART").SetText("#UXart").Share();
#endif
        }
    }
}