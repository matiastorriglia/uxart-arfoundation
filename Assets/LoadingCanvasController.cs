﻿using BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Mozilla;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class LoadingCanvasController : MonoBehaviour
{
    public static LoadingCanvasController instance;

    [SerializeField] private CanvasGroup imagenCarga;
    [SerializeField] private int procesosEnCurso;

    [Header("AR Loading")]
    public CanvasGroup arLoadCG;
    public Text arLoadText;
    float arLoadCounter = 0;
    float arLoadTime1 = 3;
    float arLoadTime2 = 8;
    float arLoadTime3 = 15;

    public GameObject portalLoadingGO;
    public Text textImagenCarga;

    public Text geoText;

    CanvasGroup surfDetectCG;

    private void Awake() {
        if (instance!=null) {
            DestroyImmediate(gameObject);
            return;
        }
        instance = this;
        DontDestroyOnLoad(gameObject);
    }
    private void Update()
    {
        if (arLoadCG.alpha == 1)
        {
            arLoadCounter += Time.deltaTime;

            if(arLoadCounter < arLoadTime1)
            {
                arLoadText.text = "";
            }
            else if(arLoadCounter >= arLoadTime1 && arLoadCounter < arLoadTime2)
            {
                arLoadText.text = LocalizationManager.instance.GetLocalizedValue("ar_load1");
            }
            else if(arLoadCounter >= arLoadTime2 && arLoadCounter < arLoadTime3)
            {
                arLoadText.text = LocalizationManager.instance.GetLocalizedValue("ar_load2");
            }
            else
            {
                arLoadText.text = LocalizationManager.instance.GetLocalizedValue("ar_load3");
            }
        }

        //if (SceneManager.GetActiveScene().name != "ARSurface")
        //{
        //    if (!surfDetectCG)
        //        surfDetectCG = GameObject.Find("ARLoadCanvas").GetComponent<CanvasGroup>();

        //    if (surfDetectCG)
        //    {
        //        surfDetectCG.alpha = 0;
        //        surfDetectCG.transform.GetChild(1).gameObject.SetActive(false);
        //    }
        //}

        if (SceneManager.GetActiveScene().name == "MapGeolocalization") {
            geoText.enabled = true;
        }else
            geoText.enabled = false;
    }

    public void ARLoad() {
        arLoadCounter = 0;
        arLoadText.text = "";
        arLoadCG.alpha = 1;
        ProcesosEnCurso++;
    }

    public void ARDone() {
        arLoadCG.alpha = 0;
        arLoadText.text = "";
    }

    public int ProcesosEnCurso {
        get {
            return procesosEnCurso;
        }
        set {
            procesosEnCurso = value;
            if (procesosEnCurso < 0) {
                procesosEnCurso = 0;
            }
            ImagenCargaEstado(value <= 0 ? false : true);
        }
    }

    private void ImagenCargaEstado(bool turnOn) {
        imagenCarga.alpha = turnOn ? 1 : 0;
        imagenCarga.blocksRaycasts = turnOn;
        imagenCarga.interactable = turnOn;
        portalLoadingGO.SetActive(turnOn);
    }

}
