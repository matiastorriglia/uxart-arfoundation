﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.Experimental.XR;
using UnityEngine.XR.ARSubsystems;
using UnityEngine.EventSystems;
using UnityEngine.Video;

public class ARPlaceObject : MonoBehaviour
{

    public GameObject placementIndicator;
    public GameObject objectToPlace;

    private ARRaycastManager arRay;
    [SerializeField] ARPlaneManager arPlane;
    private ARSessionOrigin arOrigin;
    private Pose placementPose;
    private bool placementPoseIsValid = false;

    [SerializeField] private Transform spawnLocation;
    [SerializeField] private GameObject spawnButton;
    [SerializeField] private Transform objectContainer;
    [SerializeField] private Camera cam;
    private AssetBundle bundle;

    public ObjectArData objectAr;
    public string location;
    public bool from3DBox;

    [SerializeField] private RenderTexture videoRenderTexture;
    [SerializeField] private Material videoChromaMaterial;

    Vector2 touchPos;

    static List<ARRaycastHit> hits = new List<ARRaycastHit>();

    private GameObject objetoInstanciado;

    public GameObject panelInstructivo;

    bool planesExist = false;

    bool hasNFT = false;

    bool first = true;

    // Start is called before the first frame update
    void Awake()
    {
        arOrigin = FindObjectOfType<ARSessionOrigin>();
        if (arOrigin)
            arRay = arOrigin.GetComponent<ARRaycastManager>();
    }


    private void PlaneChanged(ARPlanesChangedEventArgs args)
    {
        if (args.added != null && !objetoInstanciado.activeInHierarchy)
        {
            if (args.added.Count > 0)
            {
                ARPlane firstPlane = args.added[0];
            }
        }
    }

    bool TryGetTouchPosition(out Vector2 touchPos)
    {
        if (Input.touchCount > 0)
        {
            touchPos = Input.GetTouch(0).position;
            return true;
        }

        touchPos = default;
        return false;
    }

    // Update is called once per frame
    void Update()
    {
        if (placementIndicator.activeInHierarchy)
            NFTButton.instance.buttonObj.SetActive(hasNFT);

        if (objetoInstanciado != null) {
            if (!objetoInstanciado.activeInHierarchy && !first)
            {
                arPlane.planesChanged += PlaneChanged;

                //if (!TryGetTouchPosition(out Vector2 touchPos))
                //{
                //    return;
                //}

                //if(EventSystem.current.IsPointerOverGameObject(Input.GetTouch(0).fingerId))
                //    return;

                //Ray ray = cam.ScreenPointToRay(touchPos);

                //if (Physics.Raycast(ray, out RaycastHit hit))
                //{
                //    if(hit.collider.gameObject.tag == "UI")
                //        return;
                //}

                //if (arRay.Raycast(touchPos, hits, TrackableType.Planes) && !touching)
                //{
                //    var hitPose = hits[0].pose;
                //    if (!touching)
                //    {
                //        spawnLocation.transform.position = hitPose.position;
                //        spawnButton.SetActive(true);
                //    }
                //    hits.Clear();
                //}

                if (arRay.Raycast(cam.ViewportToScreenPoint(new Vector2(0.5f, 0.5f)), hits, TrackableType.PlaneWithinPolygon))
                {
                    var hitPose = hits[0].pose;
                    placementIndicator.transform.position = hitPose.position;
                    Spawn();
                    first = true;
                    hits.Clear();
                }
            }
            else
            {
                //if (!TryGetTouchPosition(out Vector2 touchPos))
                //{
                //    return;
                //}

                //if (arRay.Raycast(touchPos, hits, TrackableType.Planes))
                //{
                //    var hitPose = hits[0].pose;
                //    placementIndicator.transform.position = hitPose.position;
                //    //if (objetoInstanciado)
                //    //    objetoInstanciado.SetActive(true);
                //}
            }
        }
    }

    public void Spawn()
    {
        NFTButton.instance.buttonObj.SetActive(hasNFT);

        spawnButton.gameObject.SetActive(false);
        spawnLocation.gameObject.SetActive(false);
        panelInstructivo.SetActive(false);
        placementIndicator.SetActive(true);
    }

    void Start()
    {
        StartCoroutine(LoadObjectAr());
        StartCoroutine(InfoManager.instance.StartARalert(5));
    }

    public IEnumerator LoadObjectAr()
    {
        objectAr = AssetBundleManager.instance.objetoAR;    //el objeto a instanciar
        location = AssetBundleManager.instance.location;    //la localizacion del objeto
        from3DBox = AssetBundleManager.instance.from3DBox;  //?????

        if (location != "")
        {
            location = location.Split(',')[1] + ", " + location.Split(',')[0]; //Lo corrigue agregandole espacio y cambiando de lugar los datos de localizacion
        }

        if (objectAr.prefabName == "INFO")
        {
            objetoInstanciado = Instantiate(GameManager.instance.DashboardPrefab, transform.position, Quaternion.identity, objectContainer); //instancia objeto de info
            ConnectionManager.instance.AddEmitCommand("actions_dashboard", "{'accion':'lookUpDashboardId','dashboard_location':'" + location + "','channel_id':'" + ChannelManager.instance.currentChannelId + "','devolucion':'_receive_dashboardId'}");
        }
        else
        {
            yield return new WaitWhile(() => AssetBundleManager.instance.ObtainAssetBundle(objectAr.url_assetBundle, objectAr.bundleName) == null);  //instancia objeto de info
            bundle = AssetBundleManager.instance.ObtainAssetBundle(objectAr.url_assetBundle, objectAr.bundleName);
            GameObject prefab = bundle.LoadAsset(objectAr.prefabName + ".prefab") as GameObject; //Creo el prefab

            InfoManager.instance.GetObjectARInfo(objectAr);

            if (prefab == null)
            {
                Debug.Log("Error Aqui, Prefab es null");
                //visualize in app??
            }
            //else{
            objetoInstanciado = Instantiate(prefab, objectContainer.transform.position, Quaternion.identity);


            float ambientLight;
            if (objectAr.ambientLight != null)
            {
                ambientLight = float.Parse(objectAr.ambientLight, System.Globalization.CultureInfo.InvariantCulture);
                GameObject.Find("Directional Light").GetComponent<Light>().intensity = ambientLight * 0.1f;

                if (ambientLight == 0)
                {
                    RenderSettings.skybox = null; //Paraque la oscuridad sea total
                    RenderSettings.ambientSkyColor = Color.black;
                }
            }

            MeshRenderer[] renderers = objetoInstanciado.GetComponentsInChildren<MeshRenderer>();

            foreach (MeshRenderer r in renderers)
            {
                //Material original = r.material;
                //Destroy(r.material);
                //r.material = original;
                //Shader temp = r.material.shader;
                //r.material.shader = null;
                if (r.material.shader.name != "Custom/CustomStandard")
                    r.material.shader = Shader.Find(r.material.shader.name);
                else
                    r.material.shader = Shader.Find("Standard");
            }
        }

        //include this previous if?
        if (objectAr.prefabName == "INFO")
        {
            var lookPos = cam.transform.position - objetoInstanciado.transform.position;
            lookPos.y = 0;
            var rotation = Quaternion.LookRotation(lookPos);
            objetoInstanciado.transform.rotation = rotation;
        }
        else
        {
            GameObject canvasObj = GameObject.Find("NFTCanvas");

            if (canvasObj)
            {
                NFTButton.instance.url = canvasObj.GetComponent<TarjetaPromociones>().urlButton1;
                hasNFT = true;
            }
        }

        objectContainer.gameObject.AddComponent<Lean.Touch.LeanPinchScale>();
        objectContainer.localScale = new Vector3(0.5f, 0.5f, 0.5f);

        objetoInstanciado.transform.SetParent(objectContainer);
        objetoInstanciado.transform.localPosition = Vector3.zero;

        if (from3DBox && objectAr.scaleOffset != null)
        {
            objetoInstanciado.transform.localScale *= float.Parse(objectAr.scaleOffset, System.Globalization.CultureInfo.InvariantCulture);
        }

        Lean.Touch.LeanTwistRotateAxis ltsa = objectContainer.gameObject.AddComponent<Lean.Touch.LeanTwistRotateAxis>();
        ltsa.Axis = new Vector3(0, -1, 0);
        placementIndicator.AddComponent<Lean.Touch.LeanDragTranslate>();

        placementIndicator.SetActive(false);

        StartCoroutine(CameraDelay());
    }

    IEnumerator CameraDelay()
    {
        Invoke("LoadingFinish", 0.2f);
        yield return new WaitForSeconds(1f);
        cam.enabled = true;
        yield return new WaitForSeconds(0.5f);
        this.enabled = true;
        first = false;
        //xRSurface.gameObject.SetActive(true);
    }

    public void LoadingFinish()
    {
        //turn of loading wheel here? check loadingcanvascontroller
        LoadingCanvasController.instance.ARDone();
        //loadingGO.SetActive(false);
        LoadingCanvasController.instance.ProcesosEnCurso--;
    }

    void PlaceObject()
    {
        Instantiate(objectToPlace, placementPose.position, placementPose.rotation);
    }

    void UpdatePlacementPose()
    {
        //var screenCenter = cam.ViewportToScreenPoint(new Vector3(0.5f, 0.5f));
        //var hits = new List<ARRaycastHit>();
        ////arOrigin.Raycast(screenCenter, hits, TrackableType.Planes);

        //arRaycast.Raycast(screenCenter, hits, TrackableType.Planes);

        //placementPoseIsValid = hits.Count > 0;

        //if (placementPoseIsValid)
        //{
        //    placementPose = hits[0].pose;

        //    var cameraForward = cam.transform.forward;
        //    var cameraBearing = new Vector3(cameraForward.x, 0, cameraForward.z).normalized;

        //    placementPose.rotation = Quaternion.LookRotation(cameraBearing);
        //}
    }

    void UpdatePlacementIndicator()
    {
        if (placementPoseIsValid)
        {
            placementIndicator.SetActive(true);
            placementIndicator.transform.SetPositionAndRotation(placementPose.position, placementPose.rotation);
        }
        else
        {
            placementIndicator.SetActive(false);
        }
    }
}
