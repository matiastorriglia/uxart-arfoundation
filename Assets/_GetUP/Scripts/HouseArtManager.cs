﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Networking;

public class HouseArtManager : MonoBehaviour
{
    private ObjectArData objectAr;

    [Header("Components")]
    public Button artButton;
    public TextMeshProUGUI artText;
    public Image image;

    public ObjectArData ObjectAr {
        get => objectAr; set {
            objectAr = value;
            StartCoroutine(HouseArtLoad());
        }
    }

    public void LoadObjectAr()
    {
        AssetBundleManager.instance.objetoAR = ObjectAr;
        AssetBundleManager.instance.from3DBox = true;
        AssetBundleManager.instance.location = "";

        ObjectsArManager.instance.CloseArtHouseContent();
        GameManager.instance.CargarEscenaAR();
    }

    private IEnumerator HouseArtLoad()
    {
        if (ObjectAr == null)
        {
            Debug.LogWarning("Debe asignarle un ObjectAr antes de buscar una imagen");
            yield break;
        }

        using (UnityWebRequest uwr = UnityWebRequestTexture.GetTexture(ObjectAr.url_image))
        {
            // Wait for download to complete
            yield return uwr.SendWebRequest();

            if (uwr.isNetworkError || uwr.isHttpError)
            {
                Debug.Log(uwr.error);
                Debug.LogWarning("Esto aparece cuando se cambia de canal en medio de la corutina");
                yield break;
            }

            // assign texture
            var texture = DownloadHandlerTexture.GetContent(uwr);
            image.overrideSprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), Vector2.one * 0.5f);
        }
    }
}
