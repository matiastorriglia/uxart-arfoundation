﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ARBillboard : MonoBehaviour
{

    Transform arCamera;

    // Start is called before the first frame update
    void Start()
    {
        GameObject arc = GameObject.Find("AR Root");
        if (arc)
            arCamera = arc.transform;
    }

    // Update is called once per frame
    void Update()
    {
        if (!arCamera)
        {
            GameObject arc = GameObject.Find("AR Root");
            if (arc)
                arCamera = arc.transform;
        }

        if (arCamera)
        {
            transform.LookAt(arCamera);
        }
    }
}
