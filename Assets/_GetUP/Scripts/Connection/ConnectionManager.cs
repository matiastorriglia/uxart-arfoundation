﻿using System.Collections.Generic;
using UnityEngine;
using System;
using BestHTTP.SocketIO;
using UnityEngine.SceneManagement;
using System.Net;
using System.IO;
using Newtonsoft.Json;

public struct CommandEmitDB
{
    public string eventName;
    public string args;
    public bool IsSpecialEmit;

    public CommandEmitDB(string eventName, string args, bool IsSpecialEmit)
    {
        this.eventName = eventName;
        this.args = args;
        this.IsSpecialEmit = IsSpecialEmit;
    }
}

public class ConnectionManager : MonoBehaviour
{
    public static ConnectionManager instance;
    [TextArea]
    public string URL = "http://184.173.27.14/plesk-site-preview/udar.app/184.173.27.14/socket.io/";
    [TextArea]
    public string storageURL = "https://uxarcloudstorage-std.s3.us-south.cloud-object-storage.appdomain.cloud/";
    public SocketManager manager;
    private SocketOptions options;

    public ChannelManager channelManager;
    public MarkerManager markerManager;
    public ObjectsArManager objectsArManager;
    public LocalizationManager localizationManager;
    public DashboardInfoManager dashboardInfoManager;
    public SharingManager sharingManager;
    public UserManager userManager;

    public Queue<CommandEmitDB> commandsQueue = new Queue<CommandEmitDB>();
    [Header("Debug")]

    [SerializeField] public bool EmitCommandInProgress = false;
    [SerializeField] public bool SpecialEmitCommandInProgress = false;

    public bool CommandsOnQueue {
        get {
            return commandsQueue.Count != 0;
        }
    }

    private string sessionid;

    private void Awake()
    {
        if (instance != null)
        {
            DestroyImmediate(gameObject);
            return;
        }
        instance = this;
        DontDestroyOnLoad(gameObject);

        SocketManager manager = CreateSocketRef();

        //Setteo de llamadas al js
        manager.Socket.On("_recibe_status", SocketOnCallback);

        manager.Socket.On("_receive_userLogIn", OnReceiveUserLogIn);
        manager.Socket.On("_receive_userSetPref", OnReceiveUserSetPref);
        manager.Socket.On("_receive_userRegister", OnReceiveUserRegister);
        manager.Socket.On("_receive_userUnregister", OnReceiveUserUnregister);

        manager.Socket.On("_receive_canales", OnReceiveChannel);
        manager.Socket.On("_receive_markers", OnReceiveMarker);
        manager.Socket.On("_receive_targets", OnReceiveTargets);
        manager.Socket.On("_receive_geolocationsAr", OnReceiveGeoLocationsAr);
        manager.Socket.On("_receive_geolocationsInfo", OnReceiveGeoLocationsInfo);
        manager.Socket.On("_receive_portalsAr", OnReceivePortalsAr);
        manager.Socket.On("_receive_localization", OnReceiveLocalization);
        manager.Socket.On("_receive_dashboardInfo", OnReceiveDashboardInfo);
        manager.Socket.On("_receive_dashboardId", OnReceiveDashboardId);
        manager.Socket.On("_receive_uploadfeed", OnReceiveUploadFeed);
        manager.Socket.On("_receive_downloadfeed", OnReceiveDownloadFeed);
        manager.Socket.On("_receive_userName", OnReceiveUserName);
        manager.Socket.On("_receive_feedResponse", OnReceiveFeedResponse);
        manager.Socket.On("debug_emit", OnReceiveDebugEmit);

        //DownloadCredential("ftp://uxarplesk@184.173.27.14/httpdocs/motor/cert-mongodb.pem");
    }

    public SocketManager CreateSocketRef()
    { //Crea un socket(manager) para comunicarse con el servidor
        TimeSpan miliSecForReconnect = TimeSpan.FromMilliseconds(500);

        options = new SocketOptions();
        options.ReconnectionAttempts = 200;
        options.AutoConnect = true;
        options.ReconnectionDelay = miliSecForReconnect;
        options.Timeout = TimeSpan.FromMilliseconds(200000);
        options.ConnectWith = BestHTTP.SocketIO.Transports.TransportTypes.WebSocket;

        manager = new SocketManager(new Uri(URL), options); //Ingresa URL y opciones de Socket

        return manager;
    }

    //private void DownloadCredential(string URI)
    //{
    //    //create request
    //    FtpWebRequest result = (FtpWebRequest)FtpWebRequest.Create(URI);
    //    result.Method = WebRequestMethods.Ftp.DownloadFile;
    //    //Set the login details
    //    result.Credentials = new System.Net.NetworkCredential("uxarplesk", "043Vlmt@");

    //    FtpWebResponse response = (FtpWebResponse)result.GetResponse();

    //    Stream responseStream = response.GetResponseStream();
    //    StreamReader reader = new StreamReader(responseStream);
    //    print(reader.ReadToEnd());

    //    print($"Download Complete, status {response.StatusDescription}");

    //    reader.Close();
    //    response.Close();
    //}

    private float EmitProgressTimer = 0;
    private void Update()
    {
        CheckForEmitCommands();

        //if (EmitCommandInProgress && SpecialEmitCommandInProgress)
        //{
        //    EmitProgressTimer += Time.unscaledDeltaTime;
        //    if (EmitProgressTimer > 5)
        //    {
        //        Debug.LogWarning("Expesion de codigo finalizado");
        //        EmitProgressTimer = 0;
        //        EmitCommandInProgress = false;
        //        SpecialEmitCommandInProgress = false;
        //    }
        //}
    }

    //On Packet Recieve -----------------------------------------------------------

    public void OnReceiveDebugEmit(Socket socket, Packet packet, params object[] args)
    {
        Debug.Log(packet.RemoveEventName(true));
    }

    public void SocketOnCallback(Socket socket, Packet packet, params object[] args)
    {
        //Cuando se pierde la conexion en medio de un Emit, esto se ocupa de liberarlo por una orden desde el servidor
        //Para ver la orden, buscar en motor_udar.js del backend la accion "uploadFeed", dentro del try se envia el emit de reconeccion
        print("Nuevo: " + packet.RemoveEventName(true));
        print("Anterior: " + sessionid);
        if (sessionid!= packet.RemoveEventName(true))
        {
            EmitCommandInProgress = false;
            sessionid = packet.RemoveEventName(true);
        }

    }

    public void OnReceiveChannel(Socket socket, Packet packet, params object[] args)
    {
        channelManager = ChannelManager.instance;
        string resulting = packet.RemoveEventName(true);
        Debug.Log(resulting);
        channelManager.SetChannels(resulting);
        EmitCommandInProgress = false;
    }

    public void OnReceiveMarker(Socket socket, Packet packet, params object[] args)
    {
        markerManager = MarkerManager.instance;
        string resulting = packet.RemoveEventName(true);
        Debug.Log(resulting);
        markerManager.SetMarkers(resulting);
        EmitCommandInProgress = false;
    }

    public void OnReceiveTargets(Socket socket, Packet packet, params object[] args)
    {
        markerManager = MarkerManager.instance;
        string resulting = packet.RemoveEventName(true);
        Debug.Log(resulting);
        markerManager.SetTargets(resulting);
        EmitCommandInProgress = false;
    }

    public void OnReceiveGeoLocationsAr(Socket socket, Packet packet, params object[] args)
    {
        objectsArManager = ObjectsArManager.instance;
        string resulting = packet.RemoveEventName(true);
        Debug.Log(resulting);
        objectsArManager.SetGeolocationsAr(resulting);
        EmitCommandInProgress = false;
    }

    public void OnReceiveGeoLocationsInfo(Socket socket, Packet packet, params object[] args)
    {
        objectsArManager = ObjectsArManager.instance;
        string resulting = packet.RemoveEventName(true);
        Debug.Log(resulting);
        objectsArManager.SetGeolocationsInfo(resulting);
        EmitCommandInProgress = false;
    }

    public void OnReceivePortalsAr(Socket socket, Packet packet, params object[] args)
    {
        objectsArManager = ObjectsArManager.instance;
        string resulting = packet.RemoveEventName(true);
        Debug.Log(resulting);
        objectsArManager.SetPortalsAr(resulting);
        EmitCommandInProgress = false;
    }

    public void OnReceiveLocalization(Socket socket, Packet packet, params object[] args)
    {
        localizationManager = LocalizationManager.instance;
        string resulting = packet.RemoveEventName(true);
        Debug.Log(resulting);
        localizationManager.SetLanguageDictionary(resulting);
        EmitCommandInProgress = false;
    }

    public void OnReceiveUserLogIn(Socket socket, Packet packet, params object[] args)
    {
        userManager = UserManager.instance;
        string resulting = packet.RemoveEventName(true);
        Debug.Log(resulting);
        FirebaseUserManager.instance.SetUser(resulting);
        EmitCommandInProgress = false;
    }

    public void OnReceiveUserSetPref(Socket socket, Packet packet, params object[] args)
    {
        userManager = UserManager.instance;
        string resulting = packet.RemoveEventName(true);
        Debug.Log(resulting);
        FirebaseUserManager.instance.GetUserPrefSetResponse(resulting);
        EmitCommandInProgress = false;
    }

    public void OnReceiveUserRegister(Socket socket, Packet packet, params object[] args)
    {
        userManager = UserManager.instance;
        string resulting = packet.RemoveEventName(true);
        Debug.Log(resulting);
        FirebaseUserManager.instance.RegisterUserResponse(resulting);
        EmitCommandInProgress = false;
    }

    public void OnReceiveUserUnregister(Socket socket, Packet packet, params object[] args)
    {
        userManager = UserManager.instance;
        string resulting = packet.RemoveEventName(true);
        Debug.Log(resulting);
        FirebaseUserManager.instance.UnregisterUserResponse(resulting);
        EmitCommandInProgress = false;
    }

    public void OnReceiveDashboardId(Socket socket, Packet packet, params object[] args)
    {
        dashboardInfoManager = DashboardInfoManager.instance;
        string resulting = packet.RemoveEventName(true);

        DashboardInfoPacket dashboardInfoPacket = JsonConvert.DeserializeObject<DashboardInfoPacket>(resulting);
        Debug.Log(resulting);
        if (dashboardInfoPacket.result!=null && dashboardInfoPacket.result.aviso)
        {
            ConnectionManager.instance.AddEmitCommand("actions_dashboard", "{'accion':'lookUpDashboardInfo','dashboard_id':'" + dashboardInfoPacket.result.id + "', 'devolucion':'_receive_dashboardInfo'}");
        }
        EmitCommandInProgress = false;
    }

    public void OnReceiveDashboardInfo(Socket socket, Packet packet, params object[] args)
    {
        dashboardInfoManager = DashboardInfoManager.instance;
        string resulting = packet.RemoveEventName(true);
        Debug.Log(resulting);
        dashboardInfoManager.SetDashboardInfo(resulting);
        EmitCommandInProgress = false;
    }

    public void OnReceiveUploadFeed(Socket socket, Packet packet, params object[] args)
    {
        //dashboardInfoManager = DashboardInfoManager.instance;
        string resulting = packet.RemoveEventName(true);
        Debug.Log(resulting);
        //dashboardInfoManager.SetDashboardInfo(resulting);
        EmitCommandInProgress = false;
    }

    public void OnReceiveDownloadFeed(Socket socket, Packet packet, params object[] args)
    {

        //print(socket.Namespace);
        //print(socket.Manager);
        //print(socket.Manager);

        //print(socket.AutoDecodePayload);
        //print(socket.Id);

        //print("////");
        //print(packet.Namespace);
        //print(socket.Manager);
        //print(socket.Id);


        sharingManager = SharingManager.instance;
        string resulting = packet.RemoveEventName(true);
        Debug.Log(resulting);
        SharingManager.instance.SetFeedContent(resulting);
        EmitCommandInProgress = false;
    }

    public void OnReceiveUserName(Socket socket, Packet packet, params object[] args)
    {
        sharingManager = SharingManager.instance;
        string resulting = packet.RemoveEventName(true);
        Debug.Log(resulting);
        SharingManager.instance.AddUserToDictionary(resulting);
        EmitCommandInProgress = false;
    }

    public void OnReceiveFeedResponse(Socket socket, Packet packet, params object[] args)
    {
        sharingManager = SharingManager.instance;
        string resulting = packet.RemoveEventName(true);
        Debug.Log(resulting);
        EmitCommandInProgress = false;
    }


    // -----------------------------------------------------------

    public void AddEmitCommand(string eventName, string args, bool IsSpecialEmit = false)
    {
        commandsQueue.Enqueue(new CommandEmitDB(eventName, args, IsSpecialEmit));
    }


    private void CheckForEmitCommands()
    {

        if (!EmitCommandInProgress && CommandsOnQueue)
        {
            var commandDB = commandsQueue.Dequeue();
            EmitCommandInProgress = true;
            manager.Socket.Emit(commandDB.eventName, commandDB.args);
            SpecialEmitCommandInProgress = commandDB.IsSpecialEmit;

            Debug.Log("Connection command // <color=blue>eventName: " + commandDB.eventName + "</color> // <color=green> args: " + commandDB.args + " </color>");
        }
    }
}
