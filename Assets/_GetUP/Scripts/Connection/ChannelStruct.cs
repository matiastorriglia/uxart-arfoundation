﻿using System.Collections.Generic;
using System;


public class ChannelInfo
{
    public string language { get; set; }
    public string name { get; set; }
    public string info { get; set; }
}
public class ChannelLocation {
    public string location { get; set; }
    public string locationName { get; set; }
}
public class ChannelData { //Los datos del canal
    public string _id { get; set; }
    public string url_image { get; set; }
    public string url_bigImage { get; set; }
    public List<ChannelInfo> languageInfo { get; set; }
    public List<ChannelLocation> locations { get; set; }
    public double range { get; set; }
    public bool enable { get; set; }
    public DateTime date_created { get; set; }
}

//El contenido
public class ChannelResult {
    public bool aviso { get; set; }
    public string mensaje { get; set; }
    public List<ChannelData> result_canal { get; set; }
}

//El paquete que devuelve el .js
public class ChannelPacket {
    public object error { get; set; }
    public ChannelResult result { get; set; }
}



