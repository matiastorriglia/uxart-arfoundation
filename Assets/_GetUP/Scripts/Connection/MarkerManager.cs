﻿using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.Networking;
using Vuforia;
using UnityEngine.SceneManagement;

public class MarkerManager : MonoBehaviour {
    public static MarkerManager instance;

    public MarkerDatabase currentMarkerDatabase;

    public List<UDARTarget> targets;

    public bool ThereAreMarkers;

    private void Awake() {
        if (instance != null) {
            DestroyImmediate(gameObject);
            return;
        }
        instance = this;
        DontDestroyOnLoad(gameObject);
    }

    public void SetMarkers(string resulting) {
        MarkerDatabasePacket markerDatabasePacket = JsonConvert.DeserializeObject<MarkerDatabasePacket>(resulting);
        currentMarkerDatabase = markerDatabasePacket.result;
        ThereAreMarkers = currentMarkerDatabase.aviso == "true" ? true : false;
        Debug.Log("Markers setted");
        if (SceneManager.GetActiveScene().name == "Markers") {
            GameManager.instance.CargarMarcadores();
        }
    }

    public void OnDatasetLoad() {
        ConnectionManager.instance.AddEmitCommand("actions_markers", "{'accion':'lookUpTargets','dataset':'" + currentMarkerDatabase.name + "', 'devolucion':'_receive_targets'}");
    }

    public void SetTargets(string resulting) {
        UDARTargetPacket targetPacket = JsonConvert.DeserializeObject<UDARTargetPacket>(resulting);
        targets = targetPacket.result.result_targets;
        if (targets==null)
        {
            Debug.LogError(targetPacket.result.mensaje);
        }
        foreach (UDARTarget t in targets) {
            Debug.Log(t.prefabName);
            StartCoroutine(CreateTarget(t));
        }
        LoadingCanvasController.instance.ProcesosEnCurso = 0;
    }


    IEnumerator CreateTarget(UDARTarget target) {
        yield return new WaitWhile(() => AssetBundleManager.instance.ObtainAssetBundle(target.url_assetBundle, target.bundleName) == null);

        AssetBundle bundle = AssetBundleManager.instance.ObtainAssetBundle(target.url_assetBundle, target.bundleName);

        GameObject prefab = bundle.LoadAsset(target.prefabName) as GameObject; //Creo el prefab
        GameObject iPrefab = Instantiate(prefab, transform.position, Quaternion.identity);

        //Debug.LogWarning(iPrefab.GetComponent<UARImageTarget>().database);
        //Debug.LogWarning(iPrefab.GetComponent<UARImageTarget>().imageTarget);

        IEnumerable<TrackableBehaviour> trackableBehaviours = TrackerManager.Instance.GetStateManager().GetTrackableBehaviours();

        // Loop over all TrackableBehaviours.
       
        foreach (TrackableBehaviour trackableBehaviour in trackableBehaviours) {
            //Debug.LogWarning(trackableBehaviour.TrackableName + "////" + iPrefab.GetComponent<UARImageTarget>().imageTarget);
            if (trackableBehaviour.TrackableName.CompareTo(iPrefab.GetComponent<UARImageTarget>().imageTarget) == 0) {
                //Debug.LogWarning("TrackableBehaviour: " + trackableBehaviour);
                GameObject go = trackableBehaviour.gameObject;
                go.AddComponent<DefaultTrackableEventHandler>();
                iPrefab.transform.SetParent(trackableBehaviour.transform);
                iPrefab.GetComponent<UARImageTarget>().mTrackableBehaviour = trackableBehaviour;
                iPrefab.GetComponent<UARImageTarget>().AssignComponents();
                Canvas prefabCanvas = iPrefab.GetComponent<Canvas>();
                if (prefabCanvas)
                    prefabCanvas.worldCamera = MarkerPanelManager.instance.MarkerCamera;

                iPrefab.transform.localPosition = new Vector3(0, 0, 0);
            }
        }

    }
}