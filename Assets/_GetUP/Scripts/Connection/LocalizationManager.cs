﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using Newtonsoft.Json;
using UnityEngine.UI;


/*Este script se encarga de cargar archivos de idioma en la app*/
public class LocalizationManager : MonoBehaviour {
    public static LocalizationManager instance;

    public SystemLanguage currentLanguage = SystemLanguage.Unknown;
    [SerializeField] private bool ForceLanguage;
    [SerializeField] private Dropdown languageDropdown;

    //public Text errtext;
    private Dictionary<string, string> localizedText = new Dictionary<string, string>();
    public bool isReady = false;
    private string missingTextString = "not found";
    private string filename;

    public delegate void ChangeLanguage();
    public static event ChangeLanguage OnChangeLanguage;

    // Use this for initialization

    private void Awake() {
        if (instance != null) {
            DestroyImmediate(gameObject);
            return;
        }
        instance = this;
        DontDestroyOnLoad(gameObject);
    }

    //Busca la palabra traducida, en caso de que no exista, deja el texto que estaba 
    public string GetLocalizedValue(string key) {
        string result = key;
        if (localizedText.ContainsKey(key)) {
            result = localizedText[key];
        }
        return result;
    }

    public void SetLanguageOptions(SystemLanguage language) {
        if (ForceLanguage) {
            language = currentLanguage;
        }
        isReady = false;
        ConnectionManager.instance.AddEmitCommand("actions_localization", "{'accion':'lookUpLocalization','language':'" + language.ToString() + "','devolucion':'_receive_localization'}");
        currentLanguage = language;
    }

    public void SetLanguageDictionary(string resulting) {
        LocalizationPacket LocalizationPacket = JsonConvert.DeserializeObject<LocalizationPacket>(resulting);
        if (LocalizationPacket.result.result_language == null) {
            Debug.LogError("No esta el idioma en la base de datos");
        } else {
            localizedText.Clear();
            foreach (var item in LocalizationPacket.result.result_language[0].items) {
                localizedText.Add(item.key, item.value);
            }
            OnChangeLanguage?.Invoke();

            isReady = true;
        }

        Debug.Log("Localization setted");
    }

}
