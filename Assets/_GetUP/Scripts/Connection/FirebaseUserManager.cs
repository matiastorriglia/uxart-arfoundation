using System.Collections;
using System.Collections.Generic;
using Doozy.Engine.UI;
using UnityEngine.UI;

using System.Threading.Tasks;
//using Facebook.Unity;
using Google;

using Newtonsoft.Json;
using Firebase.Auth;
using UnityEngine;
using UnityEngine.Networking;


public class FirebaseUserManager : MonoBehaviour
{

    public static FirebaseUserManager instance;

    [Header("Drawers")]
    [SerializeField] private UIDrawer feedDrawer;
    [SerializeField] private UIDrawer logDrawer;
    [SerializeField] private UIDrawer userDrawer;

    [Header("UserUI")]
    [SerializeField] private Text nameText;
    [SerializeField] private Dropdown languageDropdown;
    [SerializeField] private Image userPhoto;

    [Header("Others")]
    [SerializeField] private Button CameraButton;
    public string webClientId = "<your client id here>";

    private GoogleSignInConfiguration configuration;

    [SerializeField]
    public UserData currentUser = new UserData(); //Inicializado con todos los valores en null
    private UserData newUser = new UserData(); //Usado para las registraciones

    public FirebaseAuth auth;

    private bool PressedUserIcon = false;

    private void Awake() {
        if (instance != null) {
            DestroyImmediate(instance.gameObject);
            return;
        }

        instance = this;
        DontDestroyOnLoad(gameObject);

        auth = FirebaseAuth.DefaultInstance;

        //if (!FB.IsInitialized) {
        //    FB.Init();
        //} else {
        //    FB.ActivateApp();
        //}

        configuration = new GoogleSignInConfiguration {
            WebClientId = webClientId,
            RequestIdToken = true,
            RequestEmail = true
        };

        auth.IdTokenChanged += Auth_IdTokenChanged;
    }

    private bool FirstLog = false;
    private void Auth_IdTokenChanged(object sender, System.EventArgs e) {

        if (auth.CurrentUser != null && !FirstLog) {
            //if (auth.CurrentUser.Email == "") {
            //    if (emailAux == "") {
            //        OnSignIn();
            //        return;
            //    } else {
            //        LogIn(emailAux);
            //    }
            //} else {
                LogIn();
            //}
            FirstLog = true;
            Debug.LogWarning("Current User */*/*/*/: " + auth.CurrentUser.Email);
        }
    }

    public void OnUserPressed() {
        PressedUserIcon = true;
        if (auth.CurrentUser != null) {
            feedDrawer.Toggle();
        } else {
            logDrawer.Toggle();
        }
    }

    #region Facebook
    //public void FacebookLogIn() {
    //    FB.LogInWithReadPermissions(callback: FacebookOnLogIn);
    //}

    //private void FacebookOnLogIn(ILoginResult result) {
    //    if (FB.IsLoggedIn) {
    //        AccessToken tocken = AccessToken.CurrentAccessToken;
    //        Credential credential = FacebookAuthProvider.GetCredential(tocken.TokenString);
    //        accessToken(credential);
    //    } else {
    //        Debug.Log("Login Failed");
    //    }
    //}
    #endregion

    #region Google

    public void OnSignIn() {
        GoogleSignIn.Configuration = configuration;
        GoogleSignIn.Configuration.UseGameSignIn = false;
        GoogleSignIn.Configuration.RequestIdToken = true;
        GoogleSignIn.Configuration.RequestEmail = true;
        AddStatusText("Calling SignIn");

        GoogleSignIn.DefaultInstance.SignIn().ContinueWith(
          OnAuthenticationFinished);
    }

    public void OnSignOut() {
        AddStatusText("Calling SignOut");
        GoogleSignIn.DefaultInstance.SignOut();
    }

    public void OnDisconnect() {
        AddStatusText("Calling Disconnect");
        GoogleSignIn.DefaultInstance.Disconnect();
    }

    private string emailAux;
    internal void OnAuthenticationFinished(Task<GoogleSignInUser> task) {
        if (task.IsFaulted) {
            using (IEnumerator<System.Exception> enumerator =
                    task.Exception.InnerExceptions.GetEnumerator()) {
                if (enumerator.MoveNext()) {
                    GoogleSignIn.SignInException error =
                            (GoogleSignIn.SignInException)enumerator.Current;
                    AddStatusText("Got Error: " + error.Status + " " + error.Message);
                } else {
                    AddStatusText("Got Unexpected Exception?!?" + task.Exception);
                }
            }
        } else if (task.IsCanceled) {
            AddStatusText("Canceled");
        } else {
            AddStatusText("Welcome: " + task.Result.DisplayName + "!");
            AddStatusText("Your Email: " + task.Result.Email + "!");
            emailAux = task.Result.Email;
            Credential credential = GoogleAuthProvider.GetCredential(task.Result.IdToken, null);
            accessToken(credential);
        }
    }

    private List<string> messages = new List<string>();
    void AddStatusText(string text) {
        if (messages.Count == 5) {
            messages.RemoveAt(0);
        }
        messages.Add(text);
        string txt = "";
        foreach (string s in messages) {
            txt += "\n" + s;
        }
        Debug.Log(text);
    }

    #endregion

    public void accessToken(Credential firebaseResult) {
        auth = FirebaseAuth.DefaultInstance;

        auth.SignInWithCredentialAsync(firebaseResult).ContinueWith(task => {
            if (task.IsCanceled) {
                Debug.LogError("SignInWithCredentialAsync was canceled.");
                return;
            }
            if (task.IsFaulted) {
                Debug.LogError("SignInWithCredentialAsync encountered an error: " + task.Exception);
                return;
            }
        });
    }

    private void LoadUserDataDrawer() {
        if (PressedUserIcon) {
            feedDrawer.Toggle();
            userDrawer.Toggle();
        }
        Debug.LogFormat("User signed in successfully: {0} ({1}) {2}", auth.CurrentUser.DisplayName, auth.CurrentUser.UserId, auth.CurrentUser.Email);
        StartCoroutine(PhotoDownload(auth.CurrentUser.PhotoUrl));
    }

    public IEnumerator PhotoDownload(System.Uri url) {
        if (url == null) {
            Debug.LogError("El url esta vacio", gameObject);
            yield break;
        }

        using (UnityWebRequest uwr = UnityWebRequestTexture.GetTexture(url)) {
            // Wait for download to complete
            yield return uwr.SendWebRequest();

            if (uwr.isNetworkError || uwr.isHttpError) {
                Debug.Log(uwr.error);
            }

            // assign texture
            var texture = DownloadHandlerTexture.GetContent(uwr);
            userPhoto.overrideSprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), Vector2.one * 0.5f);
            userPhoto.preserveAspect = true;
        }
    }

    public void LogOut() {
        PressedUserIcon = false;
        FirstLog = false;
        auth.SignOut();
        feedDrawer.Close();
        //CameraButton.interactable = false;
        userDrawer.Close();
        nameText.text = "";
        userPhoto.overrideSprite = Sprite.Create(null, new Rect(), Vector2.zero);
        currentUser = new UserData();
        emailAux = "";

        if (Application.systemLanguage == SystemLanguage.English || Application.systemLanguage == SystemLanguage.Spanish)
            GameManager.instance.setLanguageDropdown(Application.systemLanguage.ToString());
        else
            GameManager.instance.setLanguageDropdown(SystemLanguage.English.ToString());
    }

    public void LogIn() {
        ConnectionManager.instance.AddEmitCommand("actions_users", "{'accion':'loginUsuario','email':'" + auth.CurrentUser.Email + "','password':'" + auth.CurrentUser.UserId + "','devolucion':'_receive_userLogIn'}");
    }
    public void LogIn(string email) {
        ConnectionManager.instance.AddEmitCommand("actions_users", "{'accion':'loginUsuario','email':'" + email + "','password':'" + auth.CurrentUser.UserId + "','devolucion':'_receive_userLogIn'}");
    }

    public void UnregisterUser() {
        ConnectionManager.instance.AddEmitCommand("actions_users", "{'accion':'unregisterUser','_id':'" + currentUser._id + "','devolucion':'_receive_userUnregister'}");
    }

    private string registerEmail;
    public void RegisterUser() {
        registerEmail = auth.CurrentUser.Email;

        ConnectionManager.instance.AddEmitCommand("actions_users", "{'accion':'createUser" +
            "','name':'" + auth.CurrentUser.DisplayName +
            "','sex':'" + "" +
            "','photo':'" + auth.CurrentUser.PhotoUrl +
            "','email':'" + registerEmail +
            "','password':'" + auth.CurrentUser.UserId +
            "','language':'" + Application.systemLanguage.ToString() +
            "','channels':'" + "" +
            "','enable':'" + true +
            "','devolucion':'_receive_userRegister'}");
    }

    public void UpdateUser() {
        ConnectionManager.instance.AddEmitCommand("actions_users", "{'accion':'updatePrefUser" +
            "','_id':'" + currentUser._id +
            "','name':'" + currentUser.name +
            "','sex':'" + currentUser.sex +
            "','photo':'" + currentUser.photo +
            "','email':'" + currentUser.email +
            "','password':'" + currentUser.password +
            "','language':'" + currentUser.language +
            "','channels':'" + currentUser.channels +
            "','enable':'" + currentUser.enable +
            "','devolucion':'_receive_userSetPref'}");
    }

    public void SetUser(string resulting) {
        UserPacket UserPacket = JsonConvert.DeserializeObject<UserPacket>(resulting);
        if (!UserPacket.result.aviso) {
            RegisterUser(); //No existe, por lo que lo crea
            Debug.Log("User not exist, creating it");
            return;
        } else {
            currentUser = UserPacket.result.result_users[0];
            GameManager.instance.setLanguageDropdown(currentUser.language);
            nameText.text = currentUser.name;
            //CameraButton.interactable = true;
            LoadUserDataDrawer();
            Debug.Log("User Applied");
        }
    }

    public void RegisterUserResponse(string resulting) {
        UserPacket UserPacket = JsonConvert.DeserializeObject<UserPacket>(resulting);
        Debug.Log(UserPacket.result.mensaje);
        if (UserPacket.result.aviso) {
            LogIn(); //Usa el auth ya que se uso en la creacion del usuario que acaba de funcionar
            Debug.Log("User Created");
            //regInfoText.text = "User created succesfully";
        } else {
            Debug.Log("User coulnd't be created");
            LogOut();
            //regInfoText.text = "Mail already in use or an error ocurred";
        }
    }

    public void UnregisterUserResponse(string resulting) {
        UserPacket UserPacket = JsonConvert.DeserializeObject<UserPacket>(resulting);

        if (UserPacket.result.aviso) {
            //infoText.text = UserPacket.result.mensaje;
            LogOut();
        }
    }

    public void GetUserPrefSetResponse(string resulting) {
        UserPacket UserPacket = JsonConvert.DeserializeObject<UserPacket>(resulting);
        Debug.Log(UserPacket.result.mensaje);
    }

}
