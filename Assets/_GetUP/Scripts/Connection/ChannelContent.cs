﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;


public class ChannelContent : MonoBehaviour {
    HorizontalLayoutGroup layoutGroup;

    RectTransform rectTransform;

    public GameObject previousButton;

    public GameObject nextButton;

    public int timeToChangeChannel = 1;

    public float lastUpdate;

    bool Changing;

    [SerializeField] private int currentIndex;
    public int CurrentIndex {
        get { return currentIndex; }
        set {
            if (value < 0) {
                return;
            }
            currentIndex = value;
            ChannelManager.instance.SelectChannel(transform.GetChild(currentIndex));
        }
    }

    // Start is called before the first frame update
    void Start() {
        layoutGroup = GetComponent<HorizontalLayoutGroup>();
        rectTransform = GetComponent<RectTransform>();
        Changing = false;
    }

    // Update is called once per frame
    void Update() {

    }

    public void NextChannel() {
        float width = transform.GetChild(0).GetComponent<RectTransform>().sizeDelta.x;
        float finalPos = -(width + layoutGroup.spacing) * (transform.childCount - 1);
        Vector2 position = rectTransform.anchoredPosition;
        if (!Changing && position.x > finalPos) {
            Changing = true;

            float increment = rectTransform.anchoredPosition.x - (transform.GetChild(0).GetComponent<RectTransform>().sizeDelta.x + layoutGroup.spacing);
            DOTween.To(() => rectTransform.anchoredPosition, x => rectTransform.anchoredPosition = x, new Vector2(increment, rectTransform.anchoredPosition.y), 1);
            CurrentIndex = -(Mathf.FloorToInt(increment / (transform.GetChild(0).GetComponent<RectTransform>().sizeDelta.x + layoutGroup.spacing)) + 1);

            StartCoroutine(ChangingCourutine(timeToChangeChannel));
        }
    }

    public void PreviousChannel() {
        float width = transform.GetChild(0).GetComponent<RectTransform>().sizeDelta.x;
        float initialPos = -width + (layoutGroup.spacing / 2);
        Vector2 position = rectTransform.anchoredPosition;
        if (!Changing && CurrentIndex - 1 >= 0 && position.x < initialPos) {
            Changing = true;

            float increment = rectTransform.anchoredPosition.x + (transform.GetChild(0).GetComponent<RectTransform>().sizeDelta.x + layoutGroup.spacing);
            DOTween.To(() => rectTransform.anchoredPosition, x => rectTransform.anchoredPosition = x, new Vector2(increment, rectTransform.anchoredPosition.y), 1);
            CurrentIndex = -(Mathf.FloorToInt(increment / (transform.GetChild(0).GetComponent<RectTransform>().sizeDelta.x + layoutGroup.spacing)) + 1);

            StartCoroutine(ChangingCourutine(timeToChangeChannel));
        }
    }

    private IEnumerator ChangingCourutine(float delay) {
        yield return new WaitForSeconds(delay);
        Changing = false;
    }

    public void OnScroll(Vector2 value) {
        lastUpdate = Time.time;
        Changing = false;
    }

    //public void OnMouseUp() {
    //    changed = true;
    //    int index = -(Mathf.FloorToInt(rectTransform.anchoredPosition.x / (transform.GetChild(0).GetComponent<RectTransform>().sizeDelta.x + layoutGroup.spacing)) + 1);
    //    float position = -transform.GetChild(0).GetComponent<RectTransform>().sizeDelta.x - (transform.GetChild(0).GetComponent<RectTransform>().sizeDelta.x + layoutGroup.spacing) * index + layoutGroup.spacing / 2;
    //    DOTween.To(() => rectTransform.anchoredPosition, x => rectTransform.anchoredPosition = x, new Vector2(position, rectTransform.anchoredPosition.y), 0);
    //    CurrentIndex = -(Mathf.FloorToInt(position / (transform.GetChild(0).GetComponent<RectTransform>().sizeDelta.x + layoutGroup.spacing)) + 1);
    //}


}
