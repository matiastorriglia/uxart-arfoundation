﻿using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

using Mapbox.Unity.Location;
using Mapbox.Unity.Utilities;
using Mapbox.Utils;

using Doozy.Engine.UI;
using Doozy.Engine.Extensions;

public class ChannelManager : MonoBehaviour
{
    public static ChannelManager instance;

    public GameObject channelLowerSwipePrefab;
    public RectTransform lowerSwipeContent;
    public UIDrawer lowerDrawer;

    public List<GameObject> channels;

    public List<ChannelData> channelsData;
    public List<ChannelData> channelsOnRange = new List<ChannelData>();

    private ChannelData currentChannel;

    public string currentChannelId;

    public MarkerManager markerManager;
    public ObjectsArManager objectsArManager;

    public ChannelData CurrentChannel {
        get {
            return currentChannel;
        }
        set {
            if (value != null && channelsData.Contains(value))
            {
                currentChannel = value;
                currentChannelId = value._id;
                RefreshFeaturesByChannel(value);
            }
            else
            {
                Debug.Log("No existe el canal dado");
            }
        }
    }

    private void Awake()
    {
        if (instance != null)
        {
            DestroyImmediate(gameObject);
            return;
        }
        instance = this;
        DontDestroyOnLoad(gameObject);
    }


    // Start is called before the first frame update
    void Start()
    {
        ConnectionManager.instance.AddEmitCommand("actions_channels", "{'accion':'loadChannels','devolucion':'_receive_canales'}");

        channelsData = new List<ChannelData>();
        channels = new List<GameObject>();
    }

    //private void Update()
    //{
    //    CheckCloseChannels();
    //}

    public void SetChannels(string resulting)
    {
        ChannelPacket channelPacket = JsonConvert.DeserializeObject<ChannelPacket>(resulting);

        foreach (ChannelData channelData in channelPacket.result.result_canal)
        {
            SetChannelsOnSlider(channelData);
            SetChannelsOnSwipe(channelData);

            //var button = channel.GetComponent<Button>();
            //button.onClick.AddListener(() => SelectChannel(channel.transform));

            if (channelsData.Count == 0)
            {
                currentChannel = channelData;
                currentChannelId = channelData._id;
            }
            channelsData.Add(channelData);
        }

        //LocalizationManager.instance.PopulateDictionaryCaller();
        if (channelsData.Count > 0)
        {
            RefreshFeaturesByChannel(currentChannel);
        }

        print("Channels Setted");
    }

    private bool firstChannel = true;
    private void SetChannelsOnSlider(ChannelData channelData)
    {
        var channel = Instantiate(UIManagerUDAR.instance.channelPrefab, UIManagerUDAR.instance.channelsContainer);

        foreach (var info in channelData.languageInfo)
        {
            //Cuando encuentra el lenguaje en uso del sistema lo aplica
            if (info.language == LocalizationManager.instance.currentLanguage.ToString())
            {
                channel.name = info.name;
            }
        }

        StartCoroutine(GetImage(channelData.url_image, channel));
        channel.transform.localPosition = Vector3.zero;
        channel.GetComponent<RectTransform>().AnchorMaxToCenter();
        channel.GetComponent<RectTransform>().AnchorMinToCenter();
        channel.GetComponent<RectTransform>().transform.localPosition = Vector3.zero;
        channels.Add(channel);
        if (!firstChannel)
        {
            channel.SetActive(false);
        }
        firstChannel = false;
        //TODO: Que se active unicamente el preferido del usuario
    }

    private void SetChannelsOnSwipe(ChannelData channelData)
    {
        var channel = Instantiate(channelLowerSwipePrefab, lowerSwipeContent).GetComponent<LowerSwipeButton>();

        channel.ChannelData = channelData;

        StartCoroutine(GetImage(channelData.url_bigImage, channel.gameObject));
        channel.transform.localPosition = Vector3.zero;
    }

    //Carga la base de datos que contiene referencias a todoslos objetos del canal
    private void RefreshFeaturesByChannel(ChannelData channel)
    {
        if (channel != null)
        {
            if (PortalManager.instance) { 
                PortalManager.instance.StopAllCoroutines();

                if (PortalManager.instance.first)
                {
                    StartCoroutine(PortalManager.instance.PortalDelay());
                }
            }
            if (GeoManager.instance) { GeoManager.instance.StopAllCoroutines(); }
            if (SharingManager.instance) { SharingManager.instance.CleanFeed(); SharingManager.instance.DownloadFeed(); }

            ConnectionManager.instance.AddEmitCommand("actions_objectsar", "{'accion':'lookUpGeoLocationsAr','channel_id':'" + channel._id + "','devolucion':'_receive_geolocationsAr'}");
            ConnectionManager.instance.AddEmitCommand("actions_objectsar", "{'accion':'lookUpGeoLocationsInfo','channel_id':'" + channel._id + "','devolucion':'_receive_geolocationsInfo'}");
            ConnectionManager.instance.AddEmitCommand("actions_objectsar", "{'accion':'lookUpPortalsAr','channel_id':'" + channel._id + "','devolucion':'_receive_portalsAr'}");
            ConnectionManager.instance.AddEmitCommand("actions_markers", "{'accion':'lookUpMarkers','channel_id':'" + channel._id + "', 'devolucion':'_receive_markers'}");
        }
    }

    public void SelectChannel(Transform channelTransform)
    {
        var selectedChannel = channelsData[channelTransform.GetSiblingIndex()];
        foreach (var channel in channels)
        {
            channel.SetActive(false);
        }
        channels[channelTransform.GetSiblingIndex()].SetActive(true);

        //Debug.Log("Id de canal cargado: " + selectedChannel._id);

        if (PlayerPrefs.GetInt("SecondSolapa") != 1)
        {
            GameManager.instance.solapa.SetActive(true);
            PlayerPrefs.SetInt("SecondSolapa", 1);
        }

        CurrentChannel = selectedChannel;
        Debug.Log(CurrentChannel._id);
    }

    IEnumerator GetImage(string channelDataImage_url, GameObject channel)
    {
        // Start a download of the given URL
        LoadingCanvasController.instance.ProcesosEnCurso++;
        using (UnityWebRequest uwr = UnityWebRequestTexture.GetTexture(channelDataImage_url))
        {
            // Wait for download to complete
            yield return uwr.SendWebRequest();

            if (uwr.isNetworkError || uwr.isHttpError)
            {
                Debug.Log(uwr.error);
            }

            // assign texture
            var texture = DownloadHandlerTexture.GetContent(uwr);

            if (channel.GetComponentInChildren<LowerSwipeButton>())
            {
                channel.GetComponentInChildren<LowerSwipeButton>().channelImage.sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), Vector2.one * 0.5f);
                //channel.GetComponentInChildren<LowerSwipeButton>().channelImage.preserveAspect = true;
            }
            else
            {
                channel.GetComponentsInChildren<Image>()[0].sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), Vector2.one * 0.5f);
                channel.GetComponentsInChildren<Image>()[0].preserveAspect = true;
            }
            channel.transform.localScale = Vector3.one;
            LoadingCanvasController.instance.ProcesosEnCurso--;

        }
    }

    ////Otra implementacion de OneSignal, la idea era dar notificacion cuando un canal estaba serca.
    //private float timer = 2;
    //private void CheckCloseChannels()
    //{
        //timer += Time.deltaTime;
        //if (timer >= 2)
        //{
        //    timer = 0;
        //    foreach (var channel in channelsData)
        //    {

        //        var channelPos = Conversions.StringToLatLon(channel.location);
        //        var currentPos = LocationProviderFactory.Instance.DefaultLocationProvider.CurrentLocation.LatitudeLongitude;

        //        if (!channelsOnRange.Contains(channel) && Vector2d.Distance(currentPos, channelPos) < channel.range)
        //        {
        //            Messagge("El canal: \"" + channel.name + "\" ya esta disponible");
        //            print("El canal: " + channel.name + " ya esta disponible");
        //            channelsOnRange.Add(channel);
        //        }
        //        else if (channelsOnRange.Contains(channel) && Vector2d.Distance(currentPos, channelPos) > channel.range)
        //        {
        //            Messagge("El canal: \"" + channel.name + "\" ya no esta disponible");
        //            print("El canal: " + channel.name + " ya no esta disponible");
        //            channelsOnRange.Remove(channel);
        //        }
        //    }
        //}
    //}

    //private static string oneSignalDebugMessage;
    //public void Messagge(string messageString)
    //{
    //    // Just an example userId, use your own or get it the devices by using the GetPermissionSubscriptionState method
    //    string userId = GameManager.instance.statusNot.subscriptionStatus.userId;

    //    var notification = new Dictionary<string, object>();
    //    notification["contents"] = new Dictionary<string, string>() { { "en", messageString } };
    //    notification["include_player_ids"] = new List<string>() { userId };
    //    // Example of scheduling a notification in the future.
    //    //notification["send_after"] = System.DateTime.Now.ToUniversalTime().AddSeconds(2).ToString("U");

    //    OneSignal.PostNotification(notification, (responseSuccess) =>
    //    {
    //        oneSignalDebugMessage = "Notification posted successful! Delayed by about 30 secounds to give you time to press the home button to see a notification vs an in-app alert.\n" + Json.Serialize(responseSuccess);
    //    }, (responseFailure) =>
    //    {
    //        oneSignalDebugMessage = "Notification failed to post:\n" + Json.Serialize(responseFailure);
    //    });
    //}
}
