﻿using System.Collections.Generic;
using System;

public class PortalInfo
{
    public string language { get; set; }
    public string name { get; set; }
    public string info { get; set; }
}

public class PortalData { //Los datos del canal
    public string _id { get; set; }
    public string url_portalVideo { get; set; }
    public string url_portalImage { get; set; }
    public string url_portalExternal { get; set; }
    public List<PortalInfo> languageInfo { get; set; }
    public string channel_id { get; set; }
    public string bundleType { get; set; }
    public bool enable { get; set; }
    public DateTime date_created { get; set; }
}

//El contenido
public class PortalResult {
    public bool aviso { get; set; }
    public string mensaje { get; set; }
    public List<PortalData> result_objects { get; set; }
}

//El paquete que devuelve el .js
public class PortalPacket {
    public object error { get; set; }
    public PortalResult result { get; set; }
}
