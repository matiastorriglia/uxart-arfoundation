﻿using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
using TMPro;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.UI;
using Doozy.Engine.UI;


public class DashboardInfoManager : MonoBehaviour
{
    public static DashboardInfoManager instance;

    private List<DashboardInfoData> dashboardInfoData = new List<DashboardInfoData>();
    private List<GameObject> allData = new List<GameObject>();

    [SerializeField] private List<GameObject> categories;

    [SerializeField] private GameObject categoryPrefab;
    [SerializeField] private GameObject contentPrefab;

    [SerializeField] private ScrollRect scrollRect;

    [SerializeField] private RectTransform mainContent;

    [SerializeField] private UIView ExplodeViewer;
    [SerializeField] public UIView infoViewer;
    [SerializeField] private UIDrawer lowerDrawer;

    [SerializeField] private AudioSource audioSourceExplode;

    private void Awake()
    {
        if (instance != null)
        {
            DestroyImmediate(gameObject);
            return;
        }
        instance = this;
        DontDestroyOnLoad(gameObject);
    }


    private void Update()
    {
        if (ExplodeViewer.IsVisible)
        {
            ExplodeViewer.Hide();
            infoViewer.Show();
            Snap();
        }
    }

    public NubeController dashboardNube;
    public void HideDashboardInfo()
    {
        infoViewer.Hide();
        lowerDrawer.DetectGestures = true;
        if (dashboardNube == null)
        {
            Debug.LogError("No dashboard assigned");
        }
        else
        {
            dashboardNube.RestartAnims();
        }
    }

    public void SetDashboardInfo(string resulting)
    {
        DashboardInfoPacket dashboardInfoPacket = JsonConvert.DeserializeObject<DashboardInfoPacket>(resulting);

        if (dashboardInfoPacket.result.result_dashboardinfo == null)
        {
            dashboardInfoData.Clear();
            Debug.Log("La nube no tiene informacion");
        }
        else
        {
            dashboardInfoData = dashboardInfoPacket.result.result_dashboardinfo;

            Debug.Log("Aplicando datos del a nube");
            categories = new List<GameObject>();

            foreach (var data in allData)
            {
                Destroy(data);
            }

            allData = new List<GameObject>();

            GameObject lastCategory = null;

            for (int i = 0; i < dashboardInfoData.Count + 2; i++)
            {
                if (i < dashboardInfoData.Count)
                {
                    var category = CategoryExists(dashboardInfoData[i].category);

                    if (!category)
                    {
                        var newCategory = Instantiate(categoryPrefab, mainContent).GetComponent<DashboardPanelTitle>();
                        categories.Add(newCategory.gameObject);

                        newCategory.DashboardInfoData = dashboardInfoData[i];

                        allData.Add(newCategory.gameObject);
                        category = newCategory.gameObject;
                    }

                    var newData = Instantiate(contentPrefab, mainContent).GetComponent<DashboardPanelInfo>();
                    newData.transform.SetSiblingIndex(category.transform.GetSiblingIndex() + 1);

                    newData.DashboardInfoData = dashboardInfoData[i];

                    StartCoroutine(DataImageLoad(newData, dashboardInfoData[i]));
                    allData.Add(newData.gameObject);

                    if (newData.transform.GetSiblingIndex() + 1 < newData.transform.parent.childCount)
                    {
                        if (newData.transform.parent.GetChild(newData.transform.GetSiblingIndex() + 1).GetChild(1).GetChild(0).GetComponent<TextMeshProUGUI>())
                        {
                            //El hijo del padre es una mascara, y dsp de la mascara esta el texto. Si es texto, dar vuelta
                            newData.transform.GetChild(1).GetChild(1).GetComponent<TextMeshProUGUI>().alignment = TextAlignmentOptions.Right;
                            newData.transform.GetChild(0).SetAsLastSibling();
                        }
                    }

                    lastCategory = category;
                }
                else
                {
                    var newData = Instantiate(contentPrefab, mainContent).GetComponent<DashboardPanelInfo>();
                    newData.transform.SetSiblingIndex(i - dashboardInfoData.Count - 2);

                    //newData.transform.GetChild(0).SetAsLastSibling();

                    newData.GetComponent<CanvasGroup>().alpha = 0;
                }

            }

            //foreach (var data in dashboardInfoData)
            //{
            //    var category = CategoryExists(data.category);

            //    if (!category)
            //    {
            //        var newCategory = Instantiate(categoryPrefab, mainContent).GetComponent<DashboardPanelTitle>();
            //        categories.Add(newCategory.gameObject);

            //        newCategory.DashboardInfoData = data;

            //        allData.Add(newCategory.gameObject);
            //        category = newCategory.gameObject;
            //    }

            //    var newData = Instantiate(contentPrefab, mainContent).GetComponent<DashboardPanelInfo>();
            //    newData.transform.SetSiblingIndex(category.transform.GetSiblingIndex() + 1);

            //    newData.DashboardInfoData = data;

            //    StartCoroutine(DataImageLoad(newData, data));
            //    allData.Add(newData.gameObject);

            //    if (newData.transform.GetSiblingIndex() + 1 < newData.transform.parent.childCount)
            //    {
            //        if (newData.transform.parent.GetChild(newData.transform.GetSiblingIndex() + 1).GetChild(1).GetChild(0).GetComponent<TextMeshProUGUI>())
            //        {
            //            //El hijo del padre es una mascara, y dsp de la mascara esta el texto. Si es texto, dar vuelta
            //            newData.transform.GetChild(1).GetChild(1).GetComponent<TextMeshProUGUI>().alignment = TextAlignmentOptions.Right;
            //            newData.transform.GetChild(0).SetAsLastSibling();
            //        }
            //    }

            //}

            Debug.Log("Informacion de nube setted");
        }
    }

    private GameObject CategoryExists(string category)
    {
        foreach (var cat in categories)
        {
            if (cat.GetComponentInChildren<DashboardPanelTitle>().category == category)
            {
                return cat;
            }
        }
        return null;
    }

    public IEnumerator DataImageLoad(DashboardPanelInfo dataObject, DashboardInfoData dashboardInfoData)
    {
        using (UnityWebRequest uwr = UnityWebRequestTexture.GetTexture(dashboardInfoData.image_url))
        {
            // Wait for download to complete
            yield return uwr.SendWebRequest();

            if (uwr.isNetworkError || uwr.isHttpError)
            {
                Debug.Log(uwr.error);
            }

            // assign texture
            var texture = DownloadHandlerTexture.GetContent(uwr);
            dataObject.image.overrideSprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), Vector2.one * 0.5f);
            //dataObject.GetComponentInChildren<Image>().preserveAspect = true;
        }
    }

    private RectTransform target = null;
    public bool SnapTo(string categoryName)
    {
        target = null;

        foreach (var category in categories)
        {
            if (category.GetComponentInChildren<DashboardPanelTitle>().category.ToLower() == categoryName.ToLower())
            {
                target = (RectTransform)category.transform;
            }
        }

        if (target == null)
        {
            Debug.LogWarning("No hay ninguna categoria llamada ( " + categoryName + " ) en la nube actual");
            return false;
        }

        ExplodeViewer.Show();
        audioSourceExplode.Play();
        lowerDrawer.DetectGestures = false;
        return true;
    }

    private void Snap()
    {
        Canvas.ForceUpdateCanvases();

        mainContent.anchoredPosition =
            (Vector2)scrollRect.transform.InverseTransformPoint(mainContent.position)
            - (Vector2)scrollRect.transform.InverseTransformPoint(target.position);
    }

}
