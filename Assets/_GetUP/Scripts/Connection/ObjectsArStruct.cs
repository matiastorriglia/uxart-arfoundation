﻿using System.Collections.Generic;
using System;

//Usado para los objetos AR y los GEOTags
public class LanguageInfo
{
    public string language { get; set; }
    public string name { get; set; }
    public string info { get; set; }
}

public class Locations
{
    public string id { get; set; } //Para diferenciarlos con los paneles de INFO
    public string placeName { get; set; }
    public string location { get; set; }
    public string url_info { get; set; } //Para los GeoTags
}

public class ObjectArData
{ //Los datos del canal
    public string _id { get; set; }
    public string place { get; set; }
    public string prefabName { get; set; }
    public string url_assetBundle { get; set; }
    public string url_info { get; set; }
    public string url_image { get; set; }
    public string bundleName { get; set; }
    public string location { get; set; }
    public string scaleOffset { get; set; }
    public string ambientLight { get; set; }
    public List<Locations> locations { get; set; }
    public List<LanguageInfo> languageInfo { get; set; }
    public bool enable { get; set; }
}

//El contenido
public class ObjectsArResult
{
    public bool aviso { get; set; }
    public string mensaje { get; set; }
    public List<ObjectArData> result_objects { get; set; }
}

//El paquete que devuelve el .js
public class ObjectsArPacket
{
    public object error { get; set; }
    public ObjectsArResult result { get; set; }
}
