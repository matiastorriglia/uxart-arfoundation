﻿using System.Collections;
using System.Collections.Generic;


public class MarkerDatabase
{
    public string aviso;
    public string _id;
    public string url;
    public string name;
    public string channel_id;
}

public class MarkerDatabasePacket
{

    public object error { get; set; }
    public MarkerDatabase result { get; set; }
}


public class UDARTarget
{
    public string _id;
    public string url_assetBundle;
    public string prefabName;
    
    public string bundleName;

    public string channel_id;
}

public class UDARTargetResult
{
    string aviso;
    public string mensaje { get; set; }
    public List<UDARTarget> result_targets { get; set; }

}

public class UDARTargetPacket
{
    public object error { get; set; }

    public UDARTargetResult result{ get; set; }
}

