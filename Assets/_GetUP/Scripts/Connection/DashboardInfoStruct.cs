﻿using System.Collections.Generic;
using System;

public class DashbaordInfo
{
    public string language { get; set; }
    public string categoryName { get; set; }
    public string title { get; set; }
    public string text { get; set; }
}

public class DashboardInfoData {
    public string _id { get; set; }
    public string dashboard_id { get; set; }
    public string category { get; set; }
    public List<DashbaordInfo> languageInfo { get; set; }
    public string image_url { get; set; }
    public bool enable { get; set; }
    public DateTime date_created { get; set; }
}

//El contenido
public class DashboardInfoResult {
    public string id { get; set; }
    public bool aviso { get; set; }
    public string mensaje { get; set; }
    public List<DashboardInfoData> result_dashboardinfo { get; set; }
}

//El paquete que devuelve el .js
public class DashboardInfoPacket {
    public object error { get; set; }
    public DashboardInfoResult result { get; set; }
}