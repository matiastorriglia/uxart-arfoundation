﻿using System.Collections.Generic;
using System;

public class FeedData
{ //Los datos del canal
    public string _id { get; set; }
    public string user_id { get; set; }
    public string feedPath { get; set; }
    public string feedType { get; set; }
    public List<string> users_liked { get; set; }
    public string date_created { get; set; }
    public int views { get; set; }
}

//El contenido
public class FeedResult
{
    public bool aviso { get; set; }
    public string mensaje { get; set; }
    public List<FeedData> result_feed { get; set; }
}

//El paquete que devuelve el .js
public class FeedPacket
{
    public object error { get; set; }
    public FeedResult result { get; set; }
}

public class FeedUserResult
{
    public bool aviso { get; set; }
    public string mensaje { get; set; }
    public string userName { get; set; }
    public string user_id { get; set; }
}

public class FeedUserPacket
{
    public object error { get; set; }
    public FeedUserResult result { get; set; }
}
