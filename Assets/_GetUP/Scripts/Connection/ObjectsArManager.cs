﻿using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;
using UnityEngine;
using UnityEngine.Networking;
using Doozy.Engine.UI;
using UnityEngine.SceneManagement;
using Mapbox.Unity.Location;
using System.Linq;

public class ObjectsArManager : MonoBehaviour
{
    public static ObjectsArManager instance;

    public List<ObjectArData> GeoLocationsArData = new List<ObjectArData>();
    public List<ObjectArData> GeoLocationsInfoData = new List<ObjectArData>();
    public List<PortalData> PortalsArData = new List<PortalData>();

    public List<string> NonRepeatedObjects = new List<string>();

    [SerializeField] private RectTransform artHouseContent;
    [SerializeField] private GameObject artHousePrefab;

    [SerializeField] private UIDrawer houseArtDrawer;
    [SerializeField] private UIDrawer channelDrawer;

    private int FirstTimeLoading = 2;

    private void Awake()
    {
        if (instance != null)
        {
            DestroyImmediate(gameObject);
            return;
        }
        instance = this;
        DontDestroyOnLoad(gameObject);
    }


    public void SetGeolocationsAr(string resulting)
    {
        ObjectsArPacket objectsArPacket = JsonConvert.DeserializeObject<ObjectsArPacket>(resulting);
        if (objectsArPacket.result.result_objects == null)
        {
            GeoLocationsArData.Clear();
            Debug.Log("El canal no tiene objetosAR de geolocalizacion");
        }
        else
        {
            GeoLocationsArData = objectsArPacket.result.result_objects;
        }
        GenerateArtHouseContent(GeoLocationsArData);

        if (GeoManager.instance && FirstTimeLoading <= 0)
        {
            //StartCoroutine(GeoManager.instance.GetObjectsAr());
        }
        else if (SceneManager.GetActiveScene().name == "ARCoreSurface")
        {
            GameManager.instance.CargarPortales();
        }
        else
        {
            FirstTimeLoading--;
        }

        Debug.Log("GeoLocationsAr setted");
    }

    public void SetGeolocationsInfo(string resulting)
    {
        ObjectsArPacket objectsArPacket = JsonConvert.DeserializeObject<ObjectsArPacket>(resulting);
        if (objectsArPacket.result.result_objects == null)
        {
            GeoLocationsInfoData.Clear();
            Debug.Log("El canal no tiene Info de geolocalizacion");
        }
        else
        {
            GeoLocationsInfoData = objectsArPacket.result.result_objects;
        }

        if (GeoManager.instance && FirstTimeLoading <= 0)
        {
            //StartCoroutine(GeoManager.instance.GetObjectsInfo());
            GameManager.instance.CargarGeolocalizacion();
        }
        else
        {
            FirstTimeLoading--;
        }

        Debug.Log("GeoLocationsInfo setted");
    }

    public void SetPortalsAr(string resulting)
    {
        PortalPacket objectsArPacket = JsonConvert.DeserializeObject<PortalPacket>(resulting);
        if (objectsArPacket.result.result_objects == null)
        {
            PortalsArData.Clear();
            Debug.Log("El canal no tiene portales");
        }
        else
        {
            PortalsArData = objectsArPacket.result.result_objects;
        }

        if (PortalManager.instance)
        {
            PortalManager.instance.RefreshObjetosAr();
        }

        Debug.Log("Portals setted");
    }

    private void Clear3DHouseBox()
    {
        NonRepeatedObjects.Clear();
        foreach (Transform children in artHouseContent)
        {
            Destroy(children.gameObject);
        }
    }

    public void GenerateArtHouseContent(List<ObjectArData> objectArDatas)
    {
        Clear3DHouseBox(); 

        foreach (var arData in objectArDatas)
        {

            bool pacifico = false;

            for (int j = 0; j < arData.locations.Count; j++)
            {
                if (arData.locations[j].location == "-21.71415, -145.84745")
                {
                    pacifico = true;
                    break;
                }
            }

            if (!(arData.prefabName == "INFO") && !NonRepeatedObjects.Contains(arData.prefabName) && pacifico)
            {
                NonRepeatedObjects.Add(arData.prefabName);
                HouseArtManager houseArtManager = Instantiate(artHousePrefab, artHouseContent).GetComponent<HouseArtManager>();
                if (arData.languageInfo != null && arData.languageInfo[0].language != "")
                {
                    foreach (var li in arData.languageInfo)
                    {
                        if (li.language == LocalizationManager.instance.currentLanguage.ToString())
                        {
                            houseArtManager.artText.text = li.name;
                            break;
                        }
                    }
                }
                else
                {
                    houseArtManager.artText.text = arData.prefabName;
                }
                houseArtManager.ObjectAr = arData;
            }
            else if (!pacifico)
                Debug.Log("Obra no geolocalizada en pacifico no se muestra en lista 3D: " + arData.prefabName);
        }
    }

    public void CloseArtHouseContent()
    {
        houseArtDrawer.Close();
        channelDrawer.DetectGestures = true;
    }

    public void OpenArtHouseContent()
    {
        channelDrawer.Close();
        channelDrawer.DetectGestures = false;
        houseArtDrawer.Open();
    }
}

