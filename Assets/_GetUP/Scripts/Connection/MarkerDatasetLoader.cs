﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;
using System.Linq;
using UnityEngine.Networking;
using UnityEngine.UI;

public class MarkerDatasetLoader : MonoBehaviour
{
    public Canvas markerCanvas;
    public Text markerText;

    void Start()
    {
        // Registering call back to know when Vuforia is ready
        VuforiaARController.Instance.RegisterVuforiaStartedCallback(OnVuforiaStarted);
        markerCanvas.worldCamera = GameManager.instance.GetComponentInChildren<Camera>();

        NFTButton.instance.buttonObj.SetActive(false);

        if (ChannelManager.instance.currentChannelId != "5f2c83415711cb19c05dbcba") {
            if (MarkerManager.instance.ThereAreMarkers) {
                markerText.text = LocalizationManager.instance.GetLocalizedValue("marker_have");
            } else { 
                markerText.text = LocalizationManager.instance.GetLocalizedValue("marker_donthave");
            }
        }
        else
        {
            if (MarkerManager.instance.ThereAreMarkers)
            {
                markerText.text = LocalizationManager.instance.GetLocalizedValue("marker_have_marta");
            }
            else
            {
                markerText.text = LocalizationManager.instance.GetLocalizedValue("marker_donthave");
            }
        }
    }

    private void OnDestroy() {
        VuforiaARController.Instance.UnregisterVuforiaStartedCallback(OnVuforiaStarted);
    }


    // This function is called when vuforia gives the started callback
    void OnVuforiaStarted()
    {
        StartCoroutine(DownloadDatabaseFiles());
        // The 'path' string determines the location of xml file
        // For convinence the RealTime.xml is placed in the StreamingAssets folder
        // This file can be downloaded and the relative path will be used accordingly
    }

    IEnumerator DownloadDatabaseFiles()
    {
        //LoadingCanvasController.instance.textImagenCarga.text = LocalizationManager.instance.GetLocalizedValue("download_marker_label");
        LoadingCanvasController.instance.textImagenCarga.text = "";
        if (MarkerManager.instance.currentMarkerDatabase._id != null) {
            MarkerDatabase markerDatabase = MarkerManager.instance.currentMarkerDatabase;

            string url = string.Format("{0}/{1}.xml", markerDatabase.url, markerDatabase.name);
            Debug.Log(url);
            using (UnityWebRequest www = UnityWebRequest.Get(url)) {
                yield return www.SendWebRequest();
                if (www.isNetworkError || www.isHttpError) {
                    Debug.Log(www.error);
                } else {
                    string savePath = string.Format("{0}/{1}.xml", Application.persistentDataPath, markerDatabase.name);
                    System.IO.File.WriteAllBytes(savePath, www.downloadHandler.data);
                }
            }

            url = string.Format("{0}/{1}.dat", markerDatabase.url, markerDatabase.name);
            Debug.Log(url);
            using (UnityWebRequest www = UnityWebRequest.Get(url)) {
                yield return www.SendWebRequest();
                if (www.isNetworkError || www.isHttpError) {
                    Debug.Log(www.error);
                } else {
                    string savePath = string.Format("{0}/{1}.dat", Application.persistentDataPath, markerDatabase.name);
                    System.IO.File.WriteAllBytes(savePath, www.downloadHandler.data);
                }
            }

            string path = "";
            path = string.Format("{0}/{1}.xml", Application.persistentDataPath, markerDatabase.name);
            Debug.Log(path);
            /*       
           #if UNITY_IPHONE
                   path = Application.persistentDataPath + "/Raw/RealTime.xml";
           #elif UNITY_ANDROID
                   path = "jar:file://" + Application.persistentDataPath + "!/assets/RealTime.xml";
           #else
                   path = Application.persistentDataPath + "/StreamingAssets/RealTime.xml";
           #endif */
            //LoadingCanvasController.instance.textImagenCarga.text = LocalizationManager.instance.GetLocalizedValue("loading_marker_label");

            bool status = LoadDataSet(path, VuforiaUnity.StorageType.STORAGE_ABSOLUTE);

            if (status) {
                Debug.Log("Dataset Loaded");
                MarkerManager.instance.OnDatasetLoad();
            } else {
                Debug.Log("Dataset Load Failed");
            }
        } else {
            LoadingCanvasController.instance.ProcesosEnCurso--;
        }
    }


    // Load and activate a data set at the given path.
    private bool LoadDataSet(string dataSetPath, VuforiaUnity.StorageType storageType)
    {
        // Request an ImageTracker instance from the TrackerManager.
        ObjectTracker objectTracker = TrackerManager.Instance.GetTracker<ObjectTracker>();

        objectTracker.Stop();
        IEnumerable<DataSet> dataSetList = objectTracker.GetActiveDataSets();
        foreach (DataSet set in dataSetList.ToList())
        {
            objectTracker.DeactivateDataSet(set);
        }

        // Check if the data set exists at the given path.
        if (!DataSet.Exists(dataSetPath, storageType))
        {
            Debug.LogError("Data set " + dataSetPath + " does not exist.");
            return false;
        }

        // Create a new empty data set.
        DataSet dataSet = objectTracker.CreateDataSet();

        // Load the data set from the given path.
        if (!dataSet.Load(dataSetPath, storageType))
        {
            Debug.LogError("Failed to load data set " + dataSetPath + ".");
            return false;
        }
        //LoadingCanvasController.instance.textImagenCarga.text = LocalizationManager.instance.GetLocalizedValue("download_marker_label");

        // (Optional) Activate the data set.
        objectTracker.ActivateDataSet(dataSet);
        objectTracker.Start();


        //StartCoroutine(DownloadAssetBundle(dataSet));

        return true;
    }


}
