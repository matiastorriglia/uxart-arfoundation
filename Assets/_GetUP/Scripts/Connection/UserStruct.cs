﻿using System.Collections.Generic;
using System;

[System.Serializable]
public class UserData { //Los datos del canal
    public string _id { get; set; }
    public string name { get; set; }
    public string sex { get; set; }
    public string photo { get; set; }
    public string email { get; set; }
    public string password { get; set; }
    public string language { get; set; }
    public string channels { get; set; }
    public bool enable { get; set; }
    public string last_modify { get; set; }
}

//El contenido
[System.Serializable]
public class UserResult {
    public bool aviso { get; set; }
    public string mensaje { get; set; }
    public List<UserData> result_users { get; set; }
}

//El paquete que devuelve el .js
[System.Serializable]
public class UserPacket {
    public object error { get; set; }
    public UserResult result { get; set; }
}
