﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Doozy.Engine.UI;
using Newtonsoft.Json;
using System.Text.RegularExpressions;

public class UserManager : MonoBehaviour {
    public static UserManager instance;

    [Header("Logging")]
    [SerializeField] private CanvasGroup loggingCanvas;
    [SerializeField] private InputField mailInput;
    [SerializeField] private InputField passwordInput;
    [SerializeField] private Button logInButton;
    [SerializeField] private Button backButton;
    [SerializeField] private Text infoText;

    [Header("User")]
    [SerializeField] private UIDrawer rightDrawer;
    [SerializeField] private Text nameText;
    [SerializeField] private Dropdown languageDropdown;

    [Header("Register")]
    [SerializeField] private Text regInfoText;
    [SerializeField] private InputField nameInput;
    [SerializeField] private InputField regMailInput;
    [SerializeField] private InputField regPasswordInput;
    [SerializeField] private InputField regCheckPasswordInput;
    [HideInInspector] public string sex;
    [SerializeField] private Button RegisterButton;

    //Sirve para verificar que el mail sea valido
    public const string MatchEmailPattern =
        @"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
        + @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
        + @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
        + @"([a-zA-Z]+[\w-]+\.)+[a-zA-Z]{2,4})$";

    public UserData currentUser = new UserData(); //Inicializado con todos los valores en null

    private UserData newUser = new UserData(); //Usado para las registraciones

    public bool CanvasStatus {
        get {
            return loggingCanvas.alpha == 1;
        }
        set {
            loggingCanvas.interactable = value;
            loggingCanvas.blocksRaycasts = value;
            loggingCanvas.alpha = value ? 1 : 0;
        }
    }

    private void Awake() {
        if (instance != null) {
            DestroyImmediate(gameObject);
            return;
        }
        instance = this;
        DontDestroyOnLoad(gameObject);

        LocalizationManager.OnChangeLanguage += RefreshLanguage;
    }

    private void RefreshLanguage() {
        infoText.text = LocalizationManager.instance.GetLocalizedValue(infoText.text);
    }

    public void CheckUser() {
        infoText.text = LocalizationManager.instance.GetLocalizedValue("user_wait");
        if (currentUser.email == null) { //El emit devuelve un usuario con todos los valores null si este no existe
            CanvasStatus = true;
        } else {
            CanvasStatus = false;
            rightDrawer.Toggle();
        }
    }

    public void GoBack() {
        CanvasStatus = false;
    }

    public void LogIn() {
        ConnectionManager.instance.AddEmitCommand("actions_users", "{'accion':'loginUsuario','email':'" + mailInput.text + "','password':'" + passwordInput.text + "','devolucion':'_receive_userLogIn'}");
    }

    public void RegisterUser() {
        if (nameInput.text.Length < 3) {
            regInfoText.text = "Names need 3 or more letters";
            return;
        }
        if (regPasswordInput.text != regCheckPasswordInput.text || regPasswordInput.text.Length < 5) {
            regInfoText.text = "Passwords inputs are different. Need 5 or more letters";
            return;
        }
        if (sex != "Female" && sex != "Male") {
            regInfoText.text = "Select Female or Male";
            return;
        }

        if (!validateEmail(regMailInput.text)) {
            regInfoText.text = "Incorrect eMAIL format";
            return;
        }

        ConnectionManager.instance.AddEmitCommand("actions_users", "{'accion':'createUser" +
            "','name':'" + nameInput.text +
            "','sex':'" + sex +
            "','photo':'" + "" +
            "','email':'" + regMailInput.text.ToLower() +
            "','password':'" + regPasswordInput.text +
            "','language':'" + Application.systemLanguage.ToString() +
            "','channels':'" + "" +
            "','enable':'" + true +
            "','devolucion':'_receive_userRegister'}");
    }

    //Para separar los canales en una lista 
    public List<string> DecodeChannels() {
        if (currentUser._id == null || currentUser._id == "") {
            return null;
        }
        List<string> auxChannels = new List<string>();

        var auxArray = currentUser.channels.Split(',');
        foreach (var item in auxArray) {
            auxChannels.Add(item);
        }

        return auxChannels;
    }

    //Para agregar los canales favoritos
    public void AddChannel(string channel_id) {
        if (currentUser._id == null || currentUser._id == "") {
            return;
        }

        if (currentUser.channels == "") {
            currentUser.channels = channel_id;
        } else {
            currentUser.channels += "," + channel_id;
        }

    }

    public void UnregisterUser() {
        ConnectionManager.instance.AddEmitCommand("actions_users", "{'accion':'unregisterUser','_id':'" + currentUser._id + "','devolucion':'_receive_userUnregister'}");
    }

    public void LogOut() {
        infoText.text = "";
        rightDrawer.Toggle();
        currentUser = new UserData();
        if (Application.systemLanguage == SystemLanguage.Spanish)
            LocalizationManager.instance.SetLanguageOptions(Application.systemLanguage);
        else
            LocalizationManager.instance.SetLanguageOptions(SystemLanguage.English);
        CheckUser();
    }

    public void SetUser(string resulting) {
        UserPacket UserPacket = JsonConvert.DeserializeObject<UserPacket>(resulting);
        infoText.text = LocalizationManager.instance.GetLocalizedValue(UserPacket.result.mensaje);
        if (!UserPacket.result.aviso) {
            
            return;
        }
        Debug.Log("User Setted");
        currentUser = UserPacket.result.result_users[0];

        mailInput.text = "";
        passwordInput.text = "";

        nameText.text = currentUser.name;
        GameManager.instance.setLanguageDropdown(currentUser.language);

        CheckUser();
    }

    public void GetUserPrefSetResponse(string resulting) {
        UserPacket UserPacket = JsonConvert.DeserializeObject<UserPacket>(resulting);
        Debug.Log(UserPacket.result.mensaje);
    }

    public void RegisterUserResponse(string resulting) {
        UserPacket UserPacket = JsonConvert.DeserializeObject<UserPacket>(resulting);
        Debug.Log(UserPacket.result.mensaje);
        if (UserPacket.result.aviso) {
            regInfoText.text = "User created succesfully";
        } else {
            regInfoText.text = "Mail already in use or an error ocurred";
        }
    }

    public void UnregisterUserResponse(string resulting) {
        UserPacket UserPacket = JsonConvert.DeserializeObject<UserPacket>(resulting);
        Debug.Log(UserPacket.result.mensaje);

        if (UserPacket.result.aviso) {
            infoText.text = UserPacket.result.mensaje;
            LogOut();
        }
    }

    public static bool validateEmail(string email) {
        if (email != null)
            return Regex.IsMatch(email, MatchEmailPattern);
        else
            return false;
    }

    public void SetSex(string sexInput) {
        sex = sexInput;
    }

    public void UpdateUser() {
        ConnectionManager.instance.AddEmitCommand("actions_users", "{'accion':'updatePrefUser" +
            "','_id':'" + currentUser._id +
            "','name':'" + currentUser.name +
            "','sex':'" + currentUser.sex +
            "','photo':'" + currentUser.photo +
            "','email':'" + currentUser.email +
            "','password':'" + currentUser.password +
            "','language':'" + currentUser.language +
            "','channels':'" + currentUser.channels +
            "','enable':'" + currentUser.enable +
            "','devolucion':'_receive_userSetPref'}");
    }
}

