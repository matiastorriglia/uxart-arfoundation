﻿using System.Collections;
using System.Collections.Generic;

public class LocalizationData
{
    public string _id { get; set; }
    public string language { get; set; }
    public List<LocalizationItem> items { get; set; }
}

public class LocalizationItem
{
    public string key { get; set; }
    public string value { get; set; }
}

public class LocalizationResult {
    public bool aviso { get; set; }
    public List<LocalizationData> result_language { get; set; }
}

public class LocalizationPacket {
    public object error { get; set; }
    public LocalizationResult result { get; set; }
}