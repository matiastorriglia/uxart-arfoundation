﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlertaAr : MonoBehaviour
{
    [SerializeField] private float alertTime = 3;
    private float alertTimer;
    void Update()
    {
        alertTimer += Time.deltaTime;
        if (alertTimer >= alertTime)
        {
            Destroy(gameObject);
        }
    }
}
