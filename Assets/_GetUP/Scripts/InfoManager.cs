﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Doozy.Engine.UI;
using Boomlagoon.JSON;
using TMPro;

public class InfoManager : MonoBehaviour
{
    public static InfoManager instance;

    public UIView TutorialView;
    public UIView InfoView;
    public UIView AlertView;

    //public CanvasGroup canvasGroup;

    [SerializeField] private UIDrawer downDrawer;
    [Header("Tutorial")]
    [SerializeField] private List<GameObject> welcomeHelp;
    private List<GameObject> currentTutorialList;
    private bool TutorialWorking = false;
    private int tutorialIndex;

    [Header("Info")]
    [SerializeField] private TextMeshProUGUI infoTitle; //Generalmente es el nombre del objeto o portal
    [SerializeField] private TextMeshProUGUI info; //La informacion que se mostrara sobre el objeto

    [Header("Alert")]
    [SerializeField] private TextMeshProUGUI alert;
    [SerializeField] private float alertPersistanceTime = 5;
    private float alertPersistanceTimer;

    private ObjectArData currentObjectArData;
    private PortalData currentPortalData;

    private void Awake()
    {
        if (instance != null)
        {
            DestroyImmediate(gameObject);
            return;
        }
        instance = this;
        DontDestroyOnLoad(gameObject);
        LocalizationManager.OnChangeLanguage += RefreshLanguage;
    }

    private int TutorialIndex {
        get {
            return tutorialIndex;
        }
        set {
            //El setter se ocupa de apagar el actual y luego de prender el siguiente hasta el ultimo
            //Debug.Log(tutorialIndex + " " + value + " " + currentTutorialList.Count);
            currentTutorialList[tutorialIndex].SetActive(false);
            tutorialIndex = value;
            if (tutorialIndex < currentTutorialList.Count)
            {
                currentTutorialList[tutorialIndex].SetActive(true);
            }
            else
            {
                FinishTutorial();
            }
        }
    }

    private void Update()
    {
        TutorialUpdate();
        AlertPanelPersistance();
    }


    #region ALERT
    private void AlertPanelPersistance()
    {
        if (AlertView.IsVisible)
        {
            alertPersistanceTimer += Time.deltaTime;
            if (alertPersistanceTimer > alertPersistanceTime)
            {
                alertPersistanceTimer = 0;
                AlertView.Hide();
            }
        }
    }

    public void FinishAlert() {
        alertPersistanceTimer = 0;
        AlertView.Hide();
    }

    public IEnumerator StartMapAlert(ChannelLocation channelLocation, float TimeAlert)
    {
        if (AlertView.IsVisible) { yield break; }

        alertPersistanceTime = TimeAlert;
        AlertView.Show();
        alert.text = LocalizationManager.instance.GetLocalizedValue("map_alert") + " " + channelLocation.locationName;
    }

    public IEnumerator StartARalert(float TimeAlert)
    {
        if (AlertView.IsVisible) { yield break; }

        alertPersistanceTime = TimeAlert;
        AlertView.Show();
        alert.text = LocalizationManager.instance.GetLocalizedValue("ar_alert");
    }
    #endregion

    #region TUTORIAL
    private void TutorialUpdate()
    {
        if (TutorialWorking && Input.GetKeyDown(KeyCode.Mouse0))
        {
            TutorialIndex++;
        }
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            FinishTutorial();
        }
    }
    public void StartTutorial()
    {
        downDrawer.DetectGestures = false;

        currentTutorialList = welcomeHelp;


        TutorialWorking = true;
        TutorialView.Show();

        foreach (var tuto in welcomeHelp)
        {
            tuto.SetActive(false);
        }

        TutorialIndex = 0;
    }

    private void FinishTutorial()
    {
        TutorialView.Hide();
        tutorialIndex = 0;
        downDrawer.DetectGestures = true;
    }
    #endregion

    #region INFO
    public void OnInfoButtonPressed()
    {
        switch (SceneManager.GetActiveScene().name)
        {
            case "ARCorePortals":
                if (infoTitle.text == "") { StartTutorial(); }
                else { StartInfo(); }
                break;
            case "ARCoreSurface": StartInfo(); break;
            default:
                StartTutorial();
                break;
        }
    }
    public void StartInfo()
    {
        downDrawer.DetectGestures = false;
        InfoView.Show();
    }

    public void FinishInfo()
    {
        downDrawer.DetectGestures = true;
        InfoView.Hide();
    }
    public void CleanInfo()
    {
        infoTitle.text = "";
        info.text = "";
    }

    public void RefreshLanguage() {
        if (currentObjectArData != null)
        {
            if (currentObjectArData.languageInfo != null && currentObjectArData.languageInfo[0].language != "")
            {
                currentObjectArData.languageInfo.ForEach(delegate (LanguageInfo li)
                {
                    if (li.language == LocalizationManager.instance.currentLanguage.ToString())
                    {
                        infoTitle.text = li.name;
                        info.text = li.info;
                        return;
                    }
                });
            }
        }
        else if (currentPortalData != null)
        {
            foreach (var info in currentPortalData.languageInfo)
            {
                //Cuando encuentra el lenguaje en uso del sistema lo aplica
                if (info.language == LocalizationManager.instance.currentLanguage.ToString())
                {
                    infoTitle.text = info.name;
                    this.info.text = info.info;
                    return;
                }
            }
        }
    }

    public void GetObjectARInfo(ObjectArData objectArData = null, PortalData portaldata = null)
    {
        if (objectArData != null)
        {
            currentObjectArData = objectArData;
            currentPortalData = null;
        }
        if (portaldata != null)
        {
            currentObjectArData = null;
            currentPortalData = portaldata;
        }


        //Esta funcion se ocupa de agregar la informacion del objeto AR que se agregara a la pantalla
        //Hay dos sitios en donde puede objtener la informacion: un .json en el caso del assetbundle adquirido como TextAsset, y del documento de la db para el caso de los portales

        RefreshLanguage();
    }
    #endregion
}
