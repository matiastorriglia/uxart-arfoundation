﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortalLoadHandler : MonoBehaviour
{
    public static PortalLoadHandler instance;

    private void Awake()
    {
        instance = this;
    }

    private void OnDestroy()
    {
        instance = null;
    }

    public void SetActivePortal(bool state) {
        gameObject.SetActive(state);
    }
}
