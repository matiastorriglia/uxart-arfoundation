﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LowerSwipeButton : MonoBehaviour
{
    [SerializeField] public Image channelImage;
    [SerializeField] public Text channelTitle;
    [SerializeField] public Text channelText;

    private ChannelData channelData;

    public ChannelData ChannelData {
        get => channelData; set {
            channelData = value;
            RefreshLanguage();
        }
    }

    private void Awake()
    {
        LocalizationManager.OnChangeLanguage += RefreshLanguage;
    }

    private void OnDestroy()
    {
        LocalizationManager.OnChangeLanguage -= RefreshLanguage;
    }

    public void SetChannel()
    {
        if (ConnectionManager.instance.CommandsOnQueue)
        {
            Debug.LogWarning("Un canal ya está cargandose");
            return;
        }
        ChannelManager.instance.SelectChannel(transform);
        ChannelManager.instance.lowerDrawer.Close();
        //GameManager.instance.CargarGeolocalizacion();
    }

    public void RefreshLanguage() {
        foreach (var info in channelData.languageInfo)
        {
            //Cuando encuentra el lenguaje en uso del sistema lo aplica
            if (info.language == LocalizationManager.instance.currentLanguage.ToString())
            {
                name = info.name;
                channelTitle.text = info.name;
                channelText.text = info.info;
            }
        }
    }
}
