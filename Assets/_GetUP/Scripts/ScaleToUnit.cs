﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ScaleToUnit : MonoBehaviour
{
    // Sirve para la escena del mapa de mapbox, para que todos los objetos tengan la misma altura sin deformarlo
    private List<Bounds> allBounds = new List<Bounds>();

    void Start() {
       // Scale();
    }

    public void Scale() {
        if (GetComponentInChildren<MeshRenderer>()) {
            foreach (var meshRenderer in GetComponentsInChildren<MeshRenderer>()) {
                allBounds.Add(meshRenderer.bounds);
            }
        }
        if (GetComponentInChildren<SkinnedMeshRenderer>()) {
            foreach (var skinnedMeshRenderer in GetComponentsInChildren<SkinnedMeshRenderer>()) {
                allBounds.Add(skinnedMeshRenderer.bounds);
            }
        }

        Bounds bound = new Bounds();

        foreach (var bounds in allBounds) {
            bound.Encapsulate(bounds);
        }

        var localScale = transform.localScale;

        float yFactor = GeoManager.instance.desiredScale / bound.size.y;
        //float xFactor = GeoManager.instance.desiredScale / bound.size.x;
        //float zFactor = GeoManager.instance.desiredScale / bound.size.z;

        //float factor = (xFactor > yFactor) ? (xFactor > zFactor) ? xFactor : zFactor: (yFactor > zFactor) ? yFactor : zFactor;
        float factor = yFactor;

        localScale = new Vector3(localScale.x * factor, localScale.y * factor, localScale.z * factor);
        transform.localScale = localScale;
    }
}
