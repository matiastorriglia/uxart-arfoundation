﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Mapbox.Unity.Utilities;
using System;

public class InfoPOI : MonoBehaviour
{
    public ObjectArData objectAr;


    [SerializeField] private TextMeshProUGUI textoPOI;
    [SerializeField] private Image imagenPOI;

    [SerializeField] private string urlSite;
    [SerializeField] private string descriptionPOI;
    [SerializeField] private AudioSource audioSource;

    public string DescriptionPOI { get { return descriptionPOI; } set { descriptionPOI = value; textoPOI.text = value; } }

    private void OnDrawGizmos() {
        if (!GeoManager.instance || GeoManager.instance.playerTransform == null) {
            return;
        }

        if (Vector3.Distance(transform.position, GeoManager.instance.playerTransform.position) < 4) {
            Gizmos.color = Color.green;
        } else {
            Gizmos.color = Color.red;
        }

        Gizmos.DrawLine(transform.position + Vector3.up, GeoManager.instance.playerTransform.position + Vector3.up);
    }

    private void OnEnable() {
        StartCoroutine(BuscarPorGeo());
        imagenPOI.color = Color.black;
    }

    private float vibrateTimer = 0;
    private bool OnRange = false;
    private void Update() {
        if (vibrateTimer < 0.3f) {
            vibrateTimer += Time.deltaTime;
        }
        if (Vector3.Distance(transform.position, GeoManager.instance.playerTransform.position) < GeoManager.instance.minDistanceSup) {
            if (!OnRange) {
                if (vibrateTimer >= 0.3f) {
                    Handheld.Vibrate();
                    vibrateTimer = 0;
                    audioSource.Play();
                }
                OnRange = true;
            }
            imagenPOI.color = Color.green;
        } else {
            OnRange = false;
            imagenPOI.color = Color.black;
        }
    }


    private IEnumerator BuscarPorGeo() {
        yield return new WaitUntil(() => ConnectionManager.instance.objectsArManager.GeoLocationsInfoData != null);

        var LatLonPos = GeoManager.instance._map.WorldToGeoPosition(transform.position);

        foreach (ObjectArData objectAR in ConnectionManager.instance.objectsArManager.GeoLocationsInfoData) {
            if(objectAR != null) {
                if (Conversions.StringToLatLon(objectAR.location).ToString() == LatLonPos.ToString()) {
                    this.objectAr = objectAR;
                    break;
                }
            }
        }

        if (objectAr != null) {
            try {
                StartCoroutine(RefreshName());
            }catch(Exception e) {
                Debug.Log("Exception caught despite object not being null. Attempting value hardcode.");
                //urlSite = "https://www.youtube.com/watch?v=dQw4w9WgXcQ";
                urlSite = "";
                DescriptionPOI = "Error Object";
                gameObject.name = "Error Object";
            }
        }
        else {
            Debug.Log("ERROR WITH GEO: " + LatLonPos.ToString());
            gameObject.SetActive(false);
        }
    }

    public void LoadInfo() {
        Application.OpenURL(urlSite);
    }


    IEnumerator RefreshName() {
        //El gameObject.name se cambia en RefreshName

        urlSite = objectAr.url_info;
        DescriptionPOI = objectAr.place;
        gameObject.name = objectAr.place;

        textoPOI.GetComponent<ContentSizeFitter>().horizontalFit = ContentSizeFitter.FitMode.MinSize;
        yield return null;
        textoPOI.GetComponent<ContentSizeFitter>().horizontalFit = ContentSizeFitter.FitMode.PreferredSize;

    }
}
