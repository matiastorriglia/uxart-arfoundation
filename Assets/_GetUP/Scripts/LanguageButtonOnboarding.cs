using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class LanguageButtonOnboarding : MonoBehaviour
{
    public TextMeshProUGUI text;
    [SerializeField] private string _spanish;
    [SerializeField] private string _english;

    private void Awake()
    {
        if (Application.systemLanguage == SystemLanguage.Spanish)
        {
            text.text = _spanish;
        }
        else
        {
            text.text = _english;
        }
    }
}
