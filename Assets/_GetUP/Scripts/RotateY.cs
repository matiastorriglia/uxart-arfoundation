﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateY : MonoBehaviour
{

    private void Start() {
        transform.Rotate(0, Random.Range(0,360), 0);
    }
    private void Update() {
        transform.Rotate(0, 25 * Time.deltaTime, 0);
    }
}
