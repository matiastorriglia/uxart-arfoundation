﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mapbox.Unity.Utilities;

[CreateAssetMenu(fileName = "Data", menuName = "LocationData/Create", order = 1)]
public class LocationDataAsset : ScriptableObject {
    [SerializeField] [Geocode] public string location;
    public string url;
    public string gameObjectName;
    public string materialName;
}