﻿
using UnityEngine;
using System.Collections;
using Mapbox.Unity.Location;
using Mapbox.Unity.Map;
using Mapbox.Unity.Utilities;
using Mapbox.Utils;
using UnityEngine.SceneManagement;

public class ARSceneManager : MonoBehaviour
{
    public static ARSceneManager instance;

    [SerializeField] private AbstractMap _map;
    [SerializeField] private XRSurfaceController xRSurface;
    [SerializeField] private Transform objectContainer;
    [SerializeField] private Camera cam;
    private AssetBundle bundle;

    public ObjectArData objectAr;
    public string location;
    public bool from3DBox;

    private GameObject objetoInstanciado;

    public Canvas loadCamCanvas;

    private void Awake()
    {
        if (AssetBundleManager.instance == null)
        {
            return;
        }

        instance = this;

        cam = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
        loadCamCanvas.worldCamera = GameObject.FindObjectsOfType<Camera>()[1];
    }

    private void OnDestroy()
    {
        instance = null;
    }

    void Start()
    {
        StartCoroutine(LoadObjectAr());
        StartCoroutine(InfoManager.instance.StartARalert(5));
    }

    public IEnumerator LoadObjectAr()
    {
        objectAr = AssetBundleManager.instance.objetoAR;    //el objeto a instanciar
        location = AssetBundleManager.instance.location;    //la localizacion del objeto
        from3DBox = AssetBundleManager.instance.from3DBox;  //?????

        if (location != "")
        {
            location = location.Split(',')[1] + ", " + location.Split(',')[0]; //Lo corrigue agregandole espacio y cambiando de lugar los datos de localizacion
        }

        if (objectAr.prefabName == "INFO")
        {
            objetoInstanciado = Instantiate(GameManager.instance.DashboardPrefab, transform.position, Quaternion.identity); //instancia objeto de info
            ConnectionManager.instance.AddEmitCommand("actions_dashboard", "{'accion':'lookUpDashboardId','dashboard_location':'" + location + "','channel_id':'" + ChannelManager.instance.currentChannelId + "','devolucion':'_receive_dashboardId'}");
        }
        else
        {
            yield return new WaitWhile(() => AssetBundleManager.instance.ObtainAssetBundle(objectAr.url_assetBundle, objectAr.bundleName) == null);  //instancia objeto de info
            bundle = AssetBundleManager.instance.ObtainAssetBundle(objectAr.url_assetBundle, objectAr.bundleName);
            GameObject prefab = bundle.LoadAsset(objectAr.prefabName + ".prefab") as GameObject; //Creo el prefab

            InfoManager.instance.GetObjectARInfo(objectAr);

            if (prefab == null)
            {
                Debug.Log("Error Aqui, Prefab es null");
                //visualize in app??
            }
            //else{
            objetoInstanciado = Instantiate(prefab, transform.position, Quaternion.identity);

            float scaleOffset;
            if (from3DBox && objectAr.scaleOffset != null && float.TryParse(objectAr.scaleOffset, out scaleOffset))
            {
                objetoInstanciado.transform.localScale *= scaleOffset;
            }

            float ambientLight;
            if (objectAr.ambientLight != null && float.TryParse(objectAr.ambientLight, out ambientLight))
            {
               GameObject.Find("Directional Light").GetComponent<Light>().intensity = ambientLight*0.1f;
                if (ambientLight == 0)
                {
                    RenderSettings.skybox = null; //Paraque la oscuridad sea total
                    RenderSettings.ambientSkyColor = Color.black;
                }
            }

            MeshRenderer[] renderers = objetoInstanciado.GetComponentsInChildren<MeshRenderer>();

            foreach (MeshRenderer r in renderers) {
                //Material original = r.material;
                //Destroy(r.material);
                //r.material = original;
                //Shader temp = r.material.shader;
                //r.material.shader = null;
                if(r.material.shader.name != "Custom/CustomStandard")
                    r.material.shader = Shader.Find(r.material.shader.name);
                else
                    r.material.shader = Shader.Find("Standard");
            }
            //}
        }

        //include this previous if?
        if (objectAr.prefabName == "INFO")
        {
            var lookPos = cam.transform.position - objetoInstanciado.transform.position;
            lookPos.y = 0;
            var rotation = Quaternion.LookRotation(lookPos);
            objetoInstanciado.transform.rotation = rotation;
        }
        //else
        //{
        //    objetoInstanciado.transform.rotation = Quaternion.Euler(0, Random.Range(0, 360), 0);
        //}
        objetoInstanciado.transform.SetParent(objectContainer);
        objetoInstanciado.SetActive(true);

        StartCoroutine(CameraDelay());
    }

    IEnumerator CameraDelay()
    {
        Invoke("LoadingFinish", 0.2f);
        yield return new WaitForSeconds(1f);
        cam.enabled = true;
        yield return new WaitForSeconds(0.5f);
        xRSurface.gameObject.SetActive(true);
    }

    public void LoadingFinish()
    {
        //turn of loading wheel here? check loadingcanvascontroller
        LoadingCanvasController.instance.ARDone();
        //loadingGO.SetActive(false);
        LoadingCanvasController.instance.ProcesosEnCurso--;
    }

    public void LookCamera()
    {
        var look = objetoInstanciado.transform;
        look.LookAt(cam.transform);
        look.rotation = Quaternion.Euler(0, look.rotation.eulerAngles.y, look.rotation.eulerAngles.z);
        //objetoInstanciado.transform.LookAt(cam.transform);
    }
}
