﻿using System.Collections;
using UnityEngine.Networking;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;

using Boomlagoon.JSON;

public class AssetBundleManager : MonoBehaviour {
    public static AssetBundleManager instance;

    private Dictionary<string, AssetBundle> assetBundles = new Dictionary<string, AssetBundle>();
    private List<string> bundlesInProgress = new List<string>();

    public ObjectArData objetoAR;
    public string location;
    public bool from3DBox;

    private void Awake() {
        if (instance != null) {
            DestroyImmediate(gameObject);
            return;
        }
        instance = this;
        DontDestroyOnLoad(gameObject);
    }

    public bool HaveAssetBundle(string url, string bundleName) {
        return bundlesInProgress.Contains(url + "/" + bundleName);
    }

    public AssetBundle ObtainAssetBundle(string url, string bundleName) {
        if (bundlesInProgress.Contains(url + "/" + bundleName)) {
            return null;
        }

        if (assetBundles.ContainsKey(url + "/" + bundleName)) {
            return assetBundles[url + "/" + bundleName];
        } else {
            bundlesInProgress.Add(url + "/" + bundleName);
            StartCoroutine(DownloadAssetBundle(url, bundleName));
            print(url + "/" + bundleName);
        }
        return null;
    }


    IEnumerator DownloadAssetBundle(string url, string bundleName, int iter = 0, string bundleKey = null) {
        AssetBundle bundle = null;
        if (bundleKey == null) { //Para verificar que sea la primera interacion/intento
            bundleKey = url + "/" + bundleName;
            assetBundles.Add(bundleKey, null); //Primero registra la Key sin cargar nada
        }

        string path = "";

#if UNITY_IOS
        path = url + "/IOS/" + bundleName;
#endif
#if UNITY_ANDROID
        path = url + "/Android/" + bundleName;
#endif

        UnityWebRequest manifestRequest = UnityWebRequest.Get(path + ".manifest");
        
        yield return manifestRequest.SendWebRequest();
        Hash128 hash = new Hash128();
        uint crc = 0; 
        if (manifestRequest.error == null) { //Adquiere el CRC, valor unico para que los assetbundle pueden obtenerse del cache, si no encuentra el manifest los descarga
            string[] lines = manifestRequest.downloadHandler.text.Split('\n');
            if (lines[1].Contains("CRC:")) {

                uint.TryParse(lines[1].Substring(5, lines[1].Length - 5), out crc);
            }
            if (lines[5].Contains("Hash:")) {
                //print(lines[5].Substring(10, lines[5].Length - 10));
                hash = Hash128.Parse(lines[5].Substring(10, lines[5].Length - 10));
            }
        }

        UnityWebRequest request = UnityWebRequestAssetBundle.GetAssetBundle(path, hash, 0);

        yield return request.SendWebRequest();

        if (request.error != null) {
            Debug.LogWarning("Error al descargar el archivo: " + request.error);
            if (iter < 20) {
                yield return new WaitForSeconds(iter); //Cada ves vuelve a intentar un segundo mas tarde
                Debug.LogWarning("Intentando nuevamente, intentos restantes: " + (20 - iter).ToString() + " (" + path + ")");
                CallOtherDownloadAssetBundle(url, bundleName, ++iter, bundleKey);
            } else {
                assetBundles.Remove(bundleKey); //Quita la key del diccionario ya que no logro conseguir el bundle
            }
            yield break;
            //Detiene la corrutina
        }

        bundle = DownloadHandlerAssetBundle.GetContent(request);
        assetBundles[bundleKey] = bundle; //Luego agrega el bundle al la key creada
        bundlesInProgress.Remove(url + "/" + bundleName);
    }

    private void CallOtherDownloadAssetBundle(string url, string bundleName, int iter, string bundleKey) {
        StartCoroutine(DownloadAssetBundle(url, bundleName, iter, bundleKey));
    }
}
