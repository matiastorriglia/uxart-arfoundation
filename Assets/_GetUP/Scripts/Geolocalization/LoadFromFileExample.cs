﻿using System.IO;
using UnityEngine;

public class LoadFromFileExample : MonoBehaviour {
    public void Start() {
        var myLoadedAssetBundle = AssetBundle.LoadFromFile(Path.Combine(Application.streamingAssetsPath, "tests"));
        if (myLoadedAssetBundle == null) {
            Debug.Log("Failed to load AssetBundle!");
            return;
        }
        var prefab = myLoadedAssetBundle.LoadAsset("objeto1_prefab") as GameObject;
        var newGameObject = Instantiate(prefab);
        newGameObject.GetComponent<MeshRenderer>().material = myLoadedAssetBundle.LoadAsset("objeto1_mat") as Material;
    }
}