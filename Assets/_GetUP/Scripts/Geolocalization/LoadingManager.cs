﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;


using UnityEngine.Android;


using UnityEngine.SceneManagement;

public class LoadingManager : MonoBehaviour
{

    public LocalizationManager localizationManager;
    [SerializeField] private Button camButton;
    [SerializeField] private Button locButton;
    //[SerializeField] private Button saveButton;
    //[SerializeField] private Button notiButton;

    bool legalAccepted = false;


    [SerializeField] private CanvasGroup legalCG;
    [SerializeField] private ScrollRect legalScrollRect;

    private void Awake()
    {
        LoadingCanvasController.instance.ProcesosEnCurso++;

        if (PlayerPrefs.HasKey("LegalAccepted")) {
            if (PlayerPrefs.GetInt("LegalAccepted") == 1) {
                legalCG.alpha = 0;
                legalCG.interactable = false;
                legalCG.blocksRaycasts = false;
                legalAccepted = true;
            }else {
                legalAccepted = false;
                legalCG.alpha = 1;
                legalCG.interactable = true;
                legalCG.blocksRaycasts = true;
            }
        }else {
            legalAccepted = false;
            legalCG.alpha = 1;
            legalCG.interactable = true;
            legalCG.blocksRaycasts = true;
        }
    }

    private void Start()
    {
        Invoke("LocalizationDelayer", 1f);
        camButton.image.color = Permission.HasUserAuthorizedPermission(Permission.Camera) ? Color.grey : Color.white;
        locButton.image.color = Permission.HasUserAuthorizedPermission(Permission.FineLocation) ? Color.grey : Color.white;
    }

    public void LocalizationDelayer()
    {
        //Por alguna razon es necesario darle un pequeño delay para que traiga el .json completo
        if(Application.systemLanguage == SystemLanguage.Spanish)
            LocalizationManager.instance.SetLanguageOptions(Application.systemLanguage);
        else
            LocalizationManager.instance.SetLanguageOptions(SystemLanguage.English);
    }

    public void GrantCameraPermission()
    {
        Permission.RequestUserPermission(Permission.Camera);
    }

    public void GrantLocationPermission()
    {
        Permission.RequestUserPermission(Permission.FineLocation);
    }

    bool Cargando = false;
    private void Update()
    {

        if (!localizationManager.isReady)
            return;

        if (legalAccepted) {

            if (Cargando || !localizationManager.isReady)
            {
                return;
            }

            legalCG.alpha = 0;
            legalCG.interactable = false;
            legalCG.blocksRaycasts = false;

            #if UNITY_EDITOR
                    Cargando = true;
                    if (!ConnectionManager.instance.CommandsOnQueue) {
                        if (PlayerPrefs.GetString("onboarding", "null") == "null")
                        {
                            SceneManager.LoadScene("Onboarding");
                        }else {
                            SceneManager.LoadScene("ManagerScene");
                        }
                    }
                    return;
#endif
#if UNITY_ANDROID
                    if (AndroidPermissionsUpdate())
                    {
                                        TomarUbicacion();
                    }

#endif
#if UNITY_IOS
                        Debug.Error("Agregar los permisos necesarios para iOS en la funcion");
                    if (iOSPermissionsUpdate()) {
                                        TomarUbicacion();
                    }
#endif
        }
        else
        {
            LoadingCanvasController.instance.ProcesosEnCurso = 0;
            legalCG.alpha = 1;
            legalCG.interactable = true;
            legalCG.blocksRaycasts = true;
        }
    }

    private void TomarUbicacion()
    {
        Cargando = true;
        LoadingCanvasController.instance.ProcesosEnCurso++;
        StartCoroutine(getLocation());
    }

    public void OnValueChanged(Vector2 value)
    {
        legalScrollRect.verticalScrollbar.value = Mathf.Clamp(value.y, 0.0f, 1f);
    }

    public bool AndroidPermissionsUpdate()
    {
        camButton.image.color = Permission.HasUserAuthorizedPermission(Permission.Camera) ? Color.grey : Color.white;
        locButton.image.color = Permission.HasUserAuthorizedPermission(Permission.FineLocation) ? Color.grey : Color.white;

        if (!Cargando &&
                    Permission.HasUserAuthorizedPermission(Permission.FineLocation) &&
                    Permission.HasUserAuthorizedPermission(Permission.Camera))
        {
            return true;
        }
        else
        {
            LoadingCanvasController.instance.ProcesosEnCurso = 0;
            return false;
        }
    }

    public void AcceptLegal()
    {
        PlayerPrefs.SetInt("LegalAccepted", 1);
        PlayerPrefs.Save();
        legalAccepted = true;
    }

    public bool iOSPermissionsUpdate()
    {
        return true;
    }

    private int maxWait = 10;
    IEnumerator getLocation()
    {
        LocationService service = Input.location;
        if (!service.isEnabledByUser)
        {
            Cargando = false;
            yield break;
        }
        service.Start(1f, 0.1f);
        while (service.status == LocationServiceStatus.Initializing && maxWait > 0)
        {
            yield return new WaitForSeconds(1);
            maxWait--;
        }
        if (maxWait < 1)
        {
            Cargando = false;
            yield break;
        }
        if (service.status == LocationServiceStatus.Failed)
        {
            Cargando = false;
            yield break;
        }

        while (ConnectionManager.instance.CommandsOnQueue)
        {
            yield return null;
        }

        if (PlayerPrefs.GetString("onboarding", "null") == "null")
        {
            SceneManager.LoadScene("Onboarding");
        }
        else
        {
            SceneManager.LoadScene("ManagerScene");
        }

    }
}
