﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowTransformLerp : MonoBehaviour {
    [SerializeField]
    Transform _targetTransform;

    [SerializeField] Transform skyboxCam;
    [SerializeField] Transform mainCam;
    void Update() {
        Vector3 _targetPosition = new Vector3(_targetTransform.position.x, transform.position.y, _targetTransform.position.z);

        if (Vector3.Distance(transform.localPosition, _targetPosition) > 100) {
            //ColocacionPOI.instance.loadingManager.ObjectsLoading++;

            transform.localPosition = _targetPosition;

            //Reinicializa el mapa
            //StartCoroutine(ColocacionPOI.instance.InicilizarMapbox());
        }

        transform.position = Vector3.Lerp(transform.position, _targetPosition, 0.5f);
        skyboxCam.rotation = mainCam.rotation;
    }
}
