﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

using Mapbox.Unity.Location;
using Mapbox.Unity.Utilities;

using TMPro;
using Firebase.Auth;
using c8.Schemas;

public class PropertiesPOI : MonoBehaviour
{
    public ObjectArData objectAr;

    public int id = 0;

    public TextMeshProUGUI textoPOI;
    public Image imagenPOI;
    public GameObject labelContent;
    [SerializeField] private GameObject placeholderGO;

    private GameObject objetoCargado;

    [SerializeField] private bool IsPanel;
    [SerializeField] private AudioSource audioSource;

    public GroupPOI groupPOI;

    private void OnDrawGizmos()
    {
        if (!GeoManager.instance || GeoManager.instance.playerTransform == null)
        {
            return;
        }

        if (Vector3.Distance(transform.position, GeoManager.instance.playerTransform.position) < 4)
        {
            Gizmos.color = Color.green;
        }
        else
        {
            Gizmos.color = Color.red;
        }

        Gizmos.DrawLine(transform.position + Vector3.up, GeoManager.instance.playerTransform.position + Vector3.up);
    }

    private void OnEnable()
    {
        //DestroyImmediate(objetoCargado);
        StopAllCoroutines();
        InitObjectAR();
        StartCoroutine(BuscarPorGeo());
        LocalizationManager.OnChangeLanguage += RefreshNameLanguage;

        //GeoManager.instance.poisOnMap.Add(this);
        //Debug.Log("<color=blue>" + gameObject + "</color>", gameObject);
    }

    private void OnDisable()
    {
        InitObjectAR();
        LocalizationManager.OnChangeLanguage -= RefreshNameLanguage;

        //Debug.Log("<color=red>" + gameObject + "</color>", gameObject);
    }

    private void InitObjectAR()
    {
        if (groupPOI != null)
        {
            groupPOI.RemovePOI(this, false);
            groupPOI = null;
        }
        //GeoManager.instance.poisOnMap.Remove(this);
        Destroy(objetoCargado);
        IsPanel = false;
        Downloaded = false;
        OnRange = false;
        Cargando = false;
        objectAr = null;
        placeholderGO.SetActive(true);


        foreach (var anim in GetComponentsInChildren<Animator>())
        {
            anim.enabled = true;
        }
        foreach (var meshRenderer in GetComponentsInChildren<MeshRenderer>())
        {
            meshRenderer.enabled = true;
        }
        foreach (var meshRenderer in GetComponentsInChildren<SkinnedMeshRenderer>())
        {
            meshRenderer.enabled = true;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("POI"))
        {
            if (groupPOI == null && other.GetComponent<PropertiesPOI>() && other.GetComponent<PropertiesPOI>().groupPOI == null)
            {
                var poiGroup = new GameObject("PoiGroup").AddComponent<GroupPOI>();
                groupPOI = poiGroup;
                groupPOI.AddPOI(this);
                groupPOI.objetoCargado = Instantiate(placeholderGO, groupPOI.transform.position, Quaternion.Euler(0, Random.Range(0, 360), 0), groupPOI.transform);
                groupPOI.objetoCargado.GetComponent<MeshRenderer>().enabled = true;
                GeoManager.instance.groupPOIs.Add(groupPOI);
            }
            else if (other.GetComponent<PropertiesPOI>() && other.GetComponent<PropertiesPOI>().groupPOI != null && !other.GetComponent<PropertiesPOI>().groupPOI.poiList.Contains(this))
            {
                other.GetComponent<PropertiesPOI>().groupPOI.AddPOI(this);
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("POI") && other.GetComponent<GroupPOI>())
        {
            other.GetComponent<GroupPOI>().RemovePOI(this, true);
        }
    }

    private IEnumerator BuscarPorGeo()
    { //Asigna el objectAr corresponiente a este POI
        yield return new WaitUntil(() => ConnectionManager.instance.objectsArManager.GeoLocationsArData != null);

        var LatLonPos = GeoManager.instance._map.WorldToGeoPosition(transform.position);

        foreach (ObjectArData objectAR in ConnectionManager.instance.objectsArManager.GeoLocationsArData)
        {
            if(objectAR != null) { 
                if (objectAR.locations == null) { continue; }
                foreach (var loc in objectAR.locations)
                {
                    if (loc == null || loc.location == "") { continue; }
                    if (Conversions.StringToLatLon(loc.location).ToString() == LatLonPos.ToString())
                    {
                        this.objectAr = objectAR;
                        break;
                    }
                    if (this.objectAr != null) { break; }
                }
            }
        }

        if (objectAr != null) {
            if (objectAr.prefabName == "INFO")
            {
                IsPanel = true;
            }

            RefreshNameLanguage();
            StartCoroutine(RefreshName());
        }else {
            Debug.Log("ERROR with Property at geo: " + LatLonPos.ToString());
            gameObject.SetActive(false);
        }
    }

    IEnumerator AgregarPropiedades()
    {
        //Al ser encontrado, o no, se encarga de hacer que aparesca en el mapa instanciandolo

        if (objectAr == null)
        {
            Debug.LogError("No se encontro el objeto correspondiente, revizar que las coordenadas tengan 5 decimales", gameObject);
            yield break;
        }

        GameObject prefab;
        if (IsPanel)
        {
            //Es una excepsion, si es el dashboard/panel en tal caso se ejecuta otra funcion, evitando los assetbundle
            prefab = GameManager.instance.DashboardPrefab;

        }
        else
        {
            yield return new WaitWhile(() => AssetBundleManager.instance.ObtainAssetBundle(objectAr.url_assetBundle, objectAr.bundleName) == null);

            AssetBundle bundle = AssetBundleManager.instance.ObtainAssetBundle(objectAr.url_assetBundle, objectAr.bundleName);
            prefab = bundle.LoadAsset(objectAr.prefabName + ".prefab") as GameObject; //Creo el prefab
        }

        RefreshNameLanguage();

        if (prefab == null)
        {
            Debug.Log("Error Aqui, el prefab es null");
            yield break;
        }
        placeholderGO.SetActive(false);
        objetoCargado = Instantiate(prefab, transform.position, Quaternion.Euler(0, Random.Range(0, 360), 0), transform);
        transform.localScale = Vector3.one;
        var scaleToUnit = objetoCargado.AddComponent<ScaleToUnit>();
        scaleToUnit.Scale();

        if (objetoCargado.GetComponent<AudioSource>())
        {
            objetoCargado.GetComponent<AudioSource>().mute = true;
        }
        if (objetoCargado.GetComponent<NubeController>())
        {
            objetoCargado.GetComponent<NubeController>().interactuable = false;
        }

        if (!IsPanel) { 
            MeshRenderer[] renderers = objetoCargado.GetComponentsInChildren<MeshRenderer>();

            foreach (MeshRenderer r in renderers)
            {
                if (r.material.shader.name != "Custom/CustomStandard")
                    r.material.shader = Shader.Find(r.material.shader.name);
                else
                    r.material.shader = Shader.Find("Standard");
            }
        }

        if (Console.Instance != null)
        {
            Console.Instance.Log("Objeto Cargado (" + objectAr.prefabName + ") en la posicion " + objectAr.location, "yellow");
        }

        textoPOI.GetComponent<ContentSizeFitter>().horizontalFit = ContentSizeFitter.FitMode.MinSize;
        yield return null;
        textoPOI.GetComponent<ContentSizeFitter>().horizontalFit = ContentSizeFitter.FitMode.PreferredSize;


    }

    private float languageUpdatetime = 5;
    private float languageUpdatetimer = 0;
    private bool OnRange = false;
    private float vibrateTimer = 0;
    private bool Downloaded = false;
    private void Update()
    {
        if (vibrateTimer < 0.3f)
        {
            vibrateTimer += Time.deltaTime;
        }

        float distance = Vector3.Distance(transform.position, GeoManager.instance.playerTransform.position);

        if (distance < GeoManager.instance.minDistanceSup || GeoManager.instance.OnClosestLocationWithoutGPS)
        {
            if (!OnRange && !GeoManager.instance.OnClosestLocationWithoutGPS)
            {
                if (vibrateTimer >= 0.3f)
                {
                    Handheld.Vibrate();
                    vibrateTimer = 0;
                    audioSource.Play();
                }
                OnRange = true;
            }
            imagenPOI.color = Color.green;
        }
        else
        {
            OnRange = false;
            imagenPOI.color = Color.black;
        }

        transform.localScale = distance <= 100 ? Vector3.one : Vector3.one + Vector3.one * (distance - 99.9999f) * 0.05f;
        if (transform.localScale.magnitude < 1)
        {
            transform.localScale = Vector3.one;
        }

        //if (!IsPanel)
        //    Debug.Log(objectAr.url_assetBundle + ", " + objectAr.bundleName);

        //bool b2 = Vector3.Distance(transform.position, GeoManager.instance.playerTransform.position) < GeoManager.instance.minDistanceDownload;
        //bool b3 = objectAr != null;

        //if(!IsPanel)
        //    Debug.Log("Downloaded: " + Downloaded + ", Object AR Not Null: " + b3 + ", Distance Under: " + GeoManager.instance.minDistanceDownload + ", Distance: "  + Vector3.Distance(transform.position, GeoManager.instance.playerTransform.position));

        if (!Downloaded && objectAr != null && (AssetBundleManager.instance.HaveAssetBundle(objectAr.url_assetBundle, objectAr.bundleName) || Vector3.Distance(transform.position, GeoManager.instance.playerTransform.position) < GeoManager.instance.minDistanceDownload || IsPanel))
        {
            //if (!IsPanel)
            //    Debug.Log("starts the coroutine");
            Downloaded = true;
            StartCoroutine(AgregarPropiedades());
        }
    }

    private bool Cargando = false;

    public void CargarAR()
    {
        if (GeoManager.instance.OnClosestLocationWithoutGPS && !IsPanel)
        {
            FirebaseUserManager.instance.OnUserPressed();
            return;
        }

        if (Cargando || objetoCargado == null)
        {
            return;
        }

        if (Vector3.Distance(transform.position, GeoManager.instance.playerTransform.position) >= GeoManager.instance.minDistanceSup && (GeoManager.instance.OnClosestLocationWithoutGPS && !IsPanel))
        {
            return;
        }

        Cargando = true;
        AssetBundleManager.instance.objetoAR = objectAr;
        AssetBundleManager.instance.location = GeoManager.instance._map.WorldToGeoPosition(transform.position).ToString();
        AssetBundleManager.instance.from3DBox = false;

        LocationProviderFactory.Instance.transform.position = Vector3.zero;
        GameManager.instance.CargarEscenaAR();
    }

    private void RefreshNameLanguage()
    {
        gameObject.name = "";
        if (IsPanel)
        {
            gameObject.name = "INFO";
        }
        else if(objectAr != null)
        {
            if (objectAr.languageInfo != null && objectAr.languageInfo[0].language != "")
            {
                foreach (var info in objectAr.languageInfo)
                {
                    if (info.language == LocalizationManager.instance.currentLanguage.ToString())
                    {
                        gameObject.name = info.name;
                    }
                }
            }
        }

        if (gameObject.name == "" && objectAr != null)
        {
            gameObject.name = objectAr.prefabName;
        }

        //Debug.Log("Prefab cargado: " + gameObject.name);
        StartCoroutine(RefreshName());
    }

    private IEnumerator RefreshName()
    {
        //El gameObject.name se cambia en RefreshName
        textoPOI.text = gameObject.name;

        textoPOI.GetComponent<ContentSizeFitter>().horizontalFit = ContentSizeFitter.FitMode.MinSize;
        yield return null;
        textoPOI.GetComponent<ContentSizeFitter>().horizontalFit = ContentSizeFitter.FitMode.PreferredSize;

    }

    public Vector3 ClampMagnitude(Vector3 v, float max, float min)
    {
        double sm = v.sqrMagnitude;
        if (sm > (double)max * (double)max) return v.normalized * max;
        else if (sm < (double)min * (double)min) return v.normalized * min;
        return v;
    }
}
