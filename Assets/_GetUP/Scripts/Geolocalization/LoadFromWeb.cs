﻿using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

public class LoadFromWeb : MonoBehaviour
{
    public void Awake() {
        StartCoroutine(InstantiateObject());
    }

    IEnumerator InstantiateObject() {
        string uri = "https://fedetests.s3-sa-east-1.amazonaws.com/tests";
        UnityWebRequest request = UnityWebRequestAssetBundle.GetAssetBundle(uri, 0);
        yield return request.SendWebRequest();
        AssetBundle bundle = DownloadHandlerAssetBundle.GetContent(request);
        var prefab = bundle.LoadAsset("objeto1_prefab") as GameObject;
        var newGameObject = Instantiate(prefab);
        newGameObject.GetComponent<MeshRenderer>().material = bundle.LoadAsset("objeto1_mat") as Material;
    }
}
