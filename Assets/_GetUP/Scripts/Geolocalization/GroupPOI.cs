﻿using BestHTTP.Extensions;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
[RequireComponent(typeof(Rigidbody))]
public class GroupPOI : MonoBehaviour
{
    public List<PropertiesPOI> poiList = new List<PropertiesPOI>();
    private BoxCollider box;

    public GameObject objetoCargado;
    public bool Destroying = false;

    private float timerSinceCreation = 0;

    private void Awake()
    {
        box = gameObject.AddComponent<BoxCollider>();
        box.isTrigger = true;
        gameObject.layer = LayerMask.NameToLayer("POI");
        GetComponent<Rigidbody>().useGravity = false;
        GetComponent<Rigidbody>().isKinematic = true;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("POI") && other.GetComponent<GroupPOI>())
        {
            if (Vector3.Distance(other.transform.position, transform.position) < 100)
            {
                MergeGroupPOI(other.GetComponent<GroupPOI>());
            }
        }
    }


    private void Update()
    {
        float distance = Vector3.Distance(transform.position, GeoManager.instance.playerTransform.position);
        transform.localScale = distance <= 100 ? Vector3.one : Vector3.one * (distance - 99.9999f) * 0.05f;
        if (transform.localScale.magnitude < 1)
        {
            transform.localScale = Vector3.one;
        }

        if (timerSinceCreation < 0.5f)
        {
            timerSinceCreation += Time.deltaTime;
        }
        else
        {
            if (distance <= 100)
            {
                box.enabled = false;
                objetoCargado.SetActive(false);
                poiList.ForEach(delegate (PropertiesPOI poi) { ActivatePoi(poi, true); });
            }
            if (distance > 100)
            {
                box.enabled = true;
                objetoCargado.SetActive(true);
                poiList.ForEach(delegate (PropertiesPOI poi) { DeActivatePoi(poi); });
            }
        }

    }

    public void MergeGroupPOI(GroupPOI groupPOI)
    {
        if (Destroying || groupPOI.Destroying)
        {
            return;
        }

        foreach (var poi in groupPOI.poiList.ToArray())
        {
            if (!poiList.Contains(poi))
            {
                AddPOI(poi);
            }
            if (poi.groupPOI == groupPOI)
            {
                poi.groupPOI = this;
            }
        }
        groupPOI.Destroying = true;
        Destroy(groupPOI.gameObject);
        GeoManager.instance.groupPOIs.Remove(groupPOI);
    }

    public void AddPOI(PropertiesPOI poi)
    {
        poiList.Add(poi);
        AdjustCollider();

        DeActivatePoi(poi);
    }


    public void RemovePOI(PropertiesPOI poi, bool activateGO)
    {
        poiList.Remove(poi);
        AdjustCollider();

        ActivatePoi(poi, activateGO);

        if (poiList.Count <= 1)
        {
            GeoManager.instance.groupPOIs.Remove(this);
            Destroy(gameObject);
        }
    }

    private static void ActivatePoi(PropertiesPOI poi, bool activateGO)
    {

        foreach (var anim in poi.GetComponentsInChildren<Animator>())
        {
            anim.enabled = true;
        }
        foreach (var meshRenderer in poi.GetComponentsInChildren<MeshRenderer>())
        {
            meshRenderer.enabled = true;
        }
        foreach (var meshRenderer in poi.GetComponentsInChildren<SkinnedMeshRenderer>())
        {
            meshRenderer.enabled = true;
        }
        poi.labelContent.SetActive(activateGO);
    }

    private static void DeActivatePoi(PropertiesPOI poi)
    {
        if (poi == null) { return; }

        foreach (var anim in poi.GetComponentsInChildren<Animator>())
        {
            anim.enabled = false;
        }
        foreach (var meshRenderer in poi.GetComponentsInChildren<MeshRenderer>())
        {
            meshRenderer.enabled = false;
        }
        foreach (var meshRenderer in poi.GetComponentsInChildren<SkinnedMeshRenderer>())
        {
            meshRenderer.enabled = false;
        }
        poi.labelContent.SetActive(false);
    }


    private void AdjustCollider()
    {
        if (poiList.Count > 0 && poiList[0] == null)
        {
            Destroy(gameObject);
            return;
        }

        float xBox = float.MinValue;
        float yBox = float.MinValue;
        float zBox = float.MinValue;
        Vector3 center = Vector3.zero;

        foreach (var pointOfInterest in poiList)
        {
            if (pointOfInterest == null) { continue; }
            center += pointOfInterest.transform.position;
        }

        center /= poiList.Count;
        transform.position = center;

        foreach (var pointOfInterest in poiList)
        {
            if (xBox < pointOfInterest.GetComponent<Collider>().bounds.max.x - transform.position.x)
            {
                xBox = pointOfInterest.GetComponent<Collider>().bounds.max.x - transform.position.x;
            }
            if (yBox < pointOfInterest.GetComponent<Collider>().bounds.max.y - transform.position.y)
            {
                yBox = pointOfInterest.GetComponent<Collider>().bounds.max.y - transform.position.y;
            }
            if (zBox < pointOfInterest.GetComponent<Collider>().bounds.max.z - transform.position.z)
            {
                zBox = pointOfInterest.GetComponent<Collider>().bounds.max.z - transform.position.z;
            }
        }

        box.size = new Vector3(xBox, yBox, zBox);
    }

    public Vector3 ClampMagnitude(Vector3 v, float max, float min)
    {
        double sm = v.sqrMagnitude;
        if (sm > (double)max * (double)max) return v.normalized * max;
        else if (sm < (double)min * (double)min) return v.normalized * min;
        return v;
    }
}
