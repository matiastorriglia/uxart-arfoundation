﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Mapbox.Unity.Location;
using Mapbox.Unity.Utilities;
using Mapbox.Unity.Map;
using Mapbox.Utils;
using UnityEngine.SceneManagement;
using Mapbox.Examples;
using System;

public class GeoManager : MonoBehaviour
{
    public static GeoManager instance;

    [Header("Public")]
    public PositionWithLocationProvider positionWithLocationProvider;
    public Transform playerTransform;
    public float minDistanceSup = 8;
    public float minDistanceDownload = 1000;
    public float desiredScale;

    [Header("Private")]
    [SerializeField] private GameObject prefabArPOI;
    [SerializeField] private GameObject prefabInfoPOI;
    [SerializeField] private bool onClosestLocationWithoutGPS;


    public List<GroupPOI> groupPOIs;

    public AbstractMap _map;

    private ILocationProvider _locationProvider;
    private List<Vector2d> _locationsAr = new List<Vector2d>();
    private List<Vector2d> _locationsInfo = new List<Vector2d>();

    [Header("POIs on Map")]
    //public List<PropertiesPOI> poisOnMap;
    public float timer;
    public float timeToCheckPois = 5;
    private float updateTimer = 0;

    double astronautDistance = double.PositiveInfinity;
    ChannelLocation astroLocation;

    Vector2d currentMapLocation;

    public bool OnClosestLocationWithoutGPS {
        get => onClosestLocationWithoutGPS; set {

            positionWithLocationProvider.MapRelocating = true;

            onClosestLocationWithoutGPS = value;
        }
    }

    private void Start()
    {
        LoadingCanvasController.instance.textImagenCarga.text = LocalizationManager.instance.GetLocalizedValue("");
        instance = this;
        SceneManager.SetActiveScene(SceneManager.GetSceneByName("MapGeolocalization"));
        LocationProviderFactory.Instance.mapManager = _map;
        currentMapLocation = LocationProviderFactory.Instance.DefaultLocationProvider.CurrentLocation.LatitudeLongitude;

        NFTButton.instance.buttonObj.SetActive(false);

        StartCoroutine(FirstStart());
    }

    private void Update()
    {
        CheckUserDistanceToExperienceLocations();
    }

    private void CheckUserDistanceToExperienceLocations()
    {
        if (ChannelManager.instance.CurrentChannel.locations == null || ChannelManager.instance.CurrentChannel.locations.Count == 0)
        {
            Debug.LogWarning("The channel doesn't Have any location on the Locations array");
            return;
        }

        updateTimer += Time.deltaTime;
        if (updateTimer > 2 && !ConnectionManager.instance.CommandsOnQueue) { updateTimer = 0; }
        else { return; } //Asi evitamos que haga el calculo cada frame

        //Searchs the closest location of the Channel
        ChannelLocation closestChannelLocation = null;
        double distToClosestLocation = double.PositiveInfinity;
        Debug.Log(ChannelManager.instance.CurrentChannel.locations.Count);
        foreach (var channelLocation in ChannelManager.instance.CurrentChannel.locations)
        {
            if (channelLocation.locationName == null || channelLocation.location == null || !channelLocation.location.Contains(", ") || channelLocation.locationName == "")
            {
                Debug.LogWarning("Verificar la base de datos, recuerda tener un valor de localizacion que siga el patron \"yxx.xxxxx, yxx.xxxxx\" siendo y opcional y tener un nombre para la localidad");
                continue;
            }

            var currentLatitude = LocationProviderFactory.Instance.DefaultLocationProvider.CurrentLocation.LatitudeLongitude.x;
            var currentLongitude = LocationProviderFactory.Instance.DefaultLocationProvider.CurrentLocation.LatitudeLongitude.y;
            var targetLatitude = Conversions.StringToLatLon(channelLocation.location).x;
            var targetLongitude = Conversions.StringToLatLon(channelLocation.location).y;

            Debug.Log(channelLocation.location);

            var distance = GetDistance(currentLatitude, currentLongitude, targetLatitude, targetLongitude); //LA DISTANCIA ES EN METROS
            //print("LatLong: " + channelLocation.location + "// Metros de distancia: " + distance);

            //Debug.Log(distance + " vs " + distToClosestLocation);

            if (distance < distToClosestLocation)
            {
                closestChannelLocation = channelLocation;
                distToClosestLocation = distance;
            }
        }

        //Debug.Log("Distance: " + distToClosestLocation + ", Channel Range: " + ChannelManager.instance.CurrentChannel.range);

        if (distToClosestLocation < ChannelManager.instance.CurrentChannel.range || closestChannelLocation == null)
        {
            var mapDistance = GetDistance(LocationProviderFactory.Instance.DefaultLocationProvider.CurrentLocation.LatitudeLongitude.x,
                                          LocationProviderFactory.Instance.DefaultLocationProvider.CurrentLocation.LatitudeLongitude.y,
                                          currentMapLocation.x,
                                          currentMapLocation.y);

            Debug.Log(mapDistance + "|" + "Current: " + LocationProviderFactory.Instance.DefaultLocationProvider.CurrentLocation.LatitudeLongitude + ", Map: " + currentMapLocation);

            if (mapDistance >= ChannelManager.instance.CurrentChannel.range && closestChannelLocation != null)
            {
                _map.ResetMap();
                _map.Initialize(Conversions.StringToLatLon(closestChannelLocation.location), _map.AbsoluteZoom);
                _map.ResetMap();
                currentMapLocation = Conversions.StringToLatLon(closestChannelLocation.location);
                positionWithLocationProvider.offsetLocation = Conversions.StringToLatLon(closestChannelLocation.location);

            }
            else {
                positionWithLocationProvider.offsetLocation.x = 0;
                positionWithLocationProvider.offsetLocation.y = 0;
                if (OnClosestLocationWithoutGPS)
                    Debug.Log("right after gps change");
                else
                    Debug.Log("all normal");
                OnClosestLocationWithoutGPS = false;
                return;
                    //En caso de que el usuario este en el rango o no haya una localizacion definida, no es necesario moverlo, por lo que va a usar el GPS real
            }
        }
        else if (!OnClosestLocationWithoutGPS)
        {
            //Si llega hasta aqui, entonces el usuario se transportará
            Debug.Log("should call function after this");
            OnClosestLocationWithoutGPS = true;
            //astronautDistance = distToClosestLocation;
            astroLocation = closestChannelLocation;
            StartCoroutine(ChangeGPSToClosestLocation(closestChannelLocation));
        }
        else if(OnClosestLocationWithoutGPS)
        {
            var aDistance = GetDistance(Conversions.StringToLatLon(astroLocation.location).x, Conversions.StringToLatLon(astroLocation.location).y,
                                        Conversions.StringToLatLon(closestChannelLocation.location).x, Conversions.StringToLatLon(closestChannelLocation.location).y); //LA DISTANCIA ES EN METROS

            if(aDistance >= ChannelManager.instance.CurrentChannel.range)
            {
                OnClosestLocationWithoutGPS = false;
                return;
            }
        }
    }

    private double GetDistance(double longitude, double latitude, double otherLongitude, double otherLatitude)
    {
        var d1 = latitude * (Math.PI / 180.0);
        var num1 = longitude * (Math.PI / 180.0);
        var d2 = otherLatitude * (Math.PI / 180.0);
        var num2 = otherLongitude * (Math.PI / 180.0) - num1;
        var d3 = Math.Pow(Math.Sin((d2 - d1) / 2.0), 2.0) + Math.Cos(d1) * Math.Cos(d2) * Math.Pow(Math.Sin(num2 / 2.0), 2.0);

        return 6376500.0 * (2.0 * Math.Atan2(Math.Sqrt(d3), Math.Sqrt(1.0 - d3)));
    }

    private IEnumerator ChangeGPSToClosestLocation(ChannelLocation closestChannelLocation)
    {
        //print("No hay POIs en la zona, se le redigira a la zona mas cercana");
        Debug.Log("change gps to closest location");
        yield return new WaitUntil(() => LoadingCanvasController.instance.ProcesosEnCurso == 0);
        StartCoroutine(InfoManager.instance.StartMapAlert(closestChannelLocation, 5));
        yield return new WaitForSeconds(6);
        _map.ResetMap();
        _map.Initialize(Conversions.StringToLatLon(closestChannelLocation.location), _map.AbsoluteZoom);
        _map.ResetMap();
        currentMapLocation = Conversions.StringToLatLon(closestChannelLocation.location);
        positionWithLocationProvider.offsetLocation = Conversions.StringToLatLon(closestChannelLocation.location);

        LoadingCanvasController.instance.ProcesosEnCurso++;

        LocationService service = Input.location;
        service.Start(1f, 0.1f);

        yield return new WaitForSeconds(1);
        LoadingCanvasController.instance.ProcesosEnCurso = 0;
    }

    private IEnumerator FirstStart()
    {
        CheckUserDistanceToExperienceLocations();

        yield return new WaitWhile(() => positionWithLocationProvider.MapRelocating);

        LoadingCanvasController.instance.ProcesosEnCurso = 3;

        StartCoroutine(GetObjectsAr());
        StartCoroutine(GetObjectsInfo());
        yield return null;
        StartCoroutine(InicilizarMapbox());
    }

    public IEnumerator GetObjectsAr()
    {
        _locationsAr.Clear();

        CheckUserDistanceToExperienceLocations();
        yield return new WaitWhile(() => ConnectionManager.instance.objectsArManager == null && positionWithLocationProvider.MapRelocating);
        yield return new WaitForSeconds(1);
        foreach (ObjectArData objectAr in ConnectionManager.instance.objectsArManager.GeoLocationsArData)
        {
            //Agrego la ubicacion de los objetos en una lista
            if (objectAr.locations != null && objectAr.locations[0].location != "")
            {
                objectAr.locations.ForEach(delegate (Locations loc)
                {
                    _locationsAr.Add(Conversions.StringToLatLon(loc.location));
                });
            }
        }

        _map.VectorData.SpawnPrefabAtGeoLocation(prefabArPOI, _locationsAr.ToArray(), null, true, "Objetos");
        yield return new WaitForSeconds(0.1f);
        LoadingCanvasController.instance.ProcesosEnCurso--;
    }

    public IEnumerator GetObjectsInfo()
    {
        _locationsInfo.Clear();

        CheckUserDistanceToExperienceLocations();
        yield return new WaitWhile(() => ConnectionManager.instance.objectsArManager == null && positionWithLocationProvider.MapRelocating);
        yield return new WaitForSeconds(1);
        foreach (ObjectArData objectInfo in ConnectionManager.instance.objectsArManager.GeoLocationsInfoData)
        {
            //Agrego la ubicacion de los objetos en una lista
            _locationsInfo.Add(Conversions.StringToLatLon(objectInfo.location));
        }

        //Creo el Punto de Interes con todas las localizaciones
        _map.VectorData.SpawnPrefabAtGeoLocation(prefabInfoPOI, _locationsInfo.ToArray(), null, true, "Info");
        yield return new WaitForSeconds(0.1f);
        LoadingCanvasController.instance.ProcesosEnCurso--;
    }

    public void ReiniciarEscena()
    {
        LoadingCanvasController.instance.textImagenCarga.text = LocalizationManager.instance.GetLocalizedValue("");
        GameManager.instance.CargarGeolocalizacion();
    }

    public IEnumerator InicilizarMapbox()
    {
        //Inicializo el Mapa
        _locationProvider = LocationProviderFactory.Instance.DefaultLocationProvider;
        _locationProvider.OnLocationUpdated += LocationProvider_OnLocationUpdated;
        yield return new WaitForSeconds(0.1f);
        //Quito el Loading
        if (LoadingCanvasController.instance.ProcesosEnCurso > 0)
        {
            LoadingCanvasController.instance.ProcesosEnCurso--;
        }
    }

    void LocationProvider_OnLocationUpdated(Location location)
    {
        _locationProvider.OnLocationUpdated -= LocationProvider_OnLocationUpdated;
        _map.Initialize(location.LatitudeLongitude, _map.AbsoluteZoom);
    }

    public void RefreshGroupPOIs()
    {
        foreach (var gp in groupPOIs)
        {
            Destroy(gp.gameObject);
        }
        groupPOIs.Clear();
    }
}
