﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwipeRotate : MonoBehaviour {
    private Touch touch;
    private Vector2 touchPosition;
    private Quaternion rotationY;
    private float rotateSpeedModifier = 0.1f;

    private void Update() {
        if (Input.touchCount == 1) {
            if (Input.GetTouch(0).phase == TouchPhase.Moved) {
                rotationY = Quaternion.Euler(0f, -Input.GetTouch(0).deltaPosition.x * rotateSpeedModifier, 0f);
                transform.rotation = rotationY * transform.rotation;
            }
        }
    }
}
