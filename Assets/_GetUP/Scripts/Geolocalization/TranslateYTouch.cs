﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TranslateYTouch : MonoBehaviour {
    [SerializeField] private bool holding;

    void Start() {
        holding = false;
    }

    void Update() {
        if (holding) {
            moveY();
        }
        // One finger
        if (Input.touchCount == 1) {

            // Tap on Object
            if (Input.GetTouch(0).phase == TouchPhase.Began) {
                Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
                RaycastHit hit;

                if (Physics.Raycast(ray, out hit, 100f)) {
                    if (hit.transform == transform) {
                        holding = true;
                    }
                }
            }

            // Release 
            if (Input.GetTouch(0).phase == TouchPhase.Ended) {
                holding = false;
            }
        }
    }

    private void moveY() {
        if (Input.GetTouch(0).deltaPosition.y > 0) {
            transform.Translate(Vector3.up * Time.deltaTime);
        } else if (Input.GetTouch(0).deltaPosition.y < 0) {
            transform.Translate(Vector3.down * Time.deltaTime);
        }
        var pos = transform.position;
        pos.y = Mathf.Clamp(pos.y, -1, 1);
        transform.position = pos;
    }
}
