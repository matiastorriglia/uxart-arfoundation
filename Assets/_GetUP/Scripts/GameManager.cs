﻿using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;
using Doozy.Engine.UI;
using DanielLochner.Assets.SimpleScrollSnap;
using System.Collections.Generic;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    [SerializeField] private InfoManager tutorialManager;

    public GameObject DashboardPrefab;

    public Dropdown IdiomaDropdown;
    public UIDrawer LowerDrower;

    public Material SkyMapMaterial;

    //public OSPermissionSubscriptionState statusNot;

    private int HasPlayed;

    public GameObject solapa;

    [Header("Simple Scroll Snap")]
    public RectTransform sliderOptionsContent;
    public List<GameObject> sliderButtons;
    public SimpleScrollSnap sliderController;

    public Camera managerCamera;
    public Canvas managerCanvas;

    public UIButton toggleButton;
    public GameObject overlayGameobject;
    public bool overlayActive;
    public GameObject toggleButtonDown;

    private PortalLoadHandler _portalLoadHandler;

    public bool showSolapa = false;

    //public bool firstPortalLoad = false;

    private void Awake()
    {
        //if (instance != null)
        //{
        //    DestroyImmediate(gameObject);
        //    return;
        //}

        //LoadingCanvasController.instance.ProcesosEnCurso = 0;

        instance = this;
        DontDestroyOnLoad(gameObject);

        StartCoroutine(CargarConDelay());

        if (Application.systemLanguage == SystemLanguage.Spanish)
            setLanguageDropdown(Application.systemLanguage.ToString());
        else
            setLanguageDropdown(SystemLanguage.English.ToString());
    }

    private IEnumerator CargarConDelay()
    {
        yield return new WaitForSeconds(0.5f);
        yield return new WaitWhile(() => ConnectionManager.instance.CommandsOnQueue);
        yield return new WaitWhile(() => ConnectionManager.instance.EmitCommandInProgress);
        //CargarPortalesNoClose();
        CargarGeolocalizacion();

        toggleButton.OnClick.OnTrigger.Event.Invoke();
    }

    private void Start()
    {
        ////En una ocasion se quizo implementar un sistemade notificaciones usando OneSignal, no resultó.
        // Enable line below to enable logging if you are having issues setting up OneSignal. (logLevel, visualLogLevel)
        // OneSignal.SetLogLevel(OneSignal.LOG_LEVEL.INFO, OneSignal.LOG_LEVEL.INFO);

        //OneSignal.StartInit("6fff8cdf-09bd-42e8-8907-a0bac579b0c2")
        //  .HandleNotificationOpened(HandleNotificationOpened)
        //  .EndInit();

        //OneSignal.inFocusDisplayType = OneSignal.OSInFocusDisplayOption.Notification;

        //statusNot = OneSignal.GetPermissionSubscriptionState();

        CheckSliderButtons();
        HasPlayed = PlayerPrefs.GetInt("HasPlayed");
        if (HasPlayed == 1)
        {
            //UIManagerUDAR.instance.OnStartDrag();
            //Invoke("UIOpenChannels", 0.5f);
            //LowerDrower.Open();
        }
        else
        {
            //InfoManager.instance.StartTutorial();
            //solapa.SetActive(true);
            showSolapa = true;
            PlayerPrefs.SetInt("HasPlayed", 1);
        }
    }

    public void CheckTutorial()
    {
        if (toggleButtonDown.activeInHierarchy)
        {
            return;
        }

        HasPlayed = PlayerPrefs.GetInt("HasPlayed");
        if (HasPlayed == 1)
        {
            //UIManagerUDAR.instance.OnStartDrag();
            //Invoke("UIOpenChannels", 0.5f);
            //LowerDrower.Open();
        }
        else
        {
            //InfoManager.instance.StartTutorial();
            PlayerPrefs.SetInt("HasPlayed", 1);
        }
    }

    private void UIOpenChannels()
    { //Llamado desde un Invoker
        UIManagerUDAR.instance.topMenuView.Hide();
    }


    //// Gets called when the player opens the notification.
    //private static void HandleNotificationOpened(OSNotificationOpenedResult result)
    //{

    //}

    public bool CargandoPrincipal;
    private float MatRotation = 0;
    private void Update()
    {

        if (managerCanvas.worldCamera == null)
        {
            managerCanvas.worldCamera = managerCamera;
        }

        if (Input.GetKeyDown(KeyCode.Escape) && tutorialManager.TutorialView.IsShowing)
        {
            return;
        }

        if (Input.GetKeyDown(KeyCode.Escape) && SceneManager.GetActiveScene().name == "ARCorePortals")
        {
            Debug.Log("Saliendo");
            Application.Quit();
            return;
        }

        if (Input.GetKeyDown(KeyCode.Escape) && (SceneManager.GetActiveScene().name == "ARCoreSurface" || SceneManager.GetActiveScene().name == "ARGeolocalization") && DashboardInfoManager.instance.infoViewer.IsVisible)
        {
            DashboardInfoManager.instance.infoViewer.Hide();
            return;
        }

        if (Input.GetKeyDown(KeyCode.Escape) && !CargandoPrincipal)
        {
            CargandoPrincipal = true;
            CargarPrincipal();
        }

        MatRotation += Time.deltaTime;
        if (MatRotation == 360)
        {
            MatRotation = 0;
        }
        SkyMapMaterial.SetFloat("_Rotation", MatRotation);

        if (_portalLoadHandler)
        {
            _portalLoadHandler.SetActivePortal(!overlayGameobject.gameObject.activeInHierarchy);
        }
        else
        {
            _portalLoadHandler = FindObjectOfType<PortalLoadHandler>();
        }
    }

    public void CargarEscenaAR()
    {
        NFTButton.instance.buttonObj.SetActive(false);

        if (ConnectionManager.instance.CommandsOnQueue)
        {
            Debug.LogWarning("No se puede cambiar de escena mientras se descargan datos Socket");
            return;
        }

        LoadingCanvasController.instance.ARLoad();
        SceneManager.LoadScene("ARCoreSurface");

    }
    
    //Llamado desde un botón
    public void SetLanguageOptions(int languageIndex)
    { //Es llamado solo desde el dropdown
        LocalizationManager.instance.isReady = false;
        ConnectionManager.instance.AddEmitCommand("actions_localization", "{'accion':'lookUpLocalization','language':'" + IdiomaDropdown.options[languageIndex].text + "','devolucion':'_receive_localization'}");
        LocalizationManager.instance.currentLanguage = (SystemLanguage)System.Enum.Parse(typeof(SystemLanguage), IdiomaDropdown.options[languageIndex].text);
        if (FirebaseUserManager.instance && FirebaseUserManager.instance.currentUser._id != null)
        {
            FirebaseUserManager.instance.currentUser.language = IdiomaDropdown.options[languageIndex].text;
            FirebaseUserManager.instance.UpdateUser();
        }

    }

    public void setLanguageDropdown(string language)
    {
        for (int i = 0; i < IdiomaDropdown.options.Count; i++)
        {
            if (IdiomaDropdown.options[i].text == language)
            {
                IdiomaDropdown.value = i; //Al cambiar este valor, se llama a SetLanguageOptions(), ahi se pide el socket de la traduccion
            }
        }
    }

    public void CargarGeolocalizacion()
    {
        NFTButton.instance.buttonObj.SetActive(false);

        sliderController.GoToPanel(2);
        if (SceneManager.GetActiveScene().name != "MapGeolocalization")
        {
            if (ConnectionManager.instance.CommandsOnQueue)
            {
                InfoManager.instance.CleanInfo();
                Debug.LogWarning("No se puede cambiar de escena mientras se descargan datos Socket");
                Invoke("CargarGeolocalizacion", 1f);
                return;
            }
            MarkerManager.instance.StopAllCoroutines();
            LoadingCanvasController.instance.textImagenCarga.text = LocalizationManager.instance.GetLocalizedValue("");
            LoadingCanvasController.instance.ProcesosEnCurso++;
            SceneManager.LoadSceneAsync("MapGeolocalization");
        }
        else
        {
            GeoManager.instance.RefreshGroupPOIs();
            LoadingCanvasController.instance.ProcesosEnCurso += 2;

            GeoManager.instance.positionWithLocationProvider.MapRelocating = true;

            GeoManager.instance._map.VectorData.RemovePointsOfInterestSubLayerWithName("Objetos");
            GeoManager.instance._map.VectorData.RemovePointsOfInterestSubLayerWithName("Info");

            StartCoroutine(GeoManager.instance.GetObjectsAr());
            StartCoroutine(GeoManager.instance.GetObjectsInfo());
        }
        LowerDrower.Close();
    }

    public void CargarPortalesNoClose()
    {
        NFTButton.instance.buttonObj.SetActive(false);

        sliderController.GoToPanel(0);
        if (ConnectionManager.instance.CommandsOnQueue)
        {
            Debug.LogWarning("No se puede cambiar de escena mientras se descargan datos Socket");
            Invoke("CargarPortales", 1f);
            return;
        }

        if (SceneManager.GetActiveScene().name != "ARCorePortals")
        {
            MarkerManager.instance.StopAllCoroutines();
            InfoManager.instance.CleanInfo();
            LoadingCanvasController.instance.textImagenCarga.text = LocalizationManager.instance.GetLocalizedValue("");
            LoadingCanvasController.instance.ProcesosEnCurso++;
            SceneManager.LoadSceneAsync("ARCorePortals");
        }
        else
        {
            PortalManager.instance.RestartPortal();
        }
    }

    public void CargarPortales()
    {
        NFTButton.instance.buttonObj.SetActive(false);

        sliderController.GoToPanel(0);
        if (ConnectionManager.instance.CommandsOnQueue)
        {
            Debug.LogWarning("No se puede cambiar de escena mientras se descargan datos Socket");
            Invoke("CargarPortales", 1f);
            return;
        }

        if (SceneManager.GetActiveScene().name != "ARCorePortals")
        {
            MarkerManager.instance.StopAllCoroutines();
            InfoManager.instance.CleanInfo();
            LoadingCanvasController.instance.textImagenCarga.text = LocalizationManager.instance.GetLocalizedValue("");
            LoadingCanvasController.instance.ProcesosEnCurso++;
            SceneManager.LoadSceneAsync("ARCorePortals");
        }
        else
        {
            PortalManager.instance.RestartPortal();
        }
        LowerDrower.Close();
    }

    public void CargarPrincipal()
    {
        NFTButton.instance.buttonObj.SetActive(false);

        if (ConnectionManager.instance.CommandsOnQueue)
        {
            Debug.LogWarning("No se puede cambiar de escena mientras se descargan datos Socket");
            Invoke("CargarPrincipal", 1f);
            return;
        }
        CargarPortales();
        //LoadingCanvasController.instance.textImagenCarga.text = LocalizationManager.instance.GetLocalizedValue("loading_label");
        //LoadingCanvasController.instance.ProcesosEnCurso++;
        //LowerDrower.Close();
        //SceneManager.LoadSceneAsync("MapGeolocalization");
    }

    public void CargarMarcadores()
    {
        NFTButton.instance.buttonObj.SetActive(false);

        sliderController.GoToPanel(3);
        if (ConnectionManager.instance.CommandsOnQueue)
        {
            Debug.LogWarning("No se puede cambiar de escena mientras se descargan datos Socket");
            Invoke("CargarMarcadores", 1f);
            return;
        }
        MarkerManager.instance.StopAllCoroutines();
        InfoManager.instance.CleanInfo();
        LoadingCanvasController.instance.ProcesosEnCurso++;
        LoadingCanvasController.instance.textImagenCarga.text = LocalizationManager.instance.GetLocalizedValue("");
        LowerDrower.Close();
        SceneManager.LoadSceneAsync("Markers");
    }

    public void LoadingPortalFromPortalsSetActive(bool state)
    {
        if (PortalLoadHandler.instance != null)
        {
            PortalLoadHandler.instance.SetActivePortal(state);
        }
    }

    public void CheckSliderButtons()
    {
        foreach (var button in sliderButtons)
        {
            if (button.GetComponent<UnityEngine.UI.Button>() != null)
            {
                button.GetComponent<UnityEngine.UI.Button>().interactable = false;
            }
        }

        //print(sliderButtons[sliderController.TargetPanel].name);
        if (sliderButtons[sliderController.TargetPanel].GetComponent<UnityEngine.UI.Button>() != null)
        {
            sliderButtons[sliderController.TargetPanel].GetComponent<UnityEngine.UI.Button>().interactable = true;
        }
    }
}
