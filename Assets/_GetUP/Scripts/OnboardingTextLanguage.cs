using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class OnboardingTextLanguage : MonoBehaviour
{
    public TextMeshProUGUI text;
    [TextArea]
    [SerializeField] private string _spanish;
    [TextArea]
    [SerializeField] private string _english;

    private void Awake()
    {
        if (Application.systemLanguage == SystemLanguage.Spanish)
        {
            text.text = _spanish;
        }
        else
        {
            text.text = _english;
        }
    }
}
