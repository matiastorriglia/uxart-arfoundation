﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class DashboardPanelTitle : MonoBehaviour
{
    [SerializeField] public TextMeshProUGUI titleText;
    public string category;

    private DashboardInfoData dashboardInfoData;
    public DashboardInfoData DashboardInfoData {
        get => dashboardInfoData; set {
            dashboardInfoData = value;
            if (dashboardInfoData!=null)
            {
                category = dashboardInfoData.category;
                RefreshLanguage();
            }
        }
    }

    private void Awake()
    {
        LocalizationManager.OnChangeLanguage += RefreshLanguage;
    }

    private void OnDestroy()
    {
        LocalizationManager.OnChangeLanguage -= RefreshLanguage;
    }

    private void RefreshLanguage()
    {
        foreach (var info in dashboardInfoData.languageInfo)
        {
            //Cuando encuentra el lenguaje en uso del sistema lo aplica
            if (info.language == LocalizationManager.instance.currentLanguage.ToString())
            {
                titleText.text = info.categoryName;
            }
        }
    }
}
