﻿using UnityEngine;
using UnityEngine.UI;

public class TutorialBallonManager : MonoBehaviour
{
    [SerializeField] private Text tutorialText;
    //[SerializeField] private Text pagesText;
    [SerializeField] private Text nextText;

    [SerializeField] private string tutorialTextInstruction;

    //[SerializeField] private bool HavesPages = true;
    //[SerializeField] Vector2Int currentAndMax;

    private void Awake()
    {
        //pagesText.gameObject.SetActive(HavesPages);
        LocalizationManager.OnChangeLanguage += RefreshLanguage;
    }

    private void OnDestroy()
    {
        LocalizationManager.OnChangeLanguage -= RefreshLanguage;
    }

    private void OnEnable()
    {
        if (LocalizationManager.instance && LocalizationManager.instance.isReady)
        {
            RefreshLanguage();
        }
    }
    public void RefreshLanguage()
    {
        tutorialText.text = LocalizationManager.instance.GetLocalizedValue(tutorialTextInstruction);
        //pagesText.text = currentAndMax.x + " " + LocalizationManager.instance.GetLocalizedValue("ofLabel") + " " + currentAndMax.y;
        nextText.text = LocalizationManager.instance.GetLocalizedValue("nextLabel");
    }
}
