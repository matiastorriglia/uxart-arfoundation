using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NFTButton : MonoBehaviour
{

    public static NFTButton instance;

    public GameObject buttonObj;
    public string url;

    // Start is called before the first frame update
    void Start()
    {
        if(instance == null)
        {
            instance = this;
        }
    }

    public void OpenURL()
    {
        Application.OpenURL(url);
    }
}
