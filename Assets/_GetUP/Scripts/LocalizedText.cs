﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;


public class LocalizedText : MonoBehaviour
{

    public string key;

    private void Awake()
    {
        LocalizationManager.OnChangeLanguage += RefreshLanguage;
    }

    private void OnDestroy()
    {
        LocalizationManager.OnChangeLanguage -= RefreshLanguage;
    }

    private void OnEnable()
    {
        if (LocalizationManager.instance && LocalizationManager.instance.isReady)
        {
            RefreshLanguage();
        }
    }

    private void RefreshLanguage()
    {
        if (GetComponent<Text>())
        {
            Text text = GetComponent<Text>();
            text.text = LocalizationManager.instance.GetLocalizedValue(key);
        }
        else if(GetComponent<TextMeshProUGUI>())
        {
            TextMeshProUGUI text = GetComponent<TextMeshProUGUI>();
            text.text = LocalizationManager.instance.GetLocalizedValue(key);
        }
        else
        {
            Debug.LogWarning("No hay un componente Text en el objeto", gameObject);
        }
    }
}
