﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SplashManager : MonoBehaviour
{
    [SerializeField] private float timeToLoadScene;
    private float timerToLoadScene=0;

    [SerializeField] private string sceneName;

    [SerializeField] private Image logo;
    [SerializeField] private Image fadeImage;

    private void Update()
    {
        timerToLoadScene += Time.deltaTime;

        logo.transform.localScale = Vector3.one + Vector3.one * (timerToLoadScene / timeToLoadScene) * 0.45f;

        if (timerToLoadScene > timeToLoadScene)
        {
            fadeImage.enabled = true;
            SceneManager.LoadScene(sceneName);
        }
    }

}
