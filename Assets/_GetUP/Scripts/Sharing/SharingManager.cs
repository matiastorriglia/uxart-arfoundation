﻿using DanielLochner.Assets.SimpleScrollSnap;
using NatCorder.Examples;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Events;
using Mapbox.Platform;
using UnityEngine.UI;
#if UNITY_ANDROID || UNITY_IOS
using NativeGalleryNamespace;
#endif

public class SharingManager : MonoBehaviour
{
    public static SharingManager instance;

    public ReplayCam replayCam;
    //public GameObject ShareSign;

    [SerializeField] private RectTransform feedContent;
    [SerializeField] private GameObject feedPrefab;

    public List<CanvasGroup> uis;
    public GameObject shareUI;
    public GameObject photoImageUXart;
    public CanvasGroup recordButtonCG;
    public Image recordButtonColor;
    public GameObject stopButton;
    public Animator previewAnim;

    [SerializeField] private RawImage previewImage;

    [SerializeField] public bool Recording = false;

    public Dictionary<string, string> idUsersDictionary = new Dictionary<string, string>();
    public List<string> idFeedsList = new List<string>();
    public UnityEvent OnDictionaryUpdated = new UnityEvent();

    byte[] jpgBytes;
    string filePath;

    public CanvasGroup confirmCG;
    public Button videoPlayButton;

    private void Awake()
    {
        if (instance != null)
        {
            DestroyImmediate(gameObject);
            return;
        }
        instance = this;
        DontDestroyOnLoad(gameObject);
    }

    #region ShareToMedia

    public void SetUIActive(bool state)
    {
        uis.ForEach(delegate (CanvasGroup cg) { cg.alpha = state ? 1 : 0; cg.blocksRaycasts = cg.interactable = state; });
        shareUI.SetActive(!state);
        shareUI.GetComponent<Canvas>().enabled = !state;
        shareUI.GetComponent<GraphicRaycaster>().enabled = !state;
    }

    public void StartRecording()
    {
        GetComponent<ReplayCam>().StartRecording();
        //stopButton.SetActive(true);
        //recordButtonCG.alpha = 0;
        //recordButtonCG.blocksRaycasts = recordButtonCG.interactable = false;
        recordButtonColor.color = Color.red;
        Recording = true;
        //ShareSign.SetActive(true);
    }

    private void Update()
    {
        //if (Recording && Input.GetMouseButtonUp(0))
        //{
        //    GetComponent<ReplayCam>().StopRecording();
        //}
    }

    public void ShareImage()
    {
        if (!Recording)
        {
            StartCoroutine(TakeSSAndShare());
        }
        else
        {
            replayCam.StopRecording();
        }
    }

    public void OpenCameraUI()
    {
        SetUIActive(false);
        photoImageUXart.SetActive(true);
        //stopButton.SetActive(false);
        recordButtonCG.alpha = 1;
        recordButtonCG.blocksRaycasts = recordButtonCG.interactable = true;
        Recording = false;
        recordButtonColor.color = Color.white;
    }

    public void CheckForFeed(Vector2 scrollPosition)
    {
        if (scrollPosition.y <= 0 && !ConnectionManager.instance.CommandsOnQueue)
        {
            //DownloadFeed();
        }
    }

    public void CleanFeed()
    {
        idFeedsList.Clear();
        foreach (Transform child in feedContent) { Destroy(child.gameObject); }
    }

    private IEnumerator TakeSSAndShare()
    {
        recordButtonCG.alpha = 0;
        recordButtonCG.blocksRaycasts = recordButtonCG.interactable = false;
        yield return new WaitForEndOfFrame();

        Texture2D ss = new Texture2D(Screen.width, Screen.height, TextureFormat.ARGB32, false);
        ss.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
        ss.Apply();

        filePath = Path.Combine(Application.temporaryCachePath, "shared img.png");

        jpgBytes = ss.EncodeToJPG();
        //Destroy(ss);
        File.WriteAllBytes(filePath, jpgBytes);
        filePath = filePath.Replace('\\', '/');

        previewImage.texture = ss;

        //make a separate function called by the Confirm button
        OpenConfirmCG();
    }
    
    public void OpenConfirmCG()
    {
        videoPlayButton.gameObject.SetActive(Recording);
        previewImage.gameObject.SetActive(!Recording);

        Recording = false;

        confirmCG.gameObject.SetActive(true);
        previewAnim.Play("CameraPreview", -1, 0);
    }

    public void SaveScreenshot()
    {
        if (!Recording)
        {
            string fileName = "SharedImage_" + System.DateTime.Now.Year + "-" + System.DateTime.Now.Month + "-" + System.DateTime.Now.Day + "_" +
                                    System.DateTime.Now.Hour + System.DateTime.Now.Minute + System.DateTime.Now.Second + ".jpg";
            NativeGallery.SaveImageToGallery(jpgBytes, "UXArt", fileName);
        }
        else
        {
            string fileName = "Video_" + System.DateTime.Now.Year + "-" + System.DateTime.Now.Month + "-" + System.DateTime.Now.Day + "_" +
                        System.DateTime.Now.Hour + System.DateTime.Now.Minute + System.DateTime.Now.Second + ".mp4";
            NativeGallery.SaveVideoToGallery(File.ReadAllBytes(replayCam.GetPreviewPath()), "UXArt", fileName);
            File.Delete(replayCam.GetPreviewPath());
        }


        confirmCG.gameObject.SetActive(false);

        //if (!Recording) {
        //    SetUIActive(true);
        //}

        SetUIActive(true);
        Recording = false;
    }

    public void CancelShare() {

        confirmCG.gameObject.SetActive(false);

        //if (!Recording) {
        //    SetUIActive(true);
        //}

        SetUIActive(true);
        Recording = false;
    }

    public void ContinueShare() {

        confirmCG.gameObject.SetActive(false);

        if (!Recording) {
            ContinueImageShare();
        }else {
            ContinueRecordingShare();
        }
    }

    public void ContinueImageShare() {
        UploadFeed(jpgBytes, "image");

#if UNITY_EDITOR
        //EditorUtility.OpenWithDefaultApp(path);
        new NativeShare().AddFile(filePath).SetSubject("UXART").SetText("#UXart").Share();
#elif UNITY_IOS
        //Handheld.PlayFullScreenMovie("file://" + filePath);
        new NativeShare().AddFile("file://" + filePath).SetSubject("UXART").SetText("#UXart").Share();
#elif UNITY_ANDROID
        //Handheld.PlayFullScreenMovie(filePath);
        new NativeShare().AddFile(filePath).SetSubject("UXART").SetText("#UXart").Share();
#endif

        SetUIActive(true);
    }

    public void ContinueRecordingShare() {
        GetComponent<ReplayCam>().FinishRecording();
    }

    #endregion

    #region Feed
    public void DownloadFeed()
    {
        ConnectionManager.instance.AddEmitCommand("actions_feed", "{'accion':'downloadFeed','quantity':'1','channel_id':'" + ChannelManager.instance.currentChannelId + "','devolucion':'_receive_downloadfeed'}");
    }

    int tryCount = 0;
    public void SetFeedContent(string resulting)
    {
        FeedPacket feedPacket = JsonConvert.DeserializeObject<FeedPacket>(resulting);
        //Debug.Log(FeedPacket.result.mensaje);

        if (feedPacket.result.aviso)
        {
            foreach (var feed in feedPacket.result.result_feed)
            {
                if (idFeedsList.Contains(feed._id) && tryCount < 5)
                {
                    DownloadFeed();
                    tryCount++;
                    return;
                }

                if (tryCount >= 5) //Por si se acerca a no tener mas fotos en la DB
                {
                    tryCount = 0;
                    return;
                }

                idFeedsList.Add(feed._id);

                var feedComp = Instantiate(feedPrefab, feedContent).GetComponent<FeedContent>();
                feedComp.gameObject.transform.localPosition = new Vector3(feedComp.gameObject.transform.localPosition.x, feedComp.gameObject.transform.localPosition.y, 0);
                feedComp.SetActiveFeedType(feed.feedType, ConnectionManager.instance.storageURL + "Feed/" + feed.user_id + "/" + feed.feedPath);
                feedComp.likeButton.targetGraphic.color = Color.gray;
                feedComp.userId = feed.user_id;
                feedComp.feedId = feed._id;
                feedComp.ViewsCount = feed.views;
                if (feed.users_liked != null)
                {
                    if (feed.users_liked.Contains(FirebaseUserManager.instance.currentUser._id))
                    {
                        feedComp.likeButton.targetGraphic.color = Color.white;
                        feedComp.likeButton.image.overrideSprite = feedComp.aplausoPrendido;
                    }
                    feedComp.LikesCount = feed.users_liked.Count;
                }

                if (idUsersDictionary.ContainsKey(feed.user_id))
                {
                    feedComp.nameText.text = idUsersDictionary[feed.user_id];
                }
                else
                {
                    {
                        ConnectionManager.instance.AddEmitCommand("actions_feed", "{'accion':'getUserName','_id':'" + feed.user_id + "', 'devolucion':'_receive_userName'}");
                    }
                }
                feedComp.GiveView();
            }
        }
    }

    public void AddUserToDictionary(string resulting)
    {
        FeedUserPacket feedUserPacket = JsonConvert.DeserializeObject<FeedUserPacket>(resulting);
        if (feedUserPacket.result.aviso)
        {
            if (!idUsersDictionary.ContainsKey(feedUserPacket.result.user_id))
            {
                idUsersDictionary.Add(feedUserPacket.result.user_id, feedUserPacket.result.userName);
            }
            if (OnDictionaryUpdated != null) { OnDictionaryUpdated.Invoke(); }
        }
        else
        {
            if (OnDictionaryUpdated != null) { OnDictionaryUpdated.Invoke(); }
            Debug.LogWarning("El usuario no existe en la base de datos, pudo ser eliminado");
        }
    }

    public void UploadFeed(byte[] arrayBytes, string feedType)
    {
        if (FirebaseUserManager.instance.currentUser != null && FirebaseUserManager.instance.currentUser._id != null && FirebaseUserManager.instance.currentUser._id != "")
        {
            string base64string = Convert.ToBase64String(arrayBytes);
            ConnectionManager.instance.AddEmitCommand("actions_feed", "{'accion':'uploadFeed'," +
                                                                      "'user_id':'" + FirebaseUserManager.instance.currentUser._id + "'," +
                                                                      "'feedFile':'" + base64string + "'," +
                                                                      "'feedType':'" + feedType + "'," +
                                                                      "'channel_id':'" + ChannelManager.instance.currentChannelId + "'," +
                                                                      "'devolucion':'_recibe_status'}"
                                                        , true);
        }
        else
        {
            Debug.LogWarning("No hay usuario conectado");
        }
    }
    #endregion
}
