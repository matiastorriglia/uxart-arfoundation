﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Doozy.Engine.UI;

public class UIManagerUDAR : MonoBehaviour
{
    public static UIManagerUDAR instance;

    public UIView topMenuView;

    public GameObject menuDown;
    public GameObject dragContainer;

    [Header("Channels")]
    public Transform channelsContainer;
    public GameObject channelPrefab;

    //public Canvas masterCanvas;


    //public void OnSceneLoad(Scene scene, LoadSceneMode loadSceneMode) {
    //    ConfigScreenCamera();
    //}

    void Awake()
    {
        if (instance)
        {
            Destroy(gameObject);
            return;
        }

        instance = this;
        //No es necesario el "don't destroy" ya que es hijo de GameManager
        //ConfigScreenCamera();

        //SceneManager.sceneLoaded += OnSceneLoad;
    }

    //private void OnDestroy() {
    //    SceneManager.sceneLoaded -= OnSceneLoad;
    //}

    //void ConfigScreenCamera()
    //{
    //    masterCanvas.worldCamera = GameObject.FindObjectOfType<Camera>();
    //    masterCanvas.planeDistance = 1;
    //}

    public void OnStartDrag()
    {
        menuDown.transform.SetParent(dragContainer.transform);
    }
}
