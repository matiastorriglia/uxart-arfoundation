﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.Networking;
using RenderHeads.Media.AVProVideo;
using Firebase.Auth;

public class FeedContent : MonoBehaviour
{
    public enum FeedType { Image, Video }
    public FeedType feedType;

    [Header("Game Object Type")]
    public GameObject photoGO;
    public GameObject videoGO;

    [Header("Components")]
    public TextMeshProUGUI nameText;
    [SerializeField] private Image feedImage;
    [SerializeField] private DisplayUGUI videoUGUI;
    [SerializeField] private GameObject playImage;
    [SerializeField] private TextMeshProUGUI likesText;
    [SerializeField] private TextMeshProUGUI viewsText;
    [SerializeField] public Button likeButton;

    [Header("Sprite")]
    [SerializeField] public Sprite aplausoApagado;
    [SerializeField] public Sprite aplausoPrendido;


    [Header("Debug")]
    [SerializeField] private bool IsPlaying;

    [TextArea]
    [SerializeField] private string url;

    public string feedId;
    public string userId;

    private int likesCount = 0;
    private int viewsCount = 0;

    public bool NameUpdated;
    private bool CoroutineStarted;
    public string Url {
        get { return url; }
        set { url = value; }
    }

    public int LikesCount { get => likesCount; set { likesCount = value; likesText.text = value.ToString(); } }
    public int ViewsCount { get => viewsCount; set { viewsCount = value; viewsText.text = value.ToString(); } }

    private void OnEnable()
    {
        StartFeed();
    }

    private void StartFeed()
    {
        if (!isActiveAndEnabled) { return; }

        if (feedType == FeedType.Image && url != "" && feedImage.sprite == null && !CoroutineStarted)
        {
            StartCoroutine(GetImage(url));
        }

        if (feedType == FeedType.Video)
        {
            videoUGUI.CurrentMediaPlayer.m_Loop = true;
            videoUGUI.CurrentMediaPlayer.OpenVideoFromFile(MediaPlayer.FileLocation.AbsolutePathOrURL, url, false);
        }
    }

    public void ToggleVideo()
    {
        IsPlaying = !IsPlaying;

        if (IsPlaying)
        {
            playImage.SetActive(false);
            videoUGUI.CurrentMediaPlayer.Play();
        }
        else
        {
            playImage.SetActive(true);
            videoUGUI.CurrentMediaPlayer.Pause();
        }
    }

    public void SetActiveFeedType(string feedType, string url)
    {
        SharingManager.instance.OnDictionaryUpdated.AddListener(UpdateName);
        switch (feedType)
        {
            case "image":
                this.feedType = FeedType.Image;
                this.url = url;
                photoGO.SetActive(true);
                videoGO.SetActive(false);
                break;
            case "video":
                this.feedType = FeedType.Video;
                videoUGUI.CurrentMediaPlayer.m_VideoPath = url;
                this.url = url;
                photoGO.SetActive(false);
                videoGO.SetActive(true);
                break;
            default:
                break;
        }
        StartFeed();
    }

    IEnumerator GetImage(string feedImageURL)
    {
        CoroutineStarted = true;
        // Start a download of the given URL

        using (UnityWebRequest uwr = UnityWebRequestTexture.GetTexture(feedImageURL))
        {
            // Wait for download to complete
            yield return uwr.SendWebRequest();

            if (uwr.isNetworkError || uwr.isHttpError)
            {
                Debug.Log(uwr.error);
                yield break;
            }

            // assign texture
            var texture = DownloadHandlerTexture.GetContent(uwr);

            feedImage.overrideSprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), Vector2.one * 0.5f);
            //feedImage.preserveAspect = true;
        }
    }

    public void UpdateName()
    {
        if (NameUpdated) { return; }

        if (!SharingManager.instance.idUsersDictionary.ContainsKey(userId))
        {
            nameText.text = "Usuario Inexistente o Borrado: " + userId;
        }
        else
        {
            nameText.text = SharingManager.instance.idUsersDictionary[userId];
        }


        NameUpdated = true;
    }

    public void GiveView()
    {
        ConnectionManager.instance.AddEmitCommand("actions_feed", "{'accion':'giveView','_id':'" + feedId + "','devolucion':'_receive_feedResponse'}");
        ViewsCount++;
    }

    public void ToggleLike()
    {
        if (likeButton.targetGraphic.color == Color.white)
        {
            likeButton.image.overrideSprite = aplausoApagado;
            likeButton.targetGraphic.color = Color.gray;
            ConnectionManager.instance.AddEmitCommand("actions_feed", "{'accion':'takeOutLike','_id':'" + feedId + "','_userId':'" + FirebaseUserManager.instance.currentUser._id + "','devolucion':'_receive_feedResponse'}");
            LikesCount--;
        }
        else
        {
            likeButton.image.overrideSprite = aplausoPrendido;
            likeButton.targetGraphic.color = Color.white;
            ConnectionManager.instance.AddEmitCommand("actions_feed", "{'accion':'giveLike','_id':'" + feedId + "','_userId':'" + FirebaseUserManager.instance.currentUser._id + "','devolucion':'_receive_feedResponse'}");
            LikesCount++;
        }
    }
}
