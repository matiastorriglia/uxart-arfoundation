﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class VideoBillboard : MonoBehaviour
{

    Camera arCam;
    public VideoPlayer videoPlayer;
    //public VideoPlayer shadowPlayer;
    public AudioSource audioSource;
    public MeshRenderer videoRenderer;

    // Start is called before the first frame update
    void Start()
    {
        videoPlayer.SetTargetAudioSource(0, audioSource);
        arCam = GameObject.Find("AR Root").GetComponent<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        if (arCam)
        {
            Vector3 previousEulerAngles = transform.eulerAngles;
            transform.LookAt(arCam.transform);
            transform.eulerAngles = new Vector3(previousEulerAngles.x, -transform.eulerAngles.y, previousEulerAngles.z);
        }
    }
}
