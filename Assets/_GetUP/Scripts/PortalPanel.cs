﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PortalPanel : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] private TextMeshProUGUI textTitle;
    [SerializeField] public Button button;
    [SerializeField] public Image image;

    private PortalData portalData;

    public PortalData PortalData {
        get => portalData; set {
            portalData = value;
            if (portalData != null)
            {
                RefreshLanguage();
            }
        }
    }

    private void Awake()
    {
        LocalizationManager.OnChangeLanguage += RefreshLanguage;
    }

    private void OnDestroy()
    {
        LocalizationManager.OnChangeLanguage -= RefreshLanguage;
    }

    private void OnEnable()
    {
        if (LocalizationManager.instance && LocalizationManager.instance.isReady)
        {
            RefreshLanguage();
        }
    }

    private void RefreshLanguage()
    {
        textTitle.transform.localPosition = new Vector3(textTitle.transform.localPosition.x, textTitle.transform.localPosition.y, -1);

        if (portalData == null)
        {
            return;
        }
        foreach (var info in portalData.languageInfo)
        {
            //Cuando encuentra el lenguaje en uso del sistema lo aplica
            if (info.language == LocalizationManager.instance.currentLanguage.ToString())
            {
                textTitle.text = info.name;
            }
        }
    }

    public void SetPortalLoadImage()
    {
        GameObject.Find("PortalManager").GetComponent<PortalManager>().SetPortalImage(image.overrideSprite);
    }
}
