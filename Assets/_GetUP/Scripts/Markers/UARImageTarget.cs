﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;
using DanielLochner.Assets.SimpleScrollSnap;
using UnityEngine.Video;

public class UARImageTarget : MonoBehaviour, ITrackableEventHandler {

    [HideInInspector] public TrackableBehaviour mTrackableBehaviour;

    public string database;
    public string imageTarget;

    public VideoPlayer videoPlayer;

    public bool playVideoOnStartup = false;
    public bool forceButtons = false;

    public TarjetaPromociones tarjeta;

    public bool resetVideoOnBuffer = false;
    double lastTimePlayed;

    public void AssignComponents() {
        if (videoPlayer == null) {
            videoPlayer = GetComponentInChildren<VideoPlayer>();
        }

        if (videoPlayer) {
            //videoPlayer.Stop();
            //videoPlayer.SetDirectAudioMute(0, false);
        }

        if (mTrackableBehaviour == null) {
            mTrackableBehaviour = GetComponent<TrackableBehaviour>();
        }

        if (mTrackableBehaviour) {
            mTrackableBehaviour.RegisterTrackableEventHandler(this);
        }
    }

    void Start() {
        if (videoPlayer == null) {
            videoPlayer = GetComponentInChildren<VideoPlayer>();
        }

        if (forceButtons)
        {
            if (tarjeta)
            {
                tarjeta.OpenButtons();
            }

            if (tarjeta)
            {
                tarjeta.OpenButtons();
            }
            else
            {
                tarjeta = GetComponent<TarjetaPromociones>();
                if (tarjeta)
                    tarjeta.OpenButtons();
            }
        }

        if (videoPlayer) {
            //videoPlayer.Stop();
            //videoPlayer.SetDirectAudioMute(0, false);
        }


        if (videoPlayer && !playVideoOnStartup)
        {
            videoPlayer.Stop();
            videoPlayer.SetDirectAudioMute(0, false);
        }

        if (mTrackableBehaviour == null) {
            mTrackableBehaviour = GetComponent<TrackableBehaviour>();

        }
        if (mTrackableBehaviour) {
            mTrackableBehaviour.RegisterTrackableEventHandler(this);
        }
    }

    void Update()
    {
        if (videoPlayer)
        {
            if (videoPlayer.isPlaying && (Time.frameCount % (int)(videoPlayer.frameRate + 1)) == 0)
            {
                //if the video time is the same as the previous check, that means it's buffering cuz the video is Playing.
                if (lastTimePlayed == videoPlayer.time && resetVideoOnBuffer)//buffering
                {
                    videoPlayer.enabled = false;
                    videoPlayer.enabled = true;
                }

                lastTimePlayed = videoPlayer.time;
            }
        }
    }

    public void OnTrackableStateChanged(TrackableBehaviour.Status previousStatus, TrackableBehaviour.Status newStatus) {
        if (newStatus == TrackableBehaviour.Status.DETECTED ||
            newStatus == TrackableBehaviour.Status.TRACKED ||
            newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED) {
            if (!videoPlayer.isPlaying || videoPlayer.isPaused) {
                videoPlayer.Play();
                videoPlayer.SetDirectAudioMute(0, false);
            }
            if (GetComponentInChildren<Canvas>()) {
                GetComponentInChildren<Canvas>().worldCamera = MarkerPanelManager.instance.MarkerCamera;

            }

            if (GetComponent<TarjetaPromociones>()) {
                GetComponent<TarjetaPromociones>().OpenButtons();
                if (GetComponentInChildren<SimpleScrollSnap>()) {
                    GetComponent<TarjetaPromociones>().onVideoSlider(GetComponentInChildren<SimpleScrollSnap>());
                }
            }
            MarkerPanelManager.instance.DeactivateInstructive();
        } else {
            if (videoPlayer) {
                videoPlayer.Pause();
                videoPlayer.SetDirectAudioMute(0, true);
            }

            if (GetComponent<TarjetaPromociones>()) {
                GetComponent<TarjetaPromociones>().CloseButtons();
            }

            MarkerPanelManager.instance.ActivateInstructive();
        }
    }
}
