﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MarkerPanelManager : MonoBehaviour
{
    public static MarkerPanelManager instance;
    public Image imagePanel;
    public Text textPanel;
    public Camera MarkerCamera;

    private void Awake() {
        if (instance!=null) {
            DestroyImmediate(gameObject);
            return;
        }

        instance = this;


        imagePanel = GetComponent<Image>();
        textPanel = GetComponentInChildren<Text>();
    }

    private void Start() {
        SceneManager.SetActiveScene(SceneManager.GetSceneByName("Markers"));
        StartCoroutine(InfoManager.instance.StartARalert(5));
    }

    public void DeactivateInstructive() {
        if (imagePanel == null)
        {
            return;
        }

        imagePanel.enabled = false;
        textPanel.enabled = false;
    }

    public void ActivateInstructive() {
        if (imagePanel==null)
        {
            return;
        }
        imagePanel.enabled = true;
        textPanel.enabled = true;
    }
}
