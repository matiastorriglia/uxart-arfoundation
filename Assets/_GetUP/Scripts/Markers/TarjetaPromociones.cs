﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DanielLochner.Assets.SimpleScrollSnap;
using UnityEngine.Video;

public class TarjetaPromociones : MonoBehaviour
{
    public Image expandedImage;
    public CanvasGroup expandedCanvasGroup;
    public Animator anim;

    public string urlVideoButton;

    public TextMeshProUGUI textButton1;
    public TextMeshProUGUI textButton2;
    public TextMeshProUGUI textButton3;
    public TextMeshProUGUI textButton4;
    public TextMeshProUGUI textButton5;

    public string urlButton1;
    public string urlButton2;
    public string urlButton3;
    public string urlButton4;
    public string urlButton5;

    public string urlSliderButton1;
    public string urlSliderButton2;
    public string urlSliderButton3;

    public void onVideoSlider(SimpleScrollSnap simpleScrollSnap)
    {
        for (int i = 0; i < simpleScrollSnap.Panels.Length; i++)
        {
            if (simpleScrollSnap.Panels[i].GetComponent<VideoPlayer>())
            {
                if (i == simpleScrollSnap.TargetPanel)
                {
                    simpleScrollSnap.Panels[i].GetComponent<VideoPlayer>().Play();
                    simpleScrollSnap.Panels[i].GetComponent<VideoPlayer>().SetDirectAudioMute(0, false);
                }
                else
                {
                    //simpleScrollSnap.Panels[i].GetComponent<VideoPlayer>().Stop();
                    simpleScrollSnap.Panels[i].GetComponent<VideoPlayer>().SetDirectAudioMute(0, true);
                }
            }
        }
    }

    public void OpenButtons()
    {
        anim.SetTrigger("OpenButtons");
    }

    public void CloseButtons()
    {
        anim.SetTrigger("CloseButtons");
    }

    public void onSliderButton1()
    {
        Debug.Log("URL: " + urlSliderButton1);
        Application.OpenURL(urlSliderButton1);
    }

    public void onSliderButton2()
    {
        Debug.Log("URL: " + urlSliderButton2);
        Application.OpenURL(urlSliderButton2);
    }

    public void onSliderButton3()
    {
        Debug.Log("URL: " + urlSliderButton3);
        Application.OpenURL(urlSliderButton3);
    }

    public void onVideoButton()
    {
        Debug.Log("URL: " + urlVideoButton);
        Application.OpenURL(urlVideoButton);
    }

    public void onButton1()
    {
        Debug.Log("URL: " + urlButton1);
        Application.OpenURL(urlButton1);
    }

    public void onButton2()
    {
        Debug.Log("URL: " + urlButton2);
        Application.OpenURL(urlButton2);
    }

    public void onButton3()
    {
        Debug.Log("URL: " + urlButton3);
        Application.OpenURL(urlButton3);
    }

    public void onButton4()
    {
        Debug.Log("URL: " + urlButton4);
        Application.OpenURL(urlButton4);
    }

    public void onButton5()
    {
        Debug.Log("URL: " + urlButton5);
        Application.OpenURL(urlButton5);
    }

    public void ExpandPromo(Image image)
    {
        expandedImage.overrideSprite = image.overrideSprite;
        expandedCanvasGroup.alpha = 1;
        expandedCanvasGroup.blocksRaycasts = true;
        expandedCanvasGroup.interactable = true;
        expandedImage.preserveAspect = true;
    }

    public void ReducePromo()
    {
        expandedImage.overrideSprite = null;
        expandedCanvasGroup.alpha = 0;
        expandedCanvasGroup.blocksRaycasts = false;
        expandedCanvasGroup.interactable = false;
    }
}
