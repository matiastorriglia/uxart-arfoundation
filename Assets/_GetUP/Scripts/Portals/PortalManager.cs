﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
using DanielLochner.Assets.SimpleScrollSnap;

using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using TMPro;
using RenderHeads.Media.AVProVideo;
using BestHTTP.SecureProtocol.Org.BouncyCastle.Asn1.Mozilla;
using UnityEngine.UI;

using UnityEngine.XR.ARFoundation;
using UnityEngine.Experimental.XR;
using UnityEngine.XR.ARSubsystems;

public class PortalManager : MonoBehaviour
{
    public static PortalManager instance;

    [SerializeField] private XRSurfaceController xRSurface;

    public Canvas portalCanvas;
    public GameObject portal;
    public GameObject portalImagePrefab;
    public Image panelInstructivo;
    public Image panelAbertura;

    public SimpleScrollSnap simpleScrollSnap;

    //private VideoPlayer PortalVideoPlayer;
    [SerializeField] private MediaPlayer portalMediaPlayer;

    [SerializeField] private TextMeshPro portalTitleText;

    public CanvasGroup portalLoadCG;
    public Image portalLoadImg;

    public GameObject leftButton, rightButton;

    private ARRaycastManager arRay;
    [SerializeField] ARPlaneManager arPlane;
    private ARSessionOrigin arOrigin;

    [SerializeField] private Camera cam;

    static List<ARRaycastHit> hits = new List<ARRaycastHit>();

    public GameObject portalParent;
    public bool first = true;

    private void Awake() {
        //portalCanvas.planeDistance = 100;
        arOrigin = FindObjectOfType<ARSessionOrigin>();
        if (arOrigin)
            arRay = arOrigin.GetComponent<ARRaycastManager>();
    }

    private void Start() {
        instance = this;
        //PortalVideoPlayer = portal.GetComponentInChildren<VideoPlayer>();
        StartCoroutine(CargarObjetos());
        SceneManager.SetActiveScene(SceneManager.GetSceneByName("ARCorePortals"));
        GetComponentInChildren<Canvas>().worldCamera = GameObject.FindObjectsOfType<Camera>()[1];
        GetComponentInChildren<Canvas>().planeDistance = 100;
        portalTitleText.gameObject.SetActive(false);
        //portalCanvas.planeDistance = 100;
        StartCoroutine(InfoManager.instance.StartARalert(5));
        //StartCoroutine(PortalDelay());

        NFTButton.instance.buttonObj.SetActive(false);

        if (GameManager.instance.showSolapa)
        {
            GameManager.instance.solapa.SetActive(true);
        }

        //if (!GameManager.instance.firstPortalLoad)
        //{
        //    GameManager.instance.firstPortalLoad = true;
        //    MarkerManager.instance.StopAllCoroutines();
        //    InfoManager.instance.CleanInfo();
        //    LoadingCanvasController.instance.textImagenCarga.text = LocalizationManager.instance.GetLocalizedValue("");
        //    LoadingCanvasController.instance.ProcesosEnCurso++;
        //    SceneManager.LoadSceneAsync("ARCorePortals");
        //}
    }

    public IEnumerator PortalDelay()
    {
        yield return new WaitForSeconds(5);
        first = false;
    }

    private void OnDestroy() {
        instance = null;
    }

    void Update()
    {

        if (!portal.activeInHierarchy && !first)
        {
            if (arRay.Raycast(cam.ViewportToScreenPoint(new Vector2(0.5f, 0.5f)), hits, TrackableType.PlaneWithinPolygon))
            {
                var hitPose = hits[0].pose;
                portalParent.transform.position = hitPose.position;
                hits.Clear();
                OnPortalAttached();
                first = true;
            }
        }
    }

    private IEnumerator CargarObjetos() {
        //portalCanvas.planeDistance = 100;
        portalMediaPlayer.Stop();
        portalMediaPlayer.CloseVideo();
        //PortalVideoPlayer.Stop();
        //PortalVideoPlayer.targetTexture.Release();

        //Limpia los paneles que llevan a los portales
        if (simpleScrollSnap.Panels != null) {
            for (int i = 0; i < simpleScrollSnap.Panels.Length + 1; i++) {
                simpleScrollSnap.RemoveFromBack();
            }

            if (simpleScrollSnap.Panels.Length != 0)
            {
                for(int i = 0; i < simpleScrollSnap.Content.transform.childCount; i++) {
                    Destroy(simpleScrollSnap.Content.transform.GetChild(i).gameObject);
                }

                simpleScrollSnap.EmptyArray();
            }
        }

        //while(simpleScrollSnap.transform.childCount > 0)
        //{

        //}

        if (ConnectionManager.instance.objectsArManager.PortalsArData.Count == 0) {
            Debug.Log("El canal no contiene portales, se limpian los portales anteriores");
        }

        if (portal.activeInHierarchy == true) {
            panelAbertura.gameObject.SetActive(true);
            panelInstructivo.gameObject.SetActive(false);
        } else {
            panelAbertura.gameObject.SetActive(false);
            panelInstructivo.gameObject.SetActive(true);
        }

        foreach (PortalData portalAr in ConnectionManager.instance.objectsArManager.PortalsArData) {
            CargarImagen(portalAr);
        }

        if (simpleScrollSnap.NumberOfPanels == 0) {
            leftButton.SetActive(false);
            rightButton.SetActive(false);
        }else {
            leftButton.SetActive(true);
            rightButton.SetActive(true);
        }

        yield return new WaitForSeconds(0.1f);
        //Todo Cargado
        LoadingCanvasController.instance.ProcesosEnCurso = 0;
    }

    public void ReiniciarEscena() {
        LoadingCanvasController.instance.textImagenCarga.text = LocalizationManager.instance.GetLocalizedValue("loading_label");
        GameManager.instance.CargarPortales();
    }

    public void CargarImagen(PortalData portalData) {
        StartCoroutine(PortalImageLoad(portalData));
    }

    public void CargarPortal(PortalData portalData) {
        StartCoroutine(PortalLoad(portalData));
    }

    public void SetPortalImage(Sprite s)
    {
        portalLoadImg.overrideSprite = s;
        portalLoadCG.alpha = 1;
        StartCoroutine(PortalLoadFake());
    }

    public IEnumerator PortalLoadFake()
    {
        yield return new WaitForSeconds(6.75f);
        portalLoadCG.alpha = 0;
    }

    public IEnumerator PortalImageLoad(PortalData portalData) {
        if (portalData == null) {
            Debug.LogError("No se encontro el objetoAr correspondiente", gameObject);
            yield break;
        }

        simpleScrollSnap.AddToFront(portalImagePrefab);
        var portalPanel = simpleScrollSnap.Panels[0].GetComponent<PortalPanel>(); //Es el 0 por que se van agregando como primer hijo de todos los hermanos
        
        using (UnityWebRequest uwr = UnityWebRequestTexture.GetTexture(portalData.url_portalImage)) {
            // Wait for download to complete
            yield return uwr.SendWebRequest();

            if (uwr.isNetworkError || uwr.isHttpError) {
                Debug.Log(uwr.error);
                Debug.LogWarning("Esto aparece cuando se cambia de canal en medio de la corutina");
                yield break;
            }

            // assign texture
            var texture = DownloadHandlerTexture.GetContent(uwr);
            portalPanel.image.overrideSprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), Vector2.one * 0.5f);
            //portalTexture.preserveAspect = true;
        }

        portalPanel.button.onClick.AddListener(() => CargarPortal(portalData));
        portalPanel.PortalData = portalData;
        portalPanel.transform.localPosition = new Vector3(portalPanel.gameObject.transform.localPosition.x, portalPanel.gameObject.transform.localPosition.y, 0);
    }

    public IEnumerator PortalLoad(PortalData portalData) {
        if (portalData == null) {
            Debug.LogError("No se encontro el objetoAr correspondiente", gameObject);
            yield break;
        }

        panelInstructivo.gameObject.SetActive(false);
        panelAbertura.gameObject.SetActive(false);
        portal.GetComponentInChildren<PortalSphereManager>(true).gameObject.SetActive(portalData.url_portalExternal != "");
        portal.GetComponentInChildren<PortalSphereManager>(true).url = portalData.url_portalExternal;
        //portalTitleText.gameObject.SetActive(true);

          yield return null;

        portalMediaPlayer.m_VideoPath = portalData.url_portalVideo;

        //PortalVideoPlayer.url = portalData.url_portalVideo;
        //PortalVideoPlayer.Play();
        portalMediaPlayer.OpenVideoFromFile(MediaPlayer.FileLocation.AbsolutePathOrURL, portalData.url_portalVideo, true);

        InfoManager.instance.GetObjectARInfo(null, portalData);
    }

    public void RefreshObjetosAr() {
        StopAllCoroutines();
        portalLoadCG.alpha = 0;
        StartCoroutine(CargarObjetos());
    }

    public void OnPortalAttached() {
        panelInstructivo.gameObject.SetActive(false);
        panelAbertura.gameObject.SetActive(true);
        portal.transform.LookAt(GameObject.Find("AR Root").transform);
        portal.transform.eulerAngles = new Vector3(0, portal.transform.eulerAngles.y, 0);
        portal.SetActive(true);

        //var look = portal.transform;
        //look.LookAt(GameObject.Find("AR Root").transform);
        //look.rotation = Quaternion.Euler(0, look.rotation.eulerAngles.y, look.rotation.eulerAngles.z);
        //portalCanvas.planeDistance = 100;
    }

    public void RestartPortal() {
        if (panelInstructivo.gameObject.activeInHierarchy == false) {
            panelAbertura.gameObject.SetActive(true);
            portalTitleText.gameObject.SetActive(false);
            portal.GetComponentInChildren<PortalSphereManager>(true).gameObject.SetActive(false);
            //portalCanvas.planeDistance = 100;
        }
    }
}
