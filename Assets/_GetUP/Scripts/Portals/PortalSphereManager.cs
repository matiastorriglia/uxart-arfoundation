﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PortalSphereManager : MonoBehaviour
{
    public string url = "";

    private void OnMouseUpAsButton() {
        if (url == "") {
            return;
        }
        Application.OpenURL(url);
        print(url);
    }
}
