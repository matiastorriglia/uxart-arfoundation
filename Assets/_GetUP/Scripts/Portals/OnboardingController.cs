using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class OnboardingController : MonoBehaviour
{
    [SerializeField] private string _onboardingValue = "onboarding";

    public GameObject onboardingCanvas;
    public GameObject loadingCanvas;

    private void Start()
    {
        if (PlayerPrefs.GetString(_onboardingValue, "null") == "null")
        {
            PlayerPrefs.SetString(_onboardingValue, "true");
            onboardingCanvas.SetActive(true);
            loadingCanvas.SetActive(false);
        }
        else
        {
            onboardingCanvas.SetActive(false);
            loadingCanvas.SetActive(true);
            SceneManager.LoadScene("ManagerScene");
        }
    }

    public void GotoManagerScene()
    {
        SceneManager.LoadScene("ManagerScene");
    }
}
