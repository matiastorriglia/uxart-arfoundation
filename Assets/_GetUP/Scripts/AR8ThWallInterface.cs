using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;

namespace UnityARInterface {
    public class AR8ThWallInterface : ARInterface {

        public const float METERS_SCALE = 1.0f;
        private bool m_BackgroundRendering;
        private bool m_CanRenderBackground;
        //private ARBackgroundRenderer m_BackgroundRenderer;
        private List<Vector4> m_TempPointCloud = new List<Vector4>();
        XRController xRController;
        public float scale = METERS_SCALE;
        //private Dictionary<ARAnchor, Anchor> m_Anchors = new Dictionary<ARAnchor, Anchor>();

        public override bool IsSupported => base.IsSupported;

        public override bool BackgroundRendering {
            get {
                return m_BackgroundRendering && m_CanRenderBackground;
            }
            set {
                //if (m_BackgroundRenderer == null)
                //    return;

                //m_BackgroundRendering = value;
                //m_BackgroundRenderer.mode = m_BackgroundRendering && m_CanRenderBackground ?
                //    ARRenderMode.MaterialAsBackground : ARRenderMode.StandardBackground;
            }
        }

        public override IEnumerator StartService(Settings settings) {
            xRController = GameObject.FindWithTag("XRController").GetComponent<XRController>();

            if (xRController == null) {
                var xRControllerGO = new GameObject();
                xRControllerGO.AddComponent<XRController>();
                xRControllerGO.tag = "XRController";
            }

            xRController.enableLighting = settings.enableLightEstimation;
            xRController.enableSurfaces = settings.enablePlaneDetection;
            xRController.enableCamera = true;
            xRController.requestAndroidCameraPermissions = true;
            IsRunning = true;
            xRController.Resume();
            Debug.Log("StartService 8ThWall");
            yield return null;
        }

        public override void StopService() {
            //var anchors = m_Anchors.Keys;
            //foreach (var anchor in anchors)
            //{
            //    DestroyAnchor(anchor);
            //}
            BackgroundRendering = false;
            //if (m_BackgroundRenderer!=null) {
            //    m_BackgroundRenderer.backgroundMaterial = null;
            //    m_BackgroundRenderer.camera = null;
            //}
            //m_BackgroundRenderer = null;
            Debug.Log("StopService 8ThWall");
            if (xRController != null) {
                xRController.Pause();
            }
            IsRunning = false;
        }

        public override bool TryGetUnscaledPose(ref Pose pose) {
            if (xRController == null || xRController.GetCameraIntrinsics() == null || xRController.GetTrackingState().status != XRTrackingState.Status.NORMAL) {
                return false;
            }

            Matrix4x4 matrix = xRController.GetCameraIntrinsics();

            pose.position = xRController.GetCameraPosition();
            pose.rotation = xRController.GetCameraRotation();

            // pose.position = UnityARMatrixOps.GetPosition(matrix);
            // pose.rotation = UnityARMatrixOps.GetRotation(matrix);
            return true;
        }


        public override bool TryGetCameraImage(ref CameraImage cameraImage) { //Esto funciona solo para editor creo
            if (xRController.GetTrackingState().status == XRTrackingState.Status.UNSPECIFIED || xRController.GetTrackingState().status == XRTrackingState.Status.NOT_AVAILABLE) {
                return false;
            }
            if (xRController.GetTrackingState().reason == XRTrackingState.Reason.NOT_ENOUGH_TEXTURE) {
                return false;
            }

            Debug.Log("TryGetCameraImage");


            Texture2D textureY = xRController.GetRealityYTexture();
            cameraImage.y = textureY.GetRawTextureData();
            cameraImage.height = textureY.height;
            cameraImage.width = textureY.width;
            cameraImage.uv = xRController.GetRealityUVTexture().GetRawTextureData();
            Debug.Log("\n cameraImage.y: " + cameraImage.y + "\n cameraImage.height: " + cameraImage.height + "\n cameraImage.height: " + cameraImage.width + "\n cameraImage.width: " + cameraImage.uv);

            return true;
        }

        public override bool TryGetPointCloud(ref PointCloud pointCloud) {
            if (xRController.GetTrackingState().reason == XRTrackingState.Reason.RELOCALIZING || xRController.GetTrackingState().reason == XRTrackingState.Reason.TOO_MUCH_MOTION) {
                return false;
            }

            if (pointCloud.points == null)
                pointCloud.points = new List<Vector3>();

            pointCloud.points.Clear();
            var r = xRController.GetCurrentReality();

            if (r.getFeatureSet().getPoints().elementCount == 0) {
                return false;
            }

            foreach (var pt in r.getFeatureSet().getPoints()) {
                var p = pt.getPosition();
                pointCloud.points.Add(new Vector3(p.getX(), p.getY(), p.getZ()));
            }
            return true;
        }

        public override LightEstimate GetLightEstimate() {
            if (xRController.enableLighting) {
                return new LightEstimate() 
                {
                    capabilities = LightEstimateCapabilities.AmbientIntensity,
                    ambientIntensity = xRController.GetLightExposure()
                };
            } else {
                return new LightEstimate();
            }
        }

        public override Matrix4x4 GetDisplayTransform() {

            return xRController.GetCameraIntrinsics();
        }

        public override void SetupCamera(Camera camera) {
            xRController.UpdateCameraProjectionMatrix(
                camera, camera.transform.position, camera.transform.rotation, scale);
            Debug.Log("Camera Setup");
        }

        public override void UpdateCamera(Camera camera) {
            // This is handled for us by the XRCamera Video Controller
        }

        public override void ApplyAnchor(ARAnchor arAnchor) {
            base.ApplyAnchor(arAnchor);
        }

        public override void DestroyAnchor(ARAnchor arAnchor) {
            base.DestroyAnchor(arAnchor);
        }


        public override void Update() {
            //throw new System.NotImplementedException();

            //if (m_ARCoreSession == null)
            //    return;

            //AsyncTask.OnUpdate();

            //if (Session.Status != SessionStatus.Tracking)
            //    return;

            //if (m_ARCoreSessionConfig.EnablePlaneFinding) {
            //    Session.GetTrackables<TrackedPlane>(m_TrackedPlaneBuffer, TrackableQueryFilter.All);
            //    foreach (var trackedPlane in m_TrackedPlaneBuffer) {
            //        BoundedPlane boundedPlane;
            //        if (m_TrackedPlanes.TryGetValue(trackedPlane, out boundedPlane)) {
            //            // remove any subsumed planes
            //            if (trackedPlane.SubsumedBy != null) {
            //                OnPlaneRemoved(boundedPlane);
            //                m_TrackedPlanes.Remove(trackedPlane);
            //            }
            //            // update any planes with changed extents
            //            else if (PlaneUpdated(trackedPlane, boundedPlane)) {
            //                boundedPlane.center = trackedPlane.CenterPose.position;
            //                boundedPlane.rotation = trackedPlane.CenterPose.rotation;
            //                boundedPlane.extents.x = trackedPlane.ExtentX;
            //                boundedPlane.extents.y = trackedPlane.ExtentZ;
            //                m_TrackedPlanes[trackedPlane] = boundedPlane;
            //                OnPlaneUpdated(boundedPlane);
            //            }
            //        }
            //        // add any new planes
            //        else {
            //            boundedPlane = new BoundedPlane() {
            //                id = Guid.NewGuid().ToString(),
            //                center = trackedPlane.CenterPose.position,
            //                rotation = trackedPlane.CenterPose.rotation,
            //                extents = new Vector2(trackedPlane.ExtentX, trackedPlane.ExtentZ)
            //            };

            //            m_TrackedPlanes.Add(trackedPlane, boundedPlane);
            //            OnPlaneAdded(boundedPlane);
            //        }
            //    }

            //    // Check for planes that were removed from the tracked plane list
            //    List<TrackedPlane> planesToRemove = new List<TrackedPlane>();
            //    foreach (var kvp in m_TrackedPlanes) {
            //        var trackedPlane = kvp.Key;
            //        if (!m_TrackedPlaneBuffer.Exists(x => x == trackedPlane)) {
            //            OnPlaneRemoved(kvp.Value);
            //            planesToRemove.Add(trackedPlane);
            //        }
            //    }

            //    foreach (var plane in planesToRemove)
            //        m_TrackedPlanes.Remove(plane);

            //}

            ////Update Anchors
            //foreach (var anchor in m_Anchors) {
            //    anchor.Key.transform.position = anchor.Value.transform.position;
            //    anchor.Key.transform.rotation = anchor.Value.transform.rotation;
            //}
        }



    }
}
