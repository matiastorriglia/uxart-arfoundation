﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class DashboardPanelInfo : MonoBehaviour
{
    [SerializeField] public TextMeshProUGUI titleText;
    [SerializeField] public TextMeshProUGUI infoText;
    [SerializeField] public Image image;

    private DashboardInfoData dashboardInfoData;
    public DashboardInfoData DashboardInfoData {
        get => dashboardInfoData; set {
            dashboardInfoData = value;
            if (dashboardInfoData != null)
            {
                RefreshLanguage();
            }
        }
    }

    private void Awake()
    {
        LocalizationManager.OnChangeLanguage += RefreshLanguage;
    }

    private void OnDestroy()
    {
        LocalizationManager.OnChangeLanguage -= RefreshLanguage;
    }

    private void RefreshLanguage()
    {
        foreach (var info in dashboardInfoData.languageInfo)
        {
            //Cuando encuentra el lenguaje en uso del sistema lo aplica
            if (info.language == LocalizationManager.instance.currentLanguage.ToString())
            {
                infoText.text = info.text;
                titleText.text = info.title;
            }
        }
    }
}
