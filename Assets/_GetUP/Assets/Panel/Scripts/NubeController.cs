﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NubeController : MonoBehaviour
{
    public Animator[] elemento = new Animator[12];
    private float Timer;
    private bool[] anim = new bool[12]; // Para saber si la animación de entrada ya fue disparada
    private float[] TiemposOut = new float[12]; // Tiempos para desfazar las animaciones de salida
    private bool Desaparece; // Estado de desaparición, true cuando tengo que disparar las Anim_Out
    private int i;
    private int num_esfera;
    public AudioClip Plop1;
    public AudioClip Plop2;
    private AudioSource ASource;

    [HideInInspector] public bool interactuable = true;

    // Start is called before the first frame update
    void Start() {
        if (DashboardInfoManager.instance) {
            DashboardInfoManager.instance.dashboardNube = this;
        }

        Timer = 0.01f; // Valor mayor que cero es para Anim_In, menor que cero es Anim_Out
        anim[0] = false;
        anim[1] = false;
        anim[2] = false;
        anim[3] = false;
        anim[4] = false;
        anim[5] = false;
        anim[6] = false;
        anim[7] = false;
        anim[8] = false;
        anim[9] = false;
        anim[10] = false;
        anim[11] = false;
        TiemposOut[0] = -0.1f;
        TiemposOut[2] = -0.15f;
        TiemposOut[3] = -0.3f;
        TiemposOut[3] = -0.4f;
        TiemposOut[4] = -0.45f;
        TiemposOut[5] = -0.6f;
        TiemposOut[6] = -0.7f;
        TiemposOut[7] = -0.75f;
        TiemposOut[8] = -0.85f;
        TiemposOut[9] = -0.9f;
        TiemposOut[10] = -1.0f;
        TiemposOut[11] = -1.05f;
        Desaparece = false;
        i = 0;
        num_esfera = 0;
        ASource = GetComponent<AudioSource>();
        ASource.clip = Plop1;
    }

    // Update is called once per frame
    void Update() {
        // Según pasa el tiempo, disparo las animaciones de entrada
        if ((Timer >= 0.1) && (!anim[0])) {
            anim[0] = true;
            elemento[0].Play("Anim_In");
            ASource.Play();
        }
        if ((Timer >= 0.25) && (!anim[1])) {
            anim[1] = true;
            elemento[1].Play("Anim_In");
            ASource.Play();
        }
        if ((Timer >= 0.5) && (!anim[2])) {
            anim[2] = true;
            elemento[2].Play("Anim_In");
            ASource.Play();
        }
        if ((Timer >= 0.6) && (!anim[3])) {
            anim[3] = true;
            elemento[3].Play("Anim_In");
            ASource.Play();
        }
        if ((Timer >= 0.75) && (!anim[4])) {
            anim[4] = true;
            elemento[4].Play("Anim_In");
            ASource.Play();
        }
        if ((Timer >= 0.9) && (!anim[5])) {
            anim[5] = true;
            elemento[5].Play("Anim_In");
            ASource.Play();
        }
        if ((Timer >= 1.1) && (!anim[6])) {
            anim[6] = true;
            elemento[6].Play("Anim_In");
            ASource.Play();
        }
        if ((Timer >= 1.15) && (!anim[7])) {
            anim[7] = true;
            elemento[7].Play("Anim_In");
            ASource.Play();
        }
        if ((Timer >= 1.25) && (!anim[8])) {
            anim[8] = true;
            elemento[8].Play("Anim_In");
            ASource.Play();
        }
        if ((Timer >= 1.3) && (!anim[9])) {
            anim[9] = true;
            elemento[9].Play("Anim_In");
            ASource.Play();
        }
        if ((Timer >= 1.45) && (!anim[10])) {
            anim[10] = true;
            elemento[10].Play("Anim_In");
            ASource.Play();
        }
        if ((Timer >= 1.7) && (!anim[11])) {
            anim[11] = true;
            elemento[11].Play("Anim_In");
            ASource.Play();
        }
        // Controlo el Timer si se trata de animaciones de entrada o de salida
        if (Timer > 0) {
            Timer += Time.deltaTime;
        } else {
            Timer -= Time.deltaTime;
        }
        Desaparecer(num_esfera);
    }

    public void FocusCategory(string categoryName)
    {
        //Primero corre FocusCategory, y luego la funcion EsferaTocada, asi, en caso de que no hay una categoria de la esfera tocada, entonces no explota
        ExplodeSphere = DashboardInfoManager.instance.SnapTo(categoryName);
    }

    private bool ExplodeSphere;
    private int BubbleSelected;
    public void EsferaTocada(int esfera) {
        if (!interactuable || !ExplodeSphere) {
            return;
        }

        if (Timer > 0) {
            // Chequeo si el time venía de la anim_in y lo seteo en negativo para comenzar las anim_out
            Timer = -0.01f;
            // Cambio el clip de sonido para las animation out
            //ASource.clip = Plop2;
        }
        // Al elemento que se tocó le disparo la animación Push
        elemento[esfera].Play("Anim_Push");
        for (int i = 0; i < elemento.Length; i++) {
            if (i != esfera) {
                elemento[i].Play("Anim_Out");
            }
        }
        BubbleSelected = esfera;
        Invoke("HideBubble", 1);
        num_esfera = esfera;
        //Desaparece = true;
    }

    private void HideBubble() { //Llamado por un Invoke
        elemento[BubbleSelected].Play("Anim_Out");
    }


    public void RestartAnims() {
        for (int i = 0; i < anim.Length; i++) {
            anim[i] = false;
        }
        Timer = 0.01f;
    }

    public void Desaparecer(int esfera) {
        if (!interactuable) {
            return;
        }

        if ((i < 12) && (Desaparece)) {
            // Voy pasando por todos los índices para chequear si se cumplió el timer
            if ((Timer <= TiemposOut[i]) && (anim[i])) {
                anim[i] = false;
                if (i != esfera) {
                    elemento[i].Play("Anim_Out");
                    ASource.Play();
                }
                i++;
            }
        }
        // CUANDO i LLEGA A 12 ES CUANDO DEBEN AGREGAR EN ESTE MÉTODO EL LINK HACIA LA VENTANA 2D QUE CORRESPONDA
    }
}