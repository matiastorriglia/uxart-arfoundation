﻿/* Copio la projectionMatrix desde la cámara AR hasta la cámara secundaria
 * (que solo ve la geometría del portal) ya que el motor de AR
 * modifica la projectionMatrix por default al iniciar */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraAngle : MonoBehaviour
{
    public Camera CamaraAR;
    private Camera CamaraSecundaria;
    private float Timer;

    // Start is called before the first frame update
    void Start()
    {
        CamaraSecundaria = GetComponent<Camera>();
        Timer = 4.0f; // Para que la primera vez que cambie la matriz sea al segundo
    }

    // Update is called once per frame
    void Update()
    {
        Timer += Time.deltaTime;
        // Como no funciona haciéndolo en start, ni sé si es una operación "cara",
        // ni si efectivamente se pueden comparar las matrices, ejecuto la paridad cada 5 segundos
        if ((CamaraAR.projectionMatrix != CamaraSecundaria.projectionMatrix) && (Timer > 5))
        {
            Timer = 0;
            CamaraSecundaria.projectionMatrix = CamaraAR.projectionMatrix;
        }
    }
}