/* Oculta la habitaci�n cuando se ingresa al portal.
 * Este script se tiene que colocar en un collider trigger.
 * La c�maraAR debe contener un gameobject con un collider
 * y un rigidbody con la flag isKinematic. */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OcultarHabitacion : MonoBehaviour
{
    // PUBLIC
    public GameObject Habitacion;
    public AudioSource audio1;
    public AudioSource audio2;

    private void OnTriggerEnter(Collider other) {
        Habitacion.SetActive(false);
        audio1.Stop();
        audio2.Stop();
    }

    private void OnTriggerExit(Collider other) {
        Habitacion.SetActive(true);
        audio1.Play();
        audio2.Play(); ;
    }
}