﻿using UnityEditor;
using System.IO;

public class CreateAssetBundles {
    [MenuItem("Assets/Build AssetBundles")]
    static void BuildAllAssetBundles() {
        string assetBundleDirectory = "Assets/AssetBundles";
        if (!Directory.Exists(assetBundleDirectory + "/Android")) {
            Directory.CreateDirectory(assetBundleDirectory + "/Android");
        }
        if (!Directory.Exists(assetBundleDirectory + "/IOS")) {
            Directory.CreateDirectory(assetBundleDirectory + "/iOS");
        }
        BuildPipeline.BuildAssetBundles(assetBundleDirectory + "/Android", BuildAssetBundleOptions.None, BuildTarget.Android);
        BuildPipeline.BuildAssetBundles(assetBundleDirectory + "/iOS", BuildAssetBundleOptions.None, BuildTarget.iOS);
    }
}